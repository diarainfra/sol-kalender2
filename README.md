SOL Kalenderrakendus
====================

Dev URL: http://dev.diara.ee/kalender
Live URL: https://sol.ee/kalender

Developer know-how:
-------------------

* Database documentation and other info is in the doc folder
* Do not change the database without updating the database.mwb ERD model (Mysql Workbench). Changes are exported from the model to database.sql
* Project versioning is done by Git (so no modifying live files without first commiting to dev repo!)


Install guide:
--------------

* Enable **short_open_tag**  in php.ini
* Import dump from `doc/database.sql`
* Edit RewriteBase in `.htaccess` (for Apache)
* Copy `application/config/database.sample.php` to `application/config/database.php` and edit database connections there 
* Copy `application/bootstrap.sample.php` to `application/bootstrap.php` and edit `base_url` there 