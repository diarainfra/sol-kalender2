// Calendar
var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();


/**
 * Convert a date object to a UNIX epoch time (in seconds)
 * @param date
 */
function dateToSeconds(date) {
    return Math.round(date.getTime() / 1000);
}

var calendar_options = {
    theme:true,
    header:{
        left:'prev,next today',
        center:'title',
        right:'month,agendaWeek,agendaDay'
    },
    editable:false,
    dateFormat:'dd.mm.yy',
    allDayDefault:false,

    dayNamesMin:['P', 'E', 'T', 'K', 'N', 'R', 'L'],
    dayNamesShort:['Püh', 'Esm', 'Teis', 'Kolm', 'Nelj', 'Reede', 'Laup'],
    dayNames:['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
    monthNames:['Jaanuar', 'Veebruar', 'Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember'],
    monthNamesShort:['Jaan', 'Veeb', 'Mär', 'Apr', 'Mai', 'Juun', 'Juul', 'Aug', 'Sept', 'Okt', 'Nov', 'Det'],
    firstDay:1,
    buttonText:{
        prev:'&nbsp;&#9668;&nbsp;', // left triangle
        next:'&nbsp;&#9658;&nbsp;', // right triangle
        prevYear:'&nbsp;&lt;&lt;&nbsp;', // <<
        nextYear:'&nbsp;&gt;&gt;&nbsp;', // >>
        today:'täna',
        month:'kuu',
        week:'nädal',
        day:'päev'
    },

    /**
     * Will be called each time the calendar needs data
     *
     * Passes filters from the filter modal to the API
     * @param start
     * @param end
     * @param callback
     */
    events:function (start, end, callback) {
        $.get(base_url + 'calendar/data', {
            area:$('#area-select').val(),
            object:$('#filter-object').val(),
            'employee-type':$('input[name="employee-type"]').val(),
            employees:$('input[name="employees"]').val(),
            start:dateToSeconds(start),
            end:dateToSeconds(end)
        }, function (json) {
            if (json.status == 200) {
                callback(json.response);
            }
        });
    },
    eventBackgroundColor:'#FDD44B',
    eventBorderColor:'#333333',
    eventTextColor:'#381F16'





}

// Full overview calendar
$('#calendar').fullCalendar(calendar_options);


/**
 * Reload the calendar on filter changes
 */
function calendar_refresh() {
    console.debug('Refreshing calendar data via AJAX');
    $('#calendar').fullCalendar('refetchEvents');
}

$(document).ready(function () {
    $('#area-select, #filter-object, input[name="employee-type"], input[name="employees"]').change(calendar_refresh);
});