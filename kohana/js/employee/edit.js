var employee_id = $('input[name="employee_id"]').val();
// Double configuration - this should be circumvented!
var calendar_options = {
    theme:true,
    header:{
        left:'prev,next today',
        center:'title',
        right:'month,agendaWeek,agendaDay'
    },
    editable:false,
    dateFormat:'dd.mm.yy',
    allDayDefault:false,

    dayNamesMin:['P', 'E', 'T', 'K', 'N', 'R', 'L'],
    dayNamesShort:['Püh', 'Esm', 'Teis', 'Kolm', 'Nelj', 'Reede', 'Laup'],
    dayNames:['Pühapäev', 'Esmaspäev', 'Teisipäev', 'Kolmapäev', 'Neljapäev', 'Reede', 'Laupäev'],
    monthNames:['Jaanuar', 'Veebruar', 'Märts', 'Aprill', 'Mai', 'Juuni', 'Juuli', 'August', 'September', 'Oktoober', 'November', 'Detsember'],
    monthNamesShort:['Jaan', 'Veeb', 'Mär', 'Apr', 'Mai', 'Juun', 'Juul', 'Aug', 'Sept', 'Okt', 'Nov', 'Det'],
    firstDay:1,
    buttonText:{
        prev:'&nbsp;&#9668;&nbsp;', // left triangle
        next:'&nbsp;&#9658;&nbsp;', // right triangle
        prevYear:'&nbsp;&lt;&lt;&nbsp;', // <<
        nextYear:'&nbsp;&gt;&gt;&nbsp;', // >>
        today:'täna',
        month:'kuu',
        week:'nädal',
        day:'päev'
    },

    events:{
        type:'POST',
        cache:true,
        url:base_url + 'api/employee_calendar/' + employee_id
    },
    eventBackgroundColor:'#FDD44B',
    eventBorderColor:'#333333',
    eventTextColor:'#381F16'
}

// Employee's calendar
$('#employee_calendar').fullCalendar(calendar_options);