<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Commoneer config
 * @package Commoneer
 * @author Ando Roots 2011
 * @since 1.0
 * @copyright GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
return array(

	/**
	 * The initial version of a full-length date format
	 * Any valid date() format string will do.
	 * @see date()
	 * @see Date::localized_date
	 */
	'date_format_long' => 'd.m.Y H:i',

	/**
	 * The initial version of a short date format
	 * Any valid date() format string will do.
	 * @see date()
	 * @see Date::localized_date
	 */
	'date_format_short' => 'd.m.Y',
);

