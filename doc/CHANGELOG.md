# SOL Kalenderrakendus - Muudatuste logi

Klienditugi: help@diara.ee, (+372) 687 0999

Praegust versiooninumbrit näeb lehe jalusest.

## Versioon 1.3.0

* Nüüd on võimalik vaadata ka nende töötajate infot, kellel puudub süsteemis kasutajakonto. Varasemalt seda teha ei saanud.

## Versioon 1.2.1

* Uuendatud kujundusraamistiku "Bootstrap" versioon 2.1 peale

## Versioon 1.2.0

* Töö detailide lehe "Materjalid" vahekaart näitab oma pealkirjas ka lisatud materjalide arvu
* Materjalide vahekaardil on nüüd link "Kustuta"
* Uuendatud kujundusraamistik Bootstrap versioon 2.0 peale

## Versioon 1.1.5

* Veaparandus: Vanemate veebisirvijatega ei lubata salvestada tühje objekte

## Versioon 1.1.4

* Töö vaade organiseeritud vahekaartidesse
* Lisatud tegevuste logimine (ajaloo vaade)

## Versioon 1.1.2

* Raportite vaade avatakse esmakordsel laadimisel automaatselt käesoleva kuu pealt
* Muutus PDF kujul raporti kujundus
* Raporti vaatesse lisandus detaile (töö ID-d on klikitavad, jalused on ridade arvu summa etc)

## Versioon 1.1.1

* Tabelitel on uus välimus ning need mahutavad rohkem infot
* Dialoogid on puhtamad ja kasutatavamad
* Tööde kalendri filtreerimine toimub lehte uuesti laadimata, filtrid asuvad dialoogis
* Lisandus "Abi" link

## Versioon 1.0.0

* Esimene väljalase
