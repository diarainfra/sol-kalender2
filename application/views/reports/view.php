<div class="clearfix"></div>
<br/>
<h2><?=Helper_Reports::report_title(@$filters)?></h2>
<div class="clearfix"></div>

<? if ($in_contract_data->count()): ?>
<!-- Contract based tasks -->
<h3><?=__('Lepingulised tööd')?></h3>
<table class="table table-striped table-condensed tablesorter">
    <thead>
    <tr>
        <th><?=__('Töö nr.')?></th>
        <th><?=__('Kuup')?></th>
        <th><?=__('Algus')?></th>
        <th><?=__('Lõpp')?></th>
        <th><?=__('Objekt')?></th>
        <th><?=__('Kirjeldus')?></th>
        <th><?=__('Kogus')?></th>
        <th><?=__('Ühik')?></th>
        <th><?=__('Auto (km)')?></th>
        <th><?=__('Sisearve')?></th>
        <th><?=__('Arve esitatud')?></th>
    </tr>
    </thead>
    <tbody>
        <?=Helper_Reports::rows($in_contract_data, 'in')?>
    </tbody>
    <tfoot>
    <tr>
        <th title="<?=__('Tööde arv kokku')?>">&sum; <?=$in_contract_data->count()?></th>
        <th colspan="5" style="text-align: right;"><?=__('Teostatud tööd kokku')?></th>
        <th class="nowrap"><?=Helper_Template::pretty_time(Helper_Reports::sums($in_contract_data, 'time'))?></th>
        <th colspan="5">h</th>
    </tr>
    </tfoot>
</table>
<? else: ?>
<h5><?=__('Lepingulisi töid pole.')?></h5>
<? endif ?>

<? if ($outside_contract_data->count()): ?>
<!-- Outside contract -->
<h3><?=__('Lepinguvälised, tellitud tööd')?></h3>
<table class="table table-striped table-condensed tablesorter">
    <thead>
    <tr>
        <th><?=__('Töö nr.')?></th>
        <th><?=__('Kuup')?></th>
        <th><?=__('Algus')?></th>
        <th><?=__('Lõpp')?></th>
        <th><?=__('Objekt')?></th>
        <th><?=__('Kirjeldus')?></th>
        <th><?=__('Kogus')?></th>
        <th><?=__('Ühik')?></th>
        <th><?=__('Auto (km)')?></th>
        <th><?=__('Ühiku hind')?></th>
        <th><?=__('Kokku')?></th>
        <th><?=__('Sisearve')?></th>
    </tr>
    </thead>
    <tbody>
        <?=Helper_Reports::rows($outside_contract_data, 'out')?>
    </tbody>
    <tfoot>
    <tr>
        <th title="<?=__('Tööde arv kokku')?>">&sum; <?=$outside_contract_data->count()?></th>
        <th colspan="5" style="text-align: right;"><?=__('Teostatud tööd kokku')?></th>
        <th class="nowrap"><?=Helper_Template::pretty_time(Helper_Reports::sums($outside_contract_data, 'time'))?></th>
        <th colspan="3">h</th>
        <th>       <?=Helper_Reports::sums($outside_contract_data, 'sum')?></th>
        <th></th>
    </tr>
    </tfoot>
</table>

<? else: ?>
<h5><?=__('Lepinguväliseid töid pole.')?></h5>

<? endif ?>

<div class="clearfix"></div>
<? if (count($tasks_in_range)): ?>
<!-- Materials used during the task -->
<h3><?=__('Hooldustöödel kasutatatud materjalid, seadmed ja allhanketööd')?></h3>
<table class="table table-striped span12 table-condensed tablesorter">
    <thead>
    <tr>
        <th><?=__('Töö nr.')?></th>
        <th><?=__('Kuup')?></th>
        <th><?=__('Materjali nimetus')?></th>
        <th><?=__('Kogus')?></th>
        <th><?=__('Ühik')?></th>
        <th><?=__('Ühiku hind')?></th>
        <th><?=__('Kokku')?></th>
        <th><?=__('Sisearve?')?></th>
        <th><?=__('Sisaldub arves?')?></th>
    </tr>
    </thead>

    <?=Helper_Task::material_rows($tasks_in_range)?>
</table>

<? else: ?>
<h5><?=__('Kasutatud materjale pole.')?></h5>

<? endif ?>

<div class="clearfix"></div>