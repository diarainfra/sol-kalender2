<!-- Dialog for creating a new object -->
<div id="modal-email" class="modal hide fade">
    <form action="<?=URL::base();?><?=($type == 'report') ? 'reports/email' : 'bills/email'?>" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Saada e-mailiga')?>
            </h3>
        </div>
        <div class="modal-body">

            <fieldset>
                <table>
                    <tr>
                        <td><?=__('Saatja')?></td>
                        <td>
                            Maivi Sirts &lt;maivi.sirts@sol.ee&gt;
                        </td>
                    </tr>
                    <tr>
                        <td><?=__('Pealkiri')?></td>
                        <td>
                            <input type="text" name="subject" placeholder="<?=__('Pealkiri')?>" size="30"
                                   maxlength="150"
                                   value="<?
                                   if ($type == 'report') {
                                       $name = __('Aruanne');
                                   }
                                   elseif ($type == 'bill') {
                                       $name = __('Arve');
                                   }
                                   else {
                                       $name = __('Sisearve');
                                   }
                                   echo $name ?>: <?=@$filters['object_name']?> (<?=@$filters['start']?> - <?=@$filters['end']?>)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?=__('Saajad')?>
                            <span class="help-block">
                                <?=__('Eralda e-mailid komadega')?>
                            </span>
                        </td>
                        <td><textarea rows="4" cols="30" maxlength="800" name="to"
                                      autofocus>tiina.talivere@sol.ee,</textarea></td>
                    </tr>
                    <tr>
                        <td>
                            <?=__('Selgitus')?>
                            <span class="help-block">
                                <?=__('Kirja sisu')?>
                            </span>
                        </td>
                        <td>
                            <? $type_est = ($type == 'report') ? __('aruanne') : __('arve');
                            $text = "See kiri on saadetud SOL
                                           kalendrirakenduse poolt.
                                           Manusena on kaasatud " . $type_est . "
                                           objekti " . @$filters['object_name'] . " kohta perioodil " . @$filters['start'] . "
                                           - " . @$filters['end'];
                            $text = str_replace("\r", ' ', $text);
                            $text = str_replace("\t", ' ', $text);
                            $text = preg_replace('!\s+!', ' ', $text);                            ?>
                            <textarea cols="30" rows="4" maxlength="800" name="body"><?=$text?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="hidden" name="start" value="<?=@$filters['start']?>">
                            <input type="hidden" name="end" value="<?=@$filters['end']?>">
                            <input type="hidden" name="object" value="<?=@$filters['object']?>">
                            <input type="hidden" name="attachment" value="<?=@$attachment?>"/>
                            <input type="hidden" name="redirect"
                                   value="<?=($type == 'report') ? 'reports' : 'bills' ?>"/>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </fieldset>

        </div>
        <div class="modal-footer">
            <input type="submit" name="send-email" value="Saada" class="btn btn-large btn-primary"/>
        </div>
    </form>
</div>