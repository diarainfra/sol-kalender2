<h1><?=__('Tööde aruanne')?></h1>

<span class="help-block">
    <?=__('Aruandes kajastuvad tööd sõltuvad määratud filtritest.')?>
</span>

<div class="spacer"></div>
<a data-keyboard="true" class="btn btn-large" data-controls-modal="modal-filters" data-backdrop="true">
    <?=__('Filtrid')?>
</a>

<? if (!empty($_GET)): ?>

<a href="<?=URL::query(array('pdf' => TRUE))?>" class="btn" title="<?=__('Laadi alla PDF')?>">
    <img src="<?=URL::base()?>assets/img/file_pdf.png" alt="pdf" width="28" height="28"/>
</a>
<a data-keyboard="true" class="btn" data-controls-modal="modal-email" data-backdrop="true">
    <img src="<?=URL::base()?>assets/img/email.png" alt="e-mail" title="<?=__('Saada kirjana')?>"/>
</a>
<? endif; ?>


<!-- Filters modal -->
<div id="modal-filters" class="modal hide fade">
    <form action="<?=URL::base()?>reports/view" id="filters" method="GET">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Filtrid')?>
            </h3>
        </div>
        <div class="modal-body">

            <table>
                <tbody>
                <tr>
                    <th>Periood (algus)</th>

                    <td>
                        <input type="text" size="10" class="date" maxlength="10" name="start" placeholder="Algus"
                               value="<?=@$filters['start']?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Periood (lõpp)</th>
                    <td>
                        <input type="text" size="10" class="date" maxlength="10" name="end" placeholder="Lõpp"
                               value="<?=@$filters['end']?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Objekt</th>

                    <td>
                        <select name="object">
                            <option value=""></option>
                            <?=Helper_Objects::objects_select_with_code(@$filters['object'])?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Töötaja</th>

                    <td>
                        <select name="employee">
                            <option value=""></option>
                            <?=Helper_Employee::employee_options(@$filters['employee'])?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Töödejuhataja</th>

                    <td>
                        <select name="manager">
                            <option value=""></option>
                            <?=Helper_Employee::employee_options(@$filters['manager'], TRUE)?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Klient</th>

                    <td>
                        <select name="client">
                            <option value=""></option>
                            <?=Helper_Clients::clients_select(@$filters['client'])?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Piirkond</th>

                    <td>
                        <?=Helper_Objects::area_select(@$filters['area'], TRUE)?>
                    </td>
                </tr>
                </tbody>

            </table>

        </div>
        <div class="modal-footer">
            <input type="submit" class="btn btn-large btn-primary" value="<?=__('Filtreeri aruannet')?>"/>
        </div>
    </form>
</div>




<!-- The report will appear here -->
<div id="report_area" style="padding: 2px;">
    <?=@$report?>
</div>

<?= $email_form ?>


<link type="text/css" href="<?=URL::base()?>assets/css/custom-combobox-and-autocomplete.css" rel="stylesheet"/>
<script type="text/javascript" src="<?=URL::base()?>assets/js/libs/jquery.jqprint.js"></script>
<script type="text/javascript" src="<?=URL::base()?>assets/js/custom-combobox.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#print').click(function () {
            $('#report_area').jqprint();
        });

        // Activate combobox
        $("select[name=object]").combobox();

    })
</script>

<div class="clearfix"></div>