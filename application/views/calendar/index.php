<h1><?=__('Kalender')?></h1>

<a href="<?=URL::base()?>task/new" class="btn btn-primary"><?=__('Lisa uus töö')?></a>
<a href="<?=URL::base()?>comments/new"" class="btn btn-primary"><?=__('Lisa m&auml;rkus')?></a>

<a data-keyboard="true" class="btn" data-backdrop="true" data-controls-modal="modal-filters">
    <?=__('Kalendri filtrid')?>
</a>

<span class="help-block"><?=__('Töö detailsema info nägemiseks ning kasutatud materjalide lisamiseks vajuta kalendris selle
    nimele.')?></span>


<div class="spacer"></div>

<div id="calendar"></div>

<div class="clearfix"></div>
<div class="spacer"></div>


<!-- Filter calendar modal -->
<div id="modal-filters" class="modal hide fade">
    <div class="modal-header">
        <a href="#" class="close">&times;</a>

        <h3>
            <?=__('Kalendri filtreerimine')?>
        </h3>
    </div>
    <div class="modal-body form-horizontal">

        <p class="help-block">
            <?=__('Siin dialoogis saad taustal olevat kalendrit filtreerida. Muudatused rakenduvad automaatselt peale valiku tegemist.')?>
        </p>
        <div class="well">
            <div class="control-group">
                <label class="control-label" for="area-select"><?=__('Piirkond')?></label>

                <div class="controls">
                    <?=Helper_Objects::area_select('0', TRUE)?>

                    <p class="help-block">
                        <?=__('Kui valitud on nii piirkond kui ka objekt, filtreeritakse ainult piirkonna järgi.')?>
                    </p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="filter-object"><?=__('Objekt')?></label>

                <div class="controls">
                    <select id="filter-object" name="object">
                        <option value=""></option>
                        <?=Helper_Objects::objects_select()?>
                    </select>
                </div>
            </div>

            <div class="control-group">

                <label class="control-label">
                    <?=__('Filreerimisrežiim')?>
                </label>

                <div class="controls">
                    <label class="radio">

                        <input id="radio-one-employee" type="radio" name="employee-type"
                               value="1"/>
                        <?=__('Kõik mainitud töötajad')?>
                    </label>

                    <label class="radio">
                        <input id="radio-all-employees" type="radio" name="employee-type"
                               value="0"/>
                        <?=__('Üks mainitud töötajatest')?>
                    </label>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">
                    <?=__('Töötajad')?>
                </label>

                <div class="controls">
                    <input type="text" class="employee-suggest" name="employees"/>
                </div>
            </div>

        </div>

    </div>
</div>


<script type="text/javascript">
    /*
       Tokenizer initialization done here to refill it with posted data
    */
    $(document).ready(function () {
        // Names
        $('.employee-suggest').tokenInput(<?=URL::base()?> +'api/employee_search',
                { method:'post',
                    preventDefault:true,
                    preventDuplicates:true,
                    searchingText:"Otsin...",
                    noResultsText:"Ei leitud",
                    hintText:"Kirjuta töötaja nimi...",
                    prePopulate: <?=(!empty($tokenizer)) ? $tokenizer : 'null'?>,
                    tokenDelimiter:'+',
                    zindex:100001,
                    classes:{
                        tokenList:"token-input-list-facebook",
                        token:"token-input-token-facebook",
                        tokenDelete:"token-input-delete-token-facebook",
                        selectedToken:"token-input-selected-token-facebook",
                        highlightedToken:"token-input-highlighted-token-facebook",
                        dropdown:"token-input-dropdown-facebook",
                        dropdownItem:"token-input-dropdown-item-facebook",
                        dropdownItem2:"token-input-dropdown-item2-facebook",
                        selectedDropdownItem:"token-input-selected-dropdown-item-facebook",
                        inputToken:"token-input-input-token-facebook"
                    }
                });
    });
</script>