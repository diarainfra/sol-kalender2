<?
foreach ($msgs as $msg_type => $msgs_of_type):
    $class = NULL;
    if (in_array($msg_type, array('success', 'warning', 'info', 'error'))) {
        $class = 'alert-' . $msg_type;
    }    ?>
<div class="alert alert-block fade in <?=$class ?>">
    <a class="close" href="#" data-dismiss="alert">×</a>
    <? if (count($msgs_of_type) > 0) {
    echo implode("<br />\n", $msgs_of_type);
} else {
    echo $msgs_of_type;
}
    ?>
</div>
<?
endforeach;
?>
