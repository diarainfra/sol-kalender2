<!doctype html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <title><?=(!empty($title)) ? $title : 'Kalendrirakendus:Aruanne' ?></title>
    <meta name="description" content="SOL Kalenderrakendus">
    <meta name="author" content="Diara OÜ">
    <style type="text/css">
        <?=file_get_contents(DOCROOT . 'assets/css/print.css')?>
    </style>
</head>

<body>

<div id="container">
    <div id="main" role="main">
        <?=@$content?>
    </div>
</div>

</body>
</html>