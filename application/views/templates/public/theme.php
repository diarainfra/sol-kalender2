<?php defined('SYSPATH') or die('No direct script access.')?><!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?=$title?></title>
    <meta name="description" content="SOL Kalenderrakendus">
    <meta name="author" content="Diara OÜ">

    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- CSS concatenated and minified via ant build script-->
    <link type="text/css" href="<?=URL::base()?>assets/css/bootstrap-2.1/css/bootstrap.min.css"
          rel="stylesheet"/>
    <link rel="stylesheet" href="<?=URL::base()?>assets/css/public/theme.css">
    <?=Assets::render(Assets::CSS)?>
    <!-- end CSS-->

    <script src="<?=URL::base()?>assets/js/libs/modernizr-2.0.6.min.js"></script>
    <script src="<?=URL::base()?>assets/js/libs/jquery-1.7.1.min.js"></script>
</head>

<body>

<div id="container">
    <header>
        <h1><?=Kohana::$config->load('app.title')?></h1>

        <div class="clearfix"></div>
    </header>


    <div id="main" role="main">
        <?=Notify::render()?>
        <?=$content?>
    </div>

    <footer>
        <?=Kohana::$config->load('app.codename')?> versioon <?=Kohana::$config->load('app.version')?>
        <a href="mailto:help@diara.ee" title="<?=__('Klienditugi')?>">help@diara.ee</a> |
        <a href="http://diara.ee" title="Diara OÜ">www.diara.ee</a>
    </footer>
</div>
<!--! end of #container -->
<?=Assets::render(Assets::SCRIPT)?>
</body>
</html>