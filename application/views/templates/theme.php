<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?=$title?></title>
    <meta name="description" content="SOL Kalenderrakendus">
    <meta name="author" content="Diara OÜ">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?=URL::base()?>assets/favicon.ico">

    <link type="text/css" href="<?=URL::base()?>assets/css/bootstrap-2.1/css/bootstrap.min.css"
          rel="stylesheet"/>

    <link type="text/css" href="<?=URL::base()?>assets/css/jquery.jgrowl.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?=URL::base()?>assets/css/style.css?v=2">
    <link rel="stylesheet" href="<?=URL::base()?>assets/css/print.css?v=2" media="print">

    <script src="<?=URL::base()?>assets/js/libs/modernizr-2.0.6.min.js"></script>
    <script src="<?=URL::base()?>assets/js/libs/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        var base_url = '<?=URL::base()?>';
    </script>

    <?=Assets::render(Assets::CSS)?>
</head>

<body>

<div id="container">
    <header>
        <a href="<?=URL::base()?>">
            <img src="<?=URL::base()?>assets/img/logo.gif" alt="logo"/>
        </a>

        <h1><?=Kohana::$config->load('app.title')?></h1>
    </header>
    <div id="main-menu">
        <ul>
            <li><a href="<?=URL::base()?>calendar"
                   title="<?=__('Filtreeritav ülevaade tehtud töödest')?>"><?=__('Kalender')?></a></li>
            <li><a href="<?=URL::base()?>reports"
                   title="<?=__('Filtreeritav aruanne tehtud töödest tabeli kujul')?>"><?=__('Aruanded')?></a></li>
            <li><a href="<?=URL::base()?>bills"
                   title="<?=__('Arvete koostamine perioodi ja objekti kaupa')?>"><?=__('Arved')?></a></li>
            <li><a href="<?=URL::base()?>objects" title="<?=__('Objektide haldamine')?>"><?=__('Objektid')?></a></li>
            <li><a href="<?=URL::base()?>employee" title="Firma köötajate haldamine"><?=__('Töötajad')?></a></li>
            <li><a href="<?=URL::base()?>clients" title="Klientide haldamine"><?=__('Kliendid')?></a></li>
            <li><a href="<?=URL::base()?>services" title="Pakutavate teenuste haldamine"><?=__('Teenused')?></a></li>

            <? if (User::current()->has('roles', 2)): ?>
            <li><a href="<?=URL::base()?>log" title="<?=__('Süsteemi logid')?>"><?=__('Logid')?></a></li>
            <? endif?>

            <li style="float: right;"><a href="<?=URL::base()?>public/auth/logout"
                                         title="<?=__('Sessiooni lõpetamine')?>"><?=__('Välju')?></a></li>
            <li style="float: right;"><a href="<?=URL::base()?>help"
                                         title="<?=__('Abiteave rakendusest')?>"><?=__('Abi')?></a></li>
            <li style="float: right;"><a href="<?=URL::base()?>settings"
                                         title="<?=__('Globaalsete seadete muutmine')?>"><?=__('Seaded')?></a></li>
        </ul>
    </div>
    <?=Notify::render()?>

    <div id="main" role="main">
        <div class="row">
            <?=$content?>
        </div>
    </div>
    <footer>
        <a href="http://diara.ee">Diara OÜ</a> | help@diara.ee |
        <?=__('Versioon')?>
        <a href="<?=URL::base()?>help/changelog" title="<?=__('Muudatuste info')?>">
            <?=Kohana::$config->load('app.version')?>
        </a>
    </footer>
</div>
<!-- eo #container -->


<script type="text/javascript" src="<?=URL::base()?>assets/js/libs/jquery.jgrowl_minimized.js"></script>
<script src="<?=URL::base()?>assets/js/libs/bootstrap/alerts-1.4.0.min.js"></script>

<?=Assets::render(Assets::SCRIPT)?>
<script src="<?=URL::base()?>assets/js/plugins.js"></script>
<!-- end scripts-->


<!--[if lt IE 7 ]>
<script src="<?=URL::base()?>assets/js/libs/dd_belatedpng.js"></script>
<script>DD_belatedPNG.fix("img, .png_bg");</script>
<![endif]-->

</body>
</html>