<h1>Uus klient</h1>
<form action="<?=URL::base()?>clients" method="post">
    <fieldset>
        <table>
            <tr>
                <td>Kliendi number: *</td>
                <td><input type="text" size="6" maxlength="10" name="code" required autofocus/></td>
            </tr>
            <tr>
                <td>Juriidiline nimi: *</td>
                <td><input type="text" size="15" maxlength="80" name="name" required/></td>
            </tr>
            <tr>
                <td>Postiaadress:</td>
                <td>
                    <textarea rows="3" cols="20" name="postal_address"></textarea>
                </td>
            </tr>
            <tr>
                <td>Telefon:</td>
                <td><input type="text" size="15" maxlength="20" name="phone"/></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="email" size="20" maxlength="50" name="email"/></td>
            </tr>
            <tr>
                <td>Lepingu algus:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="contract_start"/></td>
            </tr>
            <tr>
                <td>Lepingu lõpp:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="contract_end"/></td>
            </tr>
            <tr>
                <td>Sünnipäev:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="birthday"/></td>
            </tr>
            <tr>
                <td>Lisainfo:</td>
                <td>
                    <textarea rows="3" cols="20" name="extra"></textarea>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" class="btn btn-primary btn-large right" value="Lisa"/></td>
            </tr>
        </table>

    </fieldset>
</form>