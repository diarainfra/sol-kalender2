<h1><?=__('Klientide nimekiri')?></h1>

<a href="<?=URL::base()?>clients/new" title="<?=__('Uus klient')?>" class="btn"><?=__('Uus klient')?></a>

<table class="table tablesorter table-striped table-condensed">
    <colgroup></colgroup>
    <colgroup></colgroup>
    <colgroup></colgroup>
    <thead>
    <tr>
        <th><?=__('Kood')?></th>
        <th><?=__('Juriidiline nimi')?></th>
        <th><?=__('Postiaadress')?></th>
        <th><?=__('Kontaktisikud')?></th>
        <th><?=__('E-mail')?></th>
        <th><?=__('Telefon')?></th>
        <th><?=__('Lepingu algus')?></th>
        <th><?=__('Lepingu lõpp')?></th>
        <th><?=__('Sünnipäev')?></th>
        <th><?=__('Lisainfo')?></th>
    </tr>
    </thead>
    <tbody>
    <? if (!empty($clients)): ?>
        <? foreach ($clients as $client): ?>

        <tr>
            <td><?=$client->code?></td>
            <td>
                <a href="<?=URL::base()?>clients/edit/<?=$client->id?>" title="<?=__('Muuda')?>">
                    <?=$client->name?>
                </a>
            </td>
            <td><?=$client->postal_address?></td>
            <td><?=Helper_Contacts::contact_links($c->get_contacts($client->id))?></td>
            <td><?=$client->email?></td>
            <td><?=$client->phone?></td>
            <td><?=Date::localized_date($client->contract_start) ?></td>
            <td><?=Date::localized_date($client->contract_end) ?></td>
            <td><?=Date::localized_date($client->birthday) ?></td>
            <td><?=$client->extra?></td>
        </tr>
            <? endforeach ?>
        <? endif?>

    </tbody>
</table>

<div class="clearfix"></div>