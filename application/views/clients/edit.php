<h1>Kliendi "<?=$client->name?>" muutmine</h1>

<form action="<?=URL::base()?>clients/edit/<?=$client->id?>" method="post">
    <fieldset class="float-box">
        <table>
            <tr>
                <td>Kliendi number</td>
                <td><input type="text" size="10" maxlength="10" name="code" value="<?=$client->code?>"/></td>
            </tr>
            <tr>
                <td>Nimi</td>
                <td><input type="text" size="15" maxlength="80" name="name" value="<?=$client->name?>" autofocus/></td>
            </tr>

            <tr>
                <td>Postiaadress</td>
                <td><textarea rows="3" cols="20" name="postal_address"><?=$client->postal_address?></textarea>
                </td>
            </tr>
            <tr>
                <td>Telefon:</td>
                <td><input type="text" size="15" maxlength="20" value="<?=$client->phone?>" name="phone"/></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="email" size="20" maxlength="50" name="email" value="<?=$client->email?>"/></td>
            </tr>
            <tr>
                <td>Lepingu algus:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="contract_start"
                           value="<?=$client->contract_start?>"/></td>
            </tr>
            <tr>
                <td>Lepingu lõpp:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="contract_end"
                           value="<?=$client->contract_end?>"/></td>
            </tr>
            <tr>
                <td>Sünnipäev:</td>
                <td><input type="text" class="date" size="10" maxlength="10" name="birthday"
                           value="<?=$client->birthday?>"/></td>
            </tr>
            <tr>
                <td>Lisainfo:</td>
                <td>
                    <textarea rows="3" cols="20" name="extra"><?=$client->extra?></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="id" value="<?=$client->id?>"/>
                    <a href="<?=URL::base()?>clients" title="Tagasi" class="btn">Tagasi</a>
                </td>
                <td>
                    <a href="<?=URL::base()?>clients/delete/<?=$client->id?>" title="Kustuta"
                       class="btn danger">Kustuta</a>
                    <input type="submit" class="right btn btn-primary" name="edit_client" value="Salvesta"/>
                </td>
            </tr>
        </table>

    </fieldset>
</form>

<div class="span6">
    <form action="<?=URL::base()?>clients/edit/<?=$client->id?>" method="post">

        <!-- Defines the contact to remove from the object -->
        <input type="hidden" name="remove_contact_id"/>

        <table class="table float-box">
            <caption>Kontaktisikud</caption>
            <thead>
            <tr>
                <th>Nimi</th>
                <th>E-mail</th>
                <th>Telefon</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?=Helper_Contacts::entity_contacts_list('client', $client->id)?>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <select name="contact_id">
                        <?=Helper_Contacts::contact_options()?>
                    </select>
                </td>
                <td colspan="3">
                    <input type="submit" class="btn btn-primary" name="add_contact" value="Lisa kontaktisik"/>
                    <a class="btn" href="<?=URL::base()?>contacts" title="Lisa või eemalda kontakte">Aadressiraamat</a>
                </td>
            </tr>
            </tfoot>
        </table>

    </form>
</div>
<div class="clearfix"></div>