<?php defined('SYSPATH') or die('No direct script access.') ?>

<p><?=__('public.text.must_login')?><br/>
    <?=__('public.text.admin_creates_accounts')?></p>
<form action="<?=URL::base()?>public/auth/login" method="post" class="form-stacked">
    <label for="user"><?=__('Username')?>:</label>
    <input type="text" id="user" placeholder="Mihkel" maxlength="32" name="user" autofocus required/>

    <div class="clearfix"></div>

    <label for="pass"><?=__('Password')?>:</label>
    <input type="password" placeholder="salajane" maxlength="32" name="pass" id="pass" required/>

    <div class="clearfix"></div>
    <div class="controls">
        <label class="checkbox">
        <input type="checkbox" id="remember" name="remember" value="1"/>
            <?=__('Remember me')?>
        </label>
    </div>

    <div class="clearfix"></div>

    <? if (Kohana::$environment !== Kohana::PRODUCTION): ?>
    <a data-keyboard="true" class="btn btn-large btn-primary" data-controls-modal="modal-warning"
       data-backdrop="true">
        <?=__('Login')?>
    </a>


    <!-- Warning dialog about dev version -->
    <div id="modal-warning" class="modal hide fade">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Tähelepanu')?>
            </h3>
        </div>
        <div class="modal-body">
            <p>
                <?=__('Tegemist on arendusserveriga. See tähendab, et vigade esinemine on normaalne ja ülimalt tõenäoline.')?>
            </p>

            <p>
                <?=__('Samuti juhime tähelepanu, et kasutajate poolt loodud sisu (üleslaaditud failid, sisestatud andmed) säilimine
                    arendusserveris ei ole garanteeritud.')?>
            </p>

            <p>
                <?=__('Arendusserver on mõeldud programmi testimiseks. Igapäevaseks tööks tuleks kasutada <em>live</em> versiooni aadressil :link.', array(
                ':link' => HTML::anchor(
                    Kohana::$config->load('app.live_url'),
                    Kohana::$config->load('app.live_url')
                )
            ))?>
            </p>
        </div>
        <div class="modal-footer">
            <a onclick="$('.form-stacked').submit();" class="btn danger btn-large">
                <?=__('Olgu')?>
            </a>
        </div>
    </div>


    <? else: ?>
    <input type="submit" class="btn btn-large btn-primary" name="login" value="<?=__('Login')?>"/>
    <? endif?>
</form>