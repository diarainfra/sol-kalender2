<h1><?=__('Süsteemi logid')?></h1>

<table class="table tablesorter table-striped table-condensed">
    <colgroup></colgroup>
    <colgroup></colgroup>
    <colgroup></colgroup>
    <thead>
    <tr>
        <th class="header span1"><?=__('ID')?></th>
        <th class="header span2"><?=__('Loodud')?></th>
        <th class="header span2"><?=__('Looja')?></th>
        <th class="header span8"><?=__('Sisu')?></th>
    </tr>
    </thead>
    <tbody>
    <? if (!empty($logs)): ?>
        <? foreach ($logs as $log): ?>

        <tr>
            <td><?=$log->id?></td>
            <td><?=Date::localized_date($log->created, TRUE)?></td>
            <td><?=$log->user_name?></td>
            <td><?=$log->text?></td>
        </tr>
            <? endforeach ?>
        <? endif?>

    </tbody>
</table>

<div class="clearfix"></div>