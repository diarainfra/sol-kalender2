<h1><?=__('Seaded')?></h1>
<form action="<?=URL::base()?>settings" method="post">
    <h3><?=__('Üldine')?></h3>

    <div class="span6">
        <table class="table">
            <tbody>
            <tr>
                <td><?=__('Lepinguline juurdehindlus')?></td>
                <td>
                    <input type="text" placeholder="0.05" size="4" maxlength="4" class="span2" name="extra_price"
                           value="<?=Helper_Template::mysql_float($settings['extra_price'], 3)?>"/>
                </td>
            </tr>
            <tr>
                <td><?=__('Autosõidu km hind')?></td>
                <td>
                    <input type="text" placeholder="1.500" size="4" class="span2" maxlength="5" name="km_price"
                           value="<?=Helper_Template::mysql_float($settings['km_price'], 3)?>"/> €
                </td>
            </tr>
            <tr>
                <td><?=__('E-mail meeldetuletused töötajatele')?>
                    <span class="help-block"><?=__('Päeva enne töö algust')?></span>
                </td>
                <td>
                    <input type="text" placeholder="2" size="3" maxlength="3" class="span2" name="reminder_offset"
                           value="<?=$settings['reminder_offset']?>"/>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    <input class="btn btn-primary" type="submit" name="save_settings" value="<?=__('Salvesta')?>"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
<div class="clearfix"></div>
    <div class="span6">
        <h3><?=__('Arvealuse Kontod')?></h3>
        <table class="table">
            <tbody>
            <?if (!empty($accounts)): ?>
                <? foreach ($accounts as $account): ?>
                <tr>
                    <td><?=$account?></td>
                    <td>
                        <a href=<?=URL::base()?>settings/delete_account/<?=$account?>" class="btn danger">
                            <?=__('Kustuta')?>
                        </a>
                    </td>
                </tr>
                    <? endforeach ?>
                <? endif?>
            </tbody>
            <tfoot>
            <tr>
                <td><?=__('Lisa uus konto number')?></td>
                <td>
                    <input type="text" placeholder="40475" size="5" class="span2" maxlength="10" name="account_number"/>
                    <input type="submit" name="add_account" class="btn btn-primary" value="<?=__('Lisa')?>"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</form>