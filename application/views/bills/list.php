<?php if (!empty($object->name)): ?>
    <h1><?= __('Arved objektile :name', array(':name' => $object->name)) ?></h1>
    <ol>
        <?= Helper_Bills::list_prev_bills($object->id) ?>
    </ol>
<?php else : ?>
    <h1><?= __('Arveid ei ole') ?></h1>
<?php endif ?>

<div class="spacer"></div>
<a href="<?= URL::base() ?>bills" title="<?= __('Tagasi') ?>" class="btn"><?= __('Tagasi') ?></a>