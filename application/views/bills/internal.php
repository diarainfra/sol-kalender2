<!-- Sisearve -->
</form> <!-- End of filters form -->

<div id="report_area" style="padding: 2px;">
    <h2>(Sisearve) <?=Helper_Bills::bill_title(@$filters, (bool)@$_POST['confirm-internal-bill'])?></h2>

    <form action="" method="post"> <!-- Start of TJ/accounts form -->

        <table class="data-table float-box<?=(isset($_POST['confirm-internal-bill'])) ? ' transparent' : NULL?>">
            <caption>Arve esitaja</caption>
            <thead>
            <tr>
                <td>Vastutaja:</td>
                <th><?=$employee->name?></th>
                <td>Vastutaja kood:</td>
                <th><?=$employee->code?></th>
            </tr>
            <tr>
                <td>Objekt:</td>
                <th><?=$filters['object_name']?></th>
                <td>Objekti kood:</td>
                <th><?=$object->code?></th>
            </tr>
            <tr>
                <td>Teostaja:</td>
                <td colspan="3"><?=$employee->name?></td>
            </tr>
            <tr>
                <td>Konto:</td>
                <th>
                    <? if (isset($_POST['confirm-internal-bill'])):
                    echo $_POST['biller_account'];
                else:?>
                    <select name="biller_account">
                        <?=Helper_Settings::account_options($accounts)?>
                    </select>
                    <? endif?>
                </th>
                <td></td>
                <td>
                    <span class="small faint">(teostaja allkiri)</span>
                </td>
            </tr>
            </thead>
        </table>


        <table class="data-table float-box<?=(isset($_POST['confirm-internal-bill'])) ? ' transparent' : NULL?>">
            <caption>Arve saaja</caption>
            <thead>
            <tr>
                <td>Vastutaja:</td>
                <th><?=$object_employee->name?></th>
                <td>Vastutaja kood:</td>
                <th><?=$object_employee->code?></th>
            </tr>
            <tr>
                <td>Objekt:</td>
                <th><?=$filters['object_name']?></th>
                <td>Objekti kood:</td>
                <th><?=$object->code?></th>
            </tr>
            <tr>
                <td>Konto:</td>
                <th>
                    <? if (isset($_POST['confirm-internal-bill'])):
                    echo $_POST['billed_account'];
                else:?>
                    <select name="billed_account">
                        <?=Helper_Settings::account_options($accounts)?>
                    </select>
                    <? endif?>
                </th>
                <td></td>
                <td>
                    <span class="small faint">(töödejuhataja allkiri)</span>
                </td>
            </tr>
            </thead>
        </table>

        <div class="clearfix"></div>

        <table class="data-table<?=(isset($_POST['confirm-internal-bill'])) ? ' transparent' : NULL?>">
            <caption>Teostatud tööd</caption>
            <thead>
            <tr>
                <th>Töö nr.</th>
                <th>Kirjeldus</th>
                <th>Kuupäev</th>
                <th>Kogus</th>
                <th>Ühik</th>
                <th>Autokulud</th>
                <th>Kokku</th>
            </tr>
            </thead>
            <tbody>
            <?
            if ($tasks_data->count() != 0):
                foreach ($tasks_data as $task): ?>
                <tr>
                    <td><?=$task->id?></td>
                    <td style="max-width: 400px;"><?=Text::limit_words($task->description, '16', '...')?></td>
                    <td><?=date('m', strtotime($task->finished))?></td>
                    <td>
                        <?=($task->payment == 0) ? '1' : Helper_Template::pretty_time($task->time_elapsed)?>
                    </td>
                    <td><?=($task->payment == 0) ? 'tk' : 'tund'?></td>
                    <td><?=($task->car_billing && $task->car_mileage > 0) ? Model_Task::car_mileage($task->car_mileage).' €' : NULL?></td>
                    <td><?=($task->payment == 0) ? $task->unit_price
                            : Helper_Template::calc_sum($task->time_elapsed, $task->unit_price)?>
                        €
                    </td>
                </tr>
                    <? endforeach;
            endif;
            ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="6" style="text-align: right;">Kokku:</th>
                <th><?=Helper_Reports::sums($tasks_data, 'sum')?> €</th>
            </tr>
            </tfoot>
        </table>

        <table class="data-table<?=(isset($_POST['confirm-internal-bill'])) ? ' transparent' : NULL?>">
            <caption>Kasutatud materjalid ja allhanked</caption>
            <thead>
            <tr>
                <th>Töö nr.</th>
                <th>Kuupäev</th>
                <th>Nimetus</th>
                <th>Kogus</th>
                <th>Ühik</th>
                <th>Ühiku hind</th>
                <th>Kokku</th>
            </tr>
            </thead>
            <tbody>
            <?=Helper_Task::material_rows($tasks_in_range, FALSE, FALSE)?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="6" style="text-align: right;">Kokku:</td>
                <td><?=$materials_sum?> €</td>
            </tr>
            </tfoot>
        </table>

</div>
<div class="clearfix"></div>


<? if (!isset($pdf_checkpoint)): // Is set when PDF file is being generated ?>

<div class="spacer"></div>

<? if (isset($_POST['confirm-internal-bill'])): ?>
    </form> <!-- End of TJ/accounts form -->
    <a class="button" href="#" title="Prindi aruanne" id="print"><img src="<?=URL::base()?>assets/img/printer.png"
                                                                      alt="Prindi"/>Prindi arve
    </a>


    <a class="button" href="<?=URL::base()?>api/request_pdf?file=<?=$pdf_file?>" title="Laadi alla PDF">
        <img src="<?=URL::base()?>assets/img/file_pdf.png" alt="pdf" width="28" height="28"/>Laadi alla PDF
    </a>


    <a class="button" onclick="$('#email_report').dialog({width: 450});" title="Saada kirjana">
        <img src="<?=URL::base()?>assets/img/email.png" alt="e-mail"/>
        Saada arve e-postiga
    </a>

        <?= @$email_form ?>

    <? else: ?>
    <p>Tähelepanu! Arve kinnitamisel märgitakse need tööd, mis arves kajastuvad,
        staatusesse "Arve esitatud: Jah"<br/> ning neid enam uue arve koostamisel ei arvestata ehk iga töö kajastub vaid
        ühes arves.</p>
    <input type="submit" name="confirm-internal-bill" class="confirm-bill" title="Kinnita arve"
           value="Kinnita arve"/>
    </form> <!-- End of TJ/accounts form -->
    <? endif ?>

<? endif ?>
