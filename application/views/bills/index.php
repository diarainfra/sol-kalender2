<h1><?=__('Arve')?></h1>

<!-- Hide the calender in jQuery datepicker -->
<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            changeMonth: true,
            changeYear: true,
            changeDay: false,
            showButtonPanel: true,
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });
    });
</script>

<style type="text/css">
    .ui-datepicker-calendar {
        display: none;
    }
</style>

<div class="spacer"></div>
<div class="well">
    <form action="<?=URL::base()?>bills/view" method="GET">
        <!-- Filters form -->
        <table>
            <tr>
		  <td><?=__('Klient')?></td>
                <td><?=__('Objekt')?></td>
                <td><?=__('Üksik töö')?></td>
                <td><?=__('Periood')?></td>
		  <td></td>
            </tr>
            <tr>
                <td>
                    <select name="client"  id="clientlist" size="2">
			   <?=Helper_Clients::clients_select(@$_POST['client'])?>
                    </select>
                </td>
                <td>
                    <select name="object" id="objectlist" size="2">

                        <?=Helper_Objects::objects_select(@$_POST['object'])?>
                    </select>
                </td>
                <td>

                    <?=Form::select('task_id', Helper_Bills::list_unbilled_tasks(), @$filters['task_id'], array('id' => 'tasklist', 'size' => '2'))?>

                </td>
                <td>
                    <input type="text" size="10" class="date" maxlength="10" name="period" placeholder="01.01.2011"
                           value="<?=@$filters['period']?>"/>
                </td>
		              </tr>
        </table>
		<table>
			<tr>
				<td><input type="submit" class="btn" name="external_bill" value="<?=__('Koosta arve')?>"/></td>
                <td><input type="submit" class="btn" name="internal_bill" value="<?=__('Koosta sisearve')?>"/></td>
                <td><input type="submit" class="btn" value="<?=__('Vaata koostatud arveid')?>" name="bill_list"/></td>

			</tr>
		</table>
    </form>
    <!-- End of filters form -->
</div>

<!-- The report will appear here -->
<?= @$report ?>

<script type="text/javascript" src="<?=URL::base()?>assets/js/libs/jquery.jqprint.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#print').click(function() {
            $('#report_area').jqprint();
        });
    });


$("#clientlist").change(function(){
    var clid = $('#clientlist').val();


    $('#objectlist').each(function(){ //FOR EVERY SECOND OPTION
        $("option[cid]").each(function() {
            var o_cid = $(this).attr("cid");
	if(o_cid !== clid) { //CHECK IF IT IS EQUAL TO THE FIRST SELECTED CHOICE
        $(this).attr('style', 'display: none;'); //IF IT IS, HIDE IT

    } else {
        //OTHERWISE SHOW IT, INCASE HIDDEN FROM PREVIOUS CHOICE
	$(this).show();
    }
        });

        
    });

});
$("#objectlist").change(function(){
    var text = $( "#objectlist option:selected" ).text();
    $('#tasklist').each(function(){ //FOR EVERY SECOND OPTION
	$("#tasklist").children('option').hide();
	$("#tasklist").children('option:contains("'+ text +'")').css('display', '');
	$("#tasklist").width($("select").width()); $("#tasklist").width(500);

	});

});

</script>


