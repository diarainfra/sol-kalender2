<div class="clearfix"></div>

    <form action="" method="post"> <!-- Start of TJ/accounts form -->
    <br class="clear"/>

    <div id="report_area" style="padding: 2px;">

        <h2><?=Helper_Bills::bill_title(@$filters, (bool)@$_POST['confirm-bill'])?></h2>
        <br/>

        <!-- Arvealus -->
        <table class="data-table<?=(isset($_POST['confirm-bill'])) ? ' transparent' : NULL?>">
            <caption>Arvealus</caption>
            <thead>
            <tr>
                <td>TJ nimi:</td>
                <th>
                    <?=$object->employee_name ?>
                </th>
                <td style="text-align: right;">TJ kood:</td>
                <th colspan="2" style="text-align: left;"><?=$object->employee_code?></th>
                <td style="border-left: 1px solid #333;">Arve koostas:</td>
                <th><?=$user->name?></th>
            </tr>
            <tr>
                <th colspan="2" style="text-align: right;"><?=strtoupper(Helper_Template::month_name(date('n', strtotime($filters['period']))))?></th>
                <th colspan="3" style="text-align:left;"><?=date('Y', strtotime($filters['period']))?></th>
                <td style="border-left: 1px solid #333;">Kuupäev:</td>
                <th><?=date('d.m.Y')?></th>
            </tr>
            <tr style="border-top: 2px solid #333;">
                <td colspan="2">Kliendi number:</td>
                <th colspan="5" style="text-align: left;"><?=$client->code?></th>

            </tr>
            <tr>
                <td colspan="2">Juriidiline nimi:</td>
                <th colspan="5" style="text-align: left;"><?=$client->name?></th>
            </tr>
            <tr>
                <td colspan="2">Postiaadress:</td>
                <th colspan="5" style="text-align: left;"><?=$client->postal_address?></th>
            </tr>
            <tr style="border-top: 1px solid #333;">
                <td>Objekt</td>
                <td>Konto</td>
                <td class="nowrap">Teenuse või toote nimetus</td>
                <td>Kogus</td>
                <td>Ühik</td>
                <td>Ühiku hind</td>
                <td>Summa kokku</td>
            </tr>
            </thead>
            <tbody>


            <tr>
                <td><?=$object->code?></td>
                <td>
                    <? if (!isset($_POST['tasks_account'])): ?>
                    <select name="tasks_account"><?=Helper_Settings::account_options($accounts)?></select>
                    <? else:
                    echo $_POST['tasks_account'];
                endif;?>

                </td>
                <td>Lepinguvälised tööd kokku</td>
                <td>1</td>
                <td><?=((bool)@$filters['task_id']) ? 'Töö' : 'Kuu' ?></td>
                <td class="nowrap"> <?=$jobs_sum?>
                    €
                </td>
                <td class="nowrap"><?=$jobs_sum?> €</td>
            </tr>

            <tr>
                <td><?=$object->code?></td>
                <td>
                    <? if (!isset($_POST['tasks_account'])): ?>
                    <select name="materials_account_in"><?=Helper_Settings::account_options($accounts)?></select>
                    <? else:
                    echo $_POST['tasks_account'];
                endif; ?>
                </td>
                <td>Lepinguliste tööde kasutatud materjalid kokku</td>
                <td>1</td>
                <td><?=((bool)@$filters['task_id']) ? 'Töö' : 'Kuu' ?></td>
                <td><?=$materials_sum_in?> €</td>
                <td><?=$materials_sum_in?> €</td>
            </tr>

            <tr>
                <td><?=$object->code?></td>
                <td>
                    <? if (!isset($_POST['tasks_account'])): ?>
                    <select name="materials_account_out"><?=Helper_Settings::account_options($accounts)?></select>
                    <? else:
                    echo $_POST['tasks_account'];
                endif; ?>
                </td>
                <td>Lepinguväliste tööde kasutatud materjalid kokku</td>
                <td>1</td>
                <td><?=((bool)@$filters['task_id']) ? 'Töö' : 'Kuu' ?></td>
                <td><?=$materials_sum_out?> €</td>
                <td><?=$materials_sum_out?> €</td>
            </tr>

            <tr>
                <td><?=$object->code?></td>
                <td>
                    <? if (!isset($_POST['car_account'])): ?>
                    <select name="car_account"><?=Helper_Settings::account_options($accounts)?></select>
                    <? else:
                    echo $_POST['car_account'];
                endif; ?>
                </td>
                <td>Transpordikulud</td>
                <td><?=$car_sums['distance']?></td>
                <td>Km</td>
                <td><?=Helper_Settings::get('km_price')?> €</td>
                <td><?=$car_sums['price']?> €</td>
            </tr>

            </tbody>
            <tfoot>
            <tr>
                <td style="text-align: right;" colspan="6">Kokku:</td>
                <td><?=$total_sum?> €</td>
            </tr>
            </tfoot>
        </table>

    </div>
    <div class="clearfix"></div>


<? if (!isset($pdf_checkpoint)): // Is set when PDF file is being generated ?>
    <p class="help-block">Tähelepanu! Arve kinnitamisel märgitakse need tööd, mis arves kajastuvad,
        staatusesse "Arve esitatud: Jah"<br/> ning neid enam uue arve koostamisel ei arvestata ehk iga töö kajastub vaid
        ühes arves.</p>
    <p class="help-block">
        Arvesse <strong>ei kaasata</strong> lepingupõhiseid töid, küll aga lepingupõhistes töödes kasutatud materjalid
        ja
        allhanketööd.<br/>
        Transpordikulude real arvestatakse kokku nii lepingupõhiste kui -väliste tööde transpordikulud <br/>
        siis ja ainult siis, kui töö transpordikuludel on märge "Kaasa arvesse".
    </p>

    <div class="spacer"></div>

    <? if (isset($_POST['confirm-bill'])): ?>
    </form> <!-- End of TJ/accounts form -->
    <a class="btn" href="#" title="Prindi aruanne" id="print"><img src="<?=URL::base()?>assets/img/printer.png"
                                                                   alt="Prindi"/>Prindi arve
    </a>


    <a class="btn" href="<?=URL::base()?>api/request_pdf?file=<?=$pdf_file?>" title="Laadi alla PDF">
        <img src="<?=URL::base()?>assets/img/file_pdf.png" alt="pdf" width="28" height="28"/>Laadi alla PDF
    </a>

    <a data-keyboard="true" class="btn" data-controls-modal="modal-email" data-backdrop="true">
        <img src="<?=URL::base()?>assets/img/email.png" alt="e-mail"/>
        <?=__('Saada arve e-postiga')?>
    </a>

        <?= @$email_form ?>

    <? else: ?>
    <input type="submit" name="confirm-bill" class="btn btn-large btn-primary span6 confirm-bill" title="Kinnita arve"
           value="Kinnita arve ning vali edastusviis kliendile"/>
    </form> <!-- End of TJ/accounts form -->
    <? endif ?>

<? endif ?>

<div class="clearfix"></div><br/>
<script type="text/javascript">
    $(document).ready(function () {

        // Require confirmation when generating an empty (sum = 0) bill
        var second_confirm = 0;
        var total_sum = <?=$total_sum?>;
        $('input[name="confirm-bill"]').click(function () {
            if (total_sum <= 0) {
                if (second_confirm == 0) {
                    $.jGrowl('Arve kogusumma on 0! Jätkamiseks vajutage kinnitusnuppu veel korra.');
                    second_confirm = 1;
                    return false;
                }
            }
        });
    });
</script>