<h1><?=__('Töötaja andmete muutmine')?></h1>
<form action="<?=URL::base()?>employee/edit/<?=$employee->id?>" method="post">

    <input type="hidden" name="employee_id" value="<?=$employee->id?>"/>
    <fieldset>
        <table>
            <tr>
                <td>
                    ID:
                </td>
                <td>
                    #<?=$employee->id?>
                </td>
            </tr>
            <?if (!empty($employee->user_id)): ?>
            <tr>
                <td>
                    <?=__('Kasutajanimi')?>:
                </td>
                <td>
                    <?=$user->username?>
                </td>
            </tr>
            <? endif?>
            <tr>
                <td>
                    <?=__('Nimi')?>:
                </td>
                <td>
                    <input type="text" name="name" value="<?=$employee->name?>" size="20" maxlength="50"
                           placeholder="Peep Pagusalu" required/>
                </td>
            </tr>
            <?if (!empty($employee->user_id)): ?>
            <tr>
                <td>
                    <?=__('Parool')?>:
                </td>
                <td>
                    <input type="password" name="pass" value="" size="20" maxlength="50" placeholder="[peidetud]"/>
                    <span class="help-block"><?=__('Parooli muutmiseks sisesta siia uus parool.')?></span>
                </td>
            </tr>
            <? endif?>

            <tr>
                <td>
                    <?=__('Kood')?>:
                </td>
                <td>
                    <input type="text" name="code" value="<?=$employee->code?>" size="10" maxlength="20"
                           placeholder="3443"/>
                    <span class="help-block"><?=__('Töödejuhatajal on vajalik koodi olemasolu aruannete ja arvete genereerimiseks.')?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <?=__('E-mail')?>:
                </td>
                <td>
                    <input type="email" name="email" value="<?=$employee->email?>" size="10" maxlength="100"
                           placeholder="peep@gmail.com" required/>
                </td>
            </tr>
            <tr>
                <td><a class="btn" href="<?=URL::base()?>employee" title="<?=__('Tagasi')?>"><?=__('Tagasi')?></a></td>
                <td class="right">
                    <a class="btn danger"
                       href="<?=URL::base()?>employee/delete/<?=$employee->id?>"><?=__('Kustuta')?></a>
                    <input type="submit" class="btn btn-primary" name="edit" value="<?=__('Salvesta')?>"/>
                </td>
            </tr>
        </table>
    </fieldset>
</form>
<h1><?=__('Töötaja kalender')?></h1>
<div id="employee_calendar"></div>
<div class="clearfix"></div>