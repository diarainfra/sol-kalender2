<h1><?=__('Töötajate nimekiri')?></h1>
<a data-keyboard="true" class="btn" data-controls-modal="modal-new" data-backdrop="true">
    <?=__('Uus töötaja')?>
</a>

<div class="clearfix"></div>
<table class="table table-rounded table-condensed tablesorter span9 margin-center">
    <thead>
    <tr>
        <th><?=__('ID')?></th>
        <th><?=__('Nimi')?></th>
        <th><?=__('Kood')?></th>
        <th><?=__('E-mail')?></th>
    </tr>
    </thead>
    <tbody>
    <? if (!empty($employees)): ?>
        <? foreach ($employees as $employee): ?>
        <tr>
            <td><?=$employee->id?></td>
            <td>
                <a href="<?=URL::base()?>employee/edit/<?=$employee->id?>" title="<?=__('Muuda')?>">
                    <?=$employee->name?>
                </a>
            </td>
            <td><?=$employee->code?></td>
            <td><?=$employee->email?></td>
        </tr>
            <? endforeach ?>
        <? endif?>
    </tbody>
</table>


<!-- Dialog for creating a new employee -->
<div id="modal-new" class="modal hide fade">
    <form action="<?=URL::base()?>employee" class="form-stacked" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Uus töötaja')?>
            </h3>
        </div>
        <div class="modal-body">

            <label for="new-name"><?=__('Töötaja nimi')?>:</label>
            <input type="text" name="name" size="20" id="new-name" maxlength="50" placeholder="<?=__('Nimi')?>"
                   required autofocus/>

            <div class="clearfix"></div>

            <label for="new-email"><?=__('E-mail')?>:</label>
            <input type="text" name="email" size="20" maxlength="50" id="new-email" placeholder="e-mail" required/>

            <div class="clearfix"></div>

            <label for="new-code"><?=__('Kood')?>:</label>
            <input type="text" name="code" size="20" maxlength="15" placeholder="<?=__('Kood')?>" id="new-code"/>
            <span class="help-block"><?=__('Vajalik töödejuhatajal')?></span>

            <div class="clearfix"></div>

            <label for="new-pass"><?=__('Parool')?>:</label>
            <input type="password" name="pass" size="20" id="new-pass" maxlength="40" placeholder="<?=__('Parool')?>"/>

            <div class="clearfix"></div>

        </div>
        <div class="modal-footer">
            <input type="submit" value="<?=__('Lisa töötaja')?>" class="btn btn-large btn-primary"/>
        </div>
    </form>
</div>

<div class="clearfix"></div>