<h1><?=__('Kontakti andmed')?></h1>

<form action="<?=URL::base()?>contacts/edit/<?=@$contact->id?>" method="post">
    <input type="hidden" name="contact_id" value="<?=@$contact->id?>"/>
    <fieldset>
        <table>
            <thead></thead>
            <tbody>
            <tr>
                <td><?=__('Nimi')?>:</td>
                <td>
                    <input type="text" name="name" value="<?=@$contact->name?>" autofocus required/>
                </td>
            </tr>
            <tr>
                <td><?=__('E-mail')?>:</td>
                <td>
                    <input type="email" name="email" value="<?=@$contact->email?>"/>
                </td>
            </tr>
            <tr>
                <td><?=__('Telefon')?>:</td>
                <td>
                    <input type="text" name="phone" value="<?=@$contact->phone?>"/>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <a href="<?=URL::base()?>contacts" class="btn">
                        <?=__('Tagasi')?>
                    </a>
                </td>
                <td><input type="submit" class="btn btn-primary" name="edit_contact" value="<?=__('Salvesta')?>"/></td>
            </tr>
            </tfoot>
        </table>
    </fieldset>
</form>