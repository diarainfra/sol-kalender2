<h1><?=__('Kontaktide haldamine')?></h1>
<a href="<?=URL::base()?>contacts/edit" class="btn" title="<?=__('Lisa')?>"><?=__('Uus kontakt')?></a>
<br class="clear">

<table class="table tablesorter span10 margin-center">
    <thead>
    <tr>
        <th><?=__('ID')?></th>
        <th><?=__('Nimi')?></th>
        <th><?=__('E-mail')?></th>
        <th><?=__('Telefon')?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?=Helper_Contacts::contacts_list()?>
    </tbody>
</table>

<div class="clearfix"></div>