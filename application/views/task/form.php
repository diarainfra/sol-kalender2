<?= (!$new_task) ? '<h1>Töö #' . $p['id'] . ' muutmine</h1>' : '<h1>Uus töö</h1>' ?>

<? if (!$new_task && isset($p['repeating']) && $p['repeating']): ?>
<div class="right box" style="width: 250px;">
    <h2>Süsteemi käitumisest</h2><br/>
    <? if (isset($duplicate)): ?>
    See töö on duplikaat tööle <strong>#<?=$duplicate?></strong>. Duplikaattööd tekkivad, kui töö märgitakse korduvaks.
    Duplikaadi muutmisel muutub ainult ühe töö sisu. Seda tööd saab kustutada teisi mõjutamata.

    <? else: ?>

    See töö on korduv ning omab duplikaattöid. Töö muutmisel kirjutatakse üle kõikide duplikaatide muutused.
    Korduvuskuupäevade muutmisel lisatakse / kustutatakse duplikaattöid vastavalt vajadusele.
    Töö kustutamisel kustuvad ka kõik duplikaadid.
    <? endif?>

    Lisatud kommentaarid ilmuvad ainult selle töö juures.
</div>
<? endif ?>

<form action="<?=URL::base()?>task/<?=(!$new_task) ? 'edit/' . $p['id'] : 'new' ?>" method="post">

<fieldset>
<div class="span10">
<table class="table">
<tr>
    <td class="strong"><?=__('Algus')?></td>
    <td>
        <input type="text" size="16" class="datetime span2" maxlength="16" name="started"
               value="<?=Date::localized_date(@$p['started'], TRUE)?>" onclick="calc_real_work_time()" required/>
    </td>
</tr>

<tr>
    <td><?=__('Lõpp')?></td>
    <td>
        <input type="text" size="16" class="span2 datetime" maxlength="16" name="finished"
               value="<?=Date::localized_date(@$p['finished'], TRUE)?>"/><br/>
        <span class="help-block">Töö lõpetamise aeg peab olema ajaliselt suurem töö alguse ajast</span>
    </td>
</tr>


<tr>
    <td><?=__('Hinnatud ajakulu')?></td>
    <td>
        <input type="text" size="6" maxlength="5" class="span1" name="time_estimate"
               value="<?=Helper_Template::pretty_time(@$p['time_estimate'])?>"/><br/>
        <span class="help-block">Sisesta aeg formaadis HH:mm (01:05 on üks tund ja viis minutit)</span>
    </td>
</tr>

<tr>
    <td><?=__('Tegelik ajakulu')?></td>
    <td>
        <input type="text" size="6" maxlength="5" class="span1" name="time_elapsed"
               value="<?=Helper_Template::pretty_time(@$p['time_elapsed'])?>"/><br/>
        <span class="help-block">Tegelik ajakulu (1 töötaja kohta). Sisesta aeg formaadis HH:mm (01:05 on üks tund ja viis minutit)</span>
    </td>
</tr>

<? if (!$new_task && !isset($duplicate)): ?>

<tr class="new_section">
    <td>Korduvus</td>
    <td>
        <label class="checkbox inline">
            <?=Form::checkbox('repeating', '1', (bool)@$p['repeating'])?>
            <?=__('Töö on korduv')?>
        </label>
    </td>
</tr>

<tr>
    <td>Korda iga...</td>
    <td>
        <?=Form::select('repeat_day[]', array_merge(array('*' => 'Kuupäev - Kõik'), Date::days(5)),
        (!isset($_POST['repeat_day'])) ? Model_Recurring::cron_decode($p['repeat_expression'], 'day') : @$p['repeat_day'],
        array('size' => 8, 'multiple', 'class' => 'span3'))?>

        <?=Form::select('repeat_month[]', array_merge(array('*' => 'Kuu - Kõik'), Date::months()),
        (!isset($_POST['repeat_month'])) ? Model_Recurring::cron_decode($p['repeat_expression'], 'month') : @$p['repeat_month'],
        array('size' => 8, 'multiple', 'class' => 'span3'))?>

        <?=Form::select('repeat_day_of_week[]', array_merge(array('*' => 'Päev - Kõik'), Helper_Template::day_name()),
        (!isset($_POST['repeat_day_of_week'])) ? Model_Recurring::cron_decode($p['repeat_expression'], 'day_of_week') : @$p['repeat_day_of_week'],
        array('size' => 8, 'multiple', 'class' => 'span3'))?>
    </td>
</tr>
<tr>
    <td>
        <?=__('Korda tööd kuni...')?>
    </td>
    <td>
        <input type="text" size="11" class="date span2" maxlength="11" name="repeat_until"
               value="<?=Date::localized_date(@$p['repeat_until'])?>"/><br/>
    </td>
</tr>
    <? endif ?>

<tr class="new_section">
    <td>Klient</td>
    <td><select name="client">
        <option value="0">[Kõik]</option>
        <?=Helper_Clients::clients_select(@$p['client'])?>
    </select><br/>
        <span class="help-block">Seda rippmenüüd kasutatakse ainult objektide filtreerimiseks.</span>
    </td>
</tr>

<tr>
    <td class="strong">Objekt</td>
    <td id="objects_td">
        <select name="object" required>
            <option value=""></option>
            <?=Helper_Objects::objects_select((isset($p['object'])) ? $p['object'] : @$p['object_id'])?>
        </select>
    </td>
</tr>

<tr>
    <td>Teenus</td>
    <td>
        <select name="service">
            <?=Helper_Services::service_options(@$p['service'])?>
        </select>
    </td>
</tr>

<tr class="new_section">
    <td class="strong">Töötajad</td>
    <td>
        <input type="text" class="employee-suggest" name="employees" size="30"/>
        <span class="help-block">Valikust saad valida (vähemalt) ühe või mitu tööga seotud töötajat.</span>
    </td>
</tr>

<tr>
    <td class="strong">Teostatud töö lühikirjeldus</td>
    <td>
        <textarea rows="8" cols="40" maxlength="300" class="span10" name="description"
                  required placeholder="Kirjeldus"><?=@$p['description']?></textarea>
    </td>
</tr>

<tr class="new_section">
    <td><?=__('Lepingupõhine töö?')?></td>
    <td>
        <div class="control-group">
            <label class="radio">
                <?=Form::radio('payment', 0, !(bool)@$p['payment'], array('id' => 'contract-based'))?>
                <?=__('Jah')?>
            </label>
            <label class="radio">
                <?=Form::radio('payment', 1, (bool)@$p['payment'], array('id' => 'outside-contract'))?>
                <?=__('Ei')?>
            </label>
        </div>
    </td>
</tr>
<tr>
    <td><?=__('Sisearve?')?></td>
    <td>
        <label class="checkbox inline">
            <?=Form::checkbox('internal_bill', 1, (bool)@$p['internal_bill'], array('id' => 'internal_bill'))?>
            <?=__('Jah')?>
        </label>

        <p class="help-block">Kui see valik on märgitud, läheb töö sisearvete alla.</p>
    </td>
</tr>

<tr>
    <td>Tunni hind</td>
    <td><input type="text" class="span1"
               value="<?=(empty($p['unit_price'])) ? '0' : $p['unit_price']?>" size="5"
               name="unit_price"/>€<br/>
                    <span class="help-block">
                        Tunnihinna järgi arvutatakse lepinguväliste tööde koguhind.
                        Kui tegemist on lepingupõhise sisearvega,
                        tuleb tunnihinnaks kirjutada antud töö <strong>koguhind</strong>
                        (ehk hinna arvutamisel töö kestust ei arvestata).
                    </span>
    </td>
</tr>

<? if (!$new_task): ?>
<tr>
    <td>Autokulud</td>
    <td>
        <div class="input">
            <div class="input-prepend">
                <label class="add-on">
                    <?=Form::checkbox('car_billing', 1, (!empty($p['car_billing'])), array('id' => 'car_billing'))?>
                </label>

                <?=Form::input('car_mileage',
                (empty($p['car_mileage'])) ? '0' : $p['car_mileage'],
                array(
                    'placeholder' => 0,
                    'size' => 5,
                    'maxlength' => 8,
                    'class' => 'span1'))?>km

                <?=Form::input('car_time',
                Helper_Template::pretty_time((empty($p['car_time'])) ? 0 : $p['car_time']),
                array(
                    'placeholder' => '00:40',
                    'size' => 6,
                    'class' => 'span1'
                ))?> h

            </div>
        </div>
                    <span class="help-block">
                        Autoga läbitud vahemaa kilomeetrites ning selleks kulunud aeg.
                        Autokulu lisatakse arvesse vaid siis, kui linnuke on märgitud.
                    </span>
    </td>
</tr>

    <? endif ?>

<tr>
    <td><a class="btn" href="<?=URL::base()?>calendar" title="">Tagasi</a></td>
    <td>
        <input type="hidden" name="id" value="<?=(!$new_task) ? $p['id'] : 0?>"/>
        <? if (!isset($p['billed']) || $p['billed'] == 0): ?>
        <button type="submit" name="<?=(!$new_task) ? 'save_task' : 'add_task'?>" class="btn btn-primary right"
               value="<?=(!$new_task) ? 'Salvesta' : 'Lisa'?>"/><?=(!$new_task) ? 'Salvesta' : 'Lisa'?></button>
        <? endif ?>
    </td>
</tr>

</table>
</div>
</fieldset>

</form>

<script type="text/javascript">

    function reload_objects() {
        $.post(<?=URL::base()?> +'api/client_objects', {
            client_id:$('select[name="client"]').val()
        }, function (html) {
            $('#objects_td').html(html);
        });
    }

    // Load new object values when client changes
    $('select[name="client"]').change(function () {
        reload_objects();
    });

    $(document).ready(function () {
        // Names
        $('.employee-suggest').tokenInput(<?=URL::base()?> +'api/employee_search',
                { method:'post',
                    preventDefault:true,
                    preventDuplicates:true,
                    searchingText:"Otsin...",
                    noResultsText:"Ei leitud",
                    hintText:"Kirjuta töötaja nimi...",
                    prePopulate: <?=(!empty($tokenizer)) ? $tokenizer : 'null'?>,
                    tokenDelimiter:'+',
                    classes:{
                        tokenList:"token-input-list-facebook",
                        token:"token-input-token-facebook",
                        tokenDelete:"token-input-delete-token-facebook",
                        selectedToken:"token-input-selected-token-facebook",
                        highlightedToken:"token-input-highlighted-token-facebook",
                        dropdown:"token-input-dropdown-facebook",
                        dropdownItem:"token-input-dropdown-item-facebook",
                        dropdownItem2:"token-input-dropdown-item2-facebook",
                        selectedDropdownItem:"token-input-selected-dropdown-item-facebook",
                        inputToken:"token-input-input-token-facebook"
                    }
                });
    });

    $(document).ready(function () {

        var get_date = function(elem) {
            var val = elem.val();
            var day = val.slice(0, 2);
            var month =val.slice(3, 5);
            var year =val.slice(6, 10);
            var hours = val.slice(11,13);
            var minutes = val.slice(14,16);
            return new Date(year, month, day, hours, minutes);
        };
        var msToTime = function (s) {
            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;
            if (mins < 10) {
                mins = "0" + mins;
            }
            return hrs + ':' + mins;
        };

        var calc_real_work_time = function () {
            var e_started = $('input[name="started"]');
            var e_finished = $('input[name="finished"]');
            var e_elapsed = $('input[name="time_elapsed"]');
            var e_save = $('button[type="submit"]');
            var started = get_date(e_started);
            var finished = get_date(e_finished);

            if (started < finished) {
                e_elapsed.val(msToTime(finished - started));
                e_finished.parent().removeClass("control-group error");
                e_started.parent().removeClass("control-group error");
                e_save.removeClass("disabled");
            } else {
                e_elapsed.val("0:00");
                e_finished.parent().addClass("control-group error");
                e_started.parent().addClass("control-group error");
                e_save.addClass("disabled");
            }
        };

        $('input[name="started"]').change(function () {
            calc_real_work_time()
        });

        $('input[name="finished"]').change(function () {
            calc_real_work_time()
        });

        $('select[name="client"]').val($('select[name="object"]').find(":selected").attr("cid"));

        if ($("div.alert-error")) {
            calc_real_work_time();
        }

    });


</script>