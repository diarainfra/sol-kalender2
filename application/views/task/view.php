<h1><?=__('Töö')?> #<?=$task->id?> <?=Text::limit_chars($task->description, 50, '...')?></h1>

<!-- Tab header -->
<ul class="nav nav-tabs" data-pills="tabs">
    <li class="active"><a href="#tab-info"><?=__('Andmed')?></a></li>
    <li><a href="#tab-materials"><?=__('Materjalid')?> (<?=count(Model_Task::used_materials($task->id))?>)</a></li>
    <li><a href="#tab-comments"><?=__('Kommentaarid')?> (<?=count($comments)?>)</a></li>
    <li><a href="#tab-logs"><?=__('Ajalugu')?> (<?=count($logs)?>)</a></li>
</ul>
<!-- End tabbed header -->

<div class="tab-content">

    <!-- START General tab -->
    <div id="tab-info" class="active tab-pane">
        <?=View::factory('task/view/general', array(
        'object' => $object,
        'service' => $service,
        'employees' => $employees,
        'employees_count' => $employees_count
    ))?>
    </div>
    <!-- START General tab -->

    <!-- START Materials used during the task -->
    <div id="tab-materials" class="tab-pane">
        <?=View::factory('task/view/materials', array(
        'materials' => Model_Task::used_materials($task->id)
    ))?>
    </div>
    <!-- END Materials used during the task -->

    <!-- START Comments tab -->
    <div id="tab-comments" class="tab-pane">
        <?=View::factory('task/view/comments', array(
        'comments' => $comments
    ))?>
    </div>
    <!-- END Comments tab -->

    <!-- Logs tab -->
    <div id="tab-logs" class="tab-pane">
        <!-- START Logs tab -->
        <?=View::factory('task/view/logs', array(
        'logs' => $logs
    ))?>
        <!-- START Logs tab -->
    </div>

</div>
<!-- End tabs -->

<!-- Delete modal -->
<div id="modal-task-delete" class="modal hide fade">
    <div class="modal-header">
        <a href="#" class="close">&times;</a>

        <h3><?=__('Töö kustutamine')?></h3>
    </div>
    <div class="modal-body">
        <p>
            <?=__('Kas olete kindel, et soovite selle töö kustutatuks märkida?')?>
        </p>
    </div>
    <div class="modal-body">
        <a class="btn danger" href="<?= URL::base() ?>task/delete/<?= $task->id ?>"
           title="Kustata"><?=__('Kustuta')?></a>
    </div>
</div>

<!-- Repeating tasks modal dialog -->
<? if ($recurring_tasks->count()): ?>
<div id="modal-task-duplicates" class="modal hide fade">
    <div class="modal-header">
        <a href="#" class="close">&times;</a>

        <h3><?=__('Töö duplikaadid')?></h3>
    </div>
    <div class="modal-body">
        <?=__('See töö on duplikaat tööle :original_id ning tema duplikaadid on tööd', array(
        ':original_id' => HTML::anchor('task/view/' . ($task->duplicate_of ? $task->duplicate_of : $task->id), '#' . ($task->duplicate_of ? $task->duplicate_of : $task->id))
    ))?>
        <? foreach ($recurring_tasks as $r_task): ?>
        <a href="<?=URL::base()?>task/view/<?=$r_task->id?>">#<?=$r_task->id?></a>;&nbsp;
        <? endforeach?>
    </div>
    <div class="modal-footer"></div>
</div>
<? endif ?>

<div class="clearfix"></div>