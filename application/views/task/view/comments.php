<form action="<?=URL::base()?>task/view/<?=$task->id?>" method="post">
    <div class="span10">
        <table class="table table-condensed span12">
            <thead>
            <tr>
                <th><?=__('Kuupäev')?></th>
                <th><?=__('Lisaja')?></th>
                <th><?=__('Sisu')?></th>
                <th><?=__('Kustuta')?></th>
            </tr>
            </thead>
            <tbody>
            <?=Helper_Task::comments_list($comments)?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4">
                    <textarea rows="5" cols="50" class="span12" name="comment"
                              placeholder="Kommentaari sisu"></textarea>
                    <input type="hidden" name="task_id" value="<?=$task->id?>"/>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: right;">
                    <input type="submit" name="add_comment" class="btn" value="Lisa kommentaar"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</form>