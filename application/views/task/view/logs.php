<table class="table table-condensed span12">
    <thead>
    <tr>
        <th class="header"><?=__('Kuupäev')?></th>
        <th class="header"><?=__('Lisaja')?></th>
        <th class="header"><?=__('Sisu')?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <? if ($logs->count()):
        foreach ($logs as $log): ?>
        <tr>
            <td><?=Date::localized_date($log->created, TRUE)?></td>
            <td><?=$log->user_name?></td>
            <td><?=$log->text?></td>
        </tr>
            <? endforeach;
    else:?>
    <tr>
        <td colspan="3">
            <?=__('0 sissekannet')?>
        </td>
    </tr>
        <? endif ?>
    </tbody>
</table>