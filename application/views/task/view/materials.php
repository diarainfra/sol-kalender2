<table class="table table-striped table-condensed span12" id="materialtable">
    <thead>
    <tr>
        <th><?=__('Töö nr.')?></th>
        <th><?=__('Kuup')?></th>
        <th><?=__('Nimetus')?></th>
        <th><?=__('Kogus')?></th>
        <th><?=__('Ühik')?></th>
        <th><?=__('Ühiku hind')?></th>
        <th><?=__('Kokku')?></th>
        <th><?=__('Muuda')?></th>
        <th><?=__('Kustuta')?></th>
    </tr>
    </thead>
    <? if (count($materials)): ?>
    <? foreach ($materials as $material): ?>
    <tr class="material_id" id="change" mid="<?=$material->id?>" tid="<?=$material->task_id?>">
        <td><?=HTML::anchor('task/view/' . $material->task_id, '#' . $material->task_id)?></td>
        <td><?=date('d.m', strtotime($material->date))?></td>
        <td><?=$material->name?></td>
        <td><?=$material->quantity?></td>
        <td><?=$material->unit?></td>
        <td><?=$material->unit_price?></td>
        <td><?=round($material->quantity * $material->unit_price, 2)?> €</td>
        <td>
            <? if (!$material->billed): ?>
		<a  data-controls-modal="modal-edit-materials" data-backdrop="true" data-keyboard="true class="m_b" id="testikas" mid="<?=$material->id?>">
   		 <?=__('Muuda')?>

		</a>
            <? endif?>
        </td>
        <td>
            <? if (!$material->billed): ?>
            <a href="<?=URL::base()?>task/delete_material/<?=$material->id?>">
                <?=__('Kustuta')?>
            </a>
            <? endif?>
        </td>
        <? endforeach ?>
    <? else: ?>
    <tr>
        <td colspan="9">
            <?=__('Kasutatud materjale sellel perioodil ei ole.')?>
        </td>
    </tr>
    <? endif?>
</table>
<a data-controls-modal="modal-add-materials" data-backdrop="true" data-keyboard="true" class="btn">
    <?=__('Lisa materjal')?>
</a>


<!-- Add task materials modal start -->
<div id="modal-add-materials" class="modal hide fade">
    <form action="<?= URL::base() ?>task/add_materials" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3><?=__('Materjalide lisamine')?></h3>
        </div>
        <div class="modal-body">

            <table>
                <tr>
                    <td>Nimetus</td>
                    <td>
                        <input type="text" size="30" maxlength="255" placeholder="Metallkruvi"
                               name="name" autofocus/>
                    </td>
                </tr>
                <tr>
                    <td>Kogus</td>
                    <td>
                        <input type="text" size="6" step="1" min="0" placeholder="1" value="1" maxlength="8"
                               name="quantity"/>
                    </td>
                </tr>
                <tr>
                    <td>Ühik</td>
                    <td>
                        <input type="text" size="10" maxlength="10" placeholder="tk" name="unit"/>
                    </td>
                </tr>
                <tr>
                    <td>Ühiku hind</td>
                    <td>
                        <input type="text" size="10" maxlength="20" placeholder="0" name="unit_price"/>
                        €
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="task_id" value="<?= $task->id ?>"/>
            <? if ($task->billed == 0): ?>
            <input type="submit" class="btn btn-large btn-primary" name="add_materials" value="Lisa"/>
            <? else: ?>
            <input type="reset" class="btn btn-danger" name="add_materials" value="Lisamine keelatud"
                   disabled/>
            <p class="help-block">Kuna töö eest on juba arve esitatud, ei saa sellele enam materjale
                lisada.</p>
            <? endif; ?>
        </div>
    </form>
</div>
<!-- End add materials modal -->

<!-- edit task materials modal start -->
<div id="modal-edit-materials" class="modal hide fade">
<script>

	$( "tr td a#testikas" ).click(function() {
  		var mid = $(this).closest('tr').attr('mid');
  		var tid = $(this).closest('tr').attr('tid');
		 $('#material_form').attr('action', '<?= URL::base() ?>task/edit_materials/' + mid);
		 $('#material_id').attr('value', mid);
		 $('#task_id').attr('value', tid);
	});


</script>
<form id="material_form" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3><?=__('Materjalide muutmine')?></h3>
        </div>
        <div class="modal-body">

            <table>
                <tr>
                    <td>Nimetus</td>
                    <td>
                        <input type="text" size="30" maxlength="255" placeholder="Metallkruvi"
                               name="name" autofocus/>
                    </td>
                </tr>
                <tr>
                    <td>Kogus</td>
                    <td>
                        <input type="number" size="6" step="1" min="0" placeholder="1" value="1" maxlength="8"
                               name="quantity"/>
                    </td>
                </tr>
                <tr>
                    <td>Ühik</td>
                    <td>
                        <input type="text" size="10" maxlength="10" placeholder="tk" name="unit"/>
                    </td>
                </tr>
                <tr>
                    <td>Ühiku hind</td>
                    <td>
                        <input type="text" size="10" maxlength="20" placeholder="0" name="unit_price"/>
                        €
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="material_id" id="material_id"/>
		<input type="hidden" name="task_id" id="task_id"/>
            <? if ($task->billed == 0): ?>
            <input type="submit" class="btn btn-large btn-primary" name="edit_materials" value="Muuda"/>
            <? else: ?>
            <input type="reset" class="btn btn-danger" name="edit_materials" value="Muutmine keelatud"
                   disabled/>
            <p class="help-block">Kuna töö eest on juba arve esitatud, ei saa selle t�� materjale enam muuta.</p>
            <? endif; ?>
        </div>
    </form>
</div>
<!-- End add materials modal -->

