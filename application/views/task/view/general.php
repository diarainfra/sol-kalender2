<div class="span8">
    <table class="table table-striped span10">
        <tbody>
        <tr>
            <th class="span2">ID:</th>
            <td><?=$task->id?></td>
        </tr>
        <? if ($task->duplicate_of): ?>
        <tr>
            <th>Orginaal</th>
            <td>
                <a href="<?=URL::base()?>task/view/<?=$task->duplicate_of?>">Töö #<?=$task->duplicate_of?></a>
            </td>
        </tr>
            <? endif ?>
        <tr>
            <th>Algus:</th>
            <td> <?=Date::localized_date($task->started, TRUE)?></td>
        </tr>
        <tr>
            <th>Lõpp:</th>
            <td> <?=Date::localized_date($task->finished, TRUE)?></td>
        </tr>
        <tr>
            <th>Kordub</th>
            <td>
                <? if (!$task->repeating): ?>
                Ei
                <? else: ?>
                Kuni <?= Date::localized_date($task->repeat_until) ?>
                <br/>
                <a data-controls-modal="modal-task-duplicates" data-backdrop="true" data-keyboard="true"
                   class="small clickable">
                    (kordub kokku <?=$recurring_tasks->count()?> korda)
                </a>
                <span class="help-block"><?=__('Töö duplikaatide vaatamiseks vajutage lingile.')?></span>
                <? endif ?>
            </td>
        </tr>
        <? if (!empty($task->time_estimate)): ?>
        <tr>
            <th>Hinnatud ajakulu:</th>
            <td><?=Helper_Template::pretty_time($task->time_estimate)?></td>
        </tr>
            <? endif;?>
        <tr>
            <th>Tegelik ajakulu:</th>
            <td><?=Helper_Template::pretty_time($task->time_elapsed * $employees_count)?></td>
        </tr>
        <tr>
            <th>Objekt:</th>
            <td>
                <a href="<?=URL::base()?>objects/edit/<?=$object->id?>"><?=$object->name?></a>
            </td>
        </tr>
        <tr>
            <th>Teenus:</th>
            <td><a href="<?=URL::base()?>services/edit/<?=$service->id?>"><?=$service->name?></a></td>
        </tr>
        <tr>
            <th>Piirkond:</th>
            <td><a href="<?=URL::base()?>objects"><?=$object->area_name?></a></td>
        </tr>
        <tr>
            <th>Töötajad:</th>
            <td> <?=$employees?></td>
        </tr>
        <tr>
            <th>Kirjeldus:</th>
            <td> <?=$task->description?></td>
        </tr>
        <tr>
            <th>Lepingupõhine?</th>
            <td> <?=($task->payment == 0) ? 'Jah' : 'Ei'?></td>
        </tr>
        <? if ($task->payment == 1): ?>
        <tr>
            <th>Tunni hind:</th>
            <td><?=$task->unit_price?>€</td>
        </tr>
            <? endif; ?>
        <? if ($task->car_mileage > 0): ?>
        <tr>
            <th>Auto kulu</th>
            <td><?=$task->car_mileage?>km,
                <?=Helper_Template::pretty_time($task->car_time)?>h
                (<?=Model_Task::car_mileage($task->car_mileage)?> €);
                Arve: <?=((bool)$task->car_billing) ? 'Jah' : 'Ei'?>
            </td>
        </tr>
            <? endif ?>

        <tr>
            <th>Arve esitatud?</th>
            <td><?=($task->billed == 1) ? 'Jah' : 'Ei' ?></td>
        </tr>
        <tr>
            <th>Sisearve?</th>
            <td><?=($task->internal_bill == 1) ? 'Jah' : 'Ei' ?></td>
        </tr>
<?if(!empty($material)){foreach($material as $mat){
	if(!empty($mat['name'])){?>
        <tr> 
           <th>Materjalid</th>
	

            <td>
<?if(!empty($material)){foreach($material as $mat){?>
		Materjal: <?=$mat['name']?><br>
		Kogus: <?=$mat['quantity']?><br>
		&Uuml;hik: <?=$mat['unit']?><br>
		&Uuml;hiku hind: <?=$mat['unit_price']?><br>
		Kokku: <?=$mat['quantity'] * $mat['unit_price'];?><br><br>
	<?}}?>
		</td>

        </tr>
<?}}}?>

        </tbody>
    </table>

    <a class="btn" href="<?=URL::base()?>calendar" title="Tagasi">Tagasi</a>
    <? if (!$task->billed): ?>
    <a data-controls-modal="modal-task-delete" data-backdrop="true" data-keyboard="true" class="btn danger">
        <?=__('Kustuta')?>
    </a>
    <a class="btn btn-primary" href="<?=URL::base()?>task/edit/<?=$task->id?>" title="Muuda">Muuda</a>
    <? endif ?>
</div>