<? /*
  * Template for upcoming task notification.
  * Employees will get this e-mail when they have
  * a task in the near future.
  */
?>
<div style="font-family: Arial; font-size: 12px;">
    <p style="font-size: 12px; color: #444; font-style: italic;">
        See on Kalenderrakenduse poolt saadetud automaatne teade<br/>
        teavitamaks Teid lähitulevikku planeeritud tööst.
    </p>

    <h3 style="font-size: 16px;">Töö #<?=$task->id?> andmed</h3>
    <table>
        <tbody>
        <tr>
            <th>Objekt:</th>
            <td><?=$object->name?></td>
        </tr>
        <tr>
            <th>Kontaktisik(ud):</th>
            <td>
                <? if (!empty($contacts)):
                foreach ($contacts as $contact):
                    echo $contact->name . ' ( ' . $contact->phone . ' )<br />';
                endforeach;
            endif ?>
            </td>
        </tr>
        <tr>
            <th>Algus:</th>
            <td><?=Date::localized_date($task->started, TRUE)?></td>
        </tr>

        <? if (Helper_Task::date_set($task->finished)): ?>
        <tr>
            <th>Lõpp:</th>
            <td><?=Date::localized_date($task->finished, TRUE)?></td>
        </tr>
        <? endif?>

        <tr>
            <th>Lisainfo:</th>
            <td><?=$task->description?></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="2">
                <a href="<?=URL::base()?>task/view/<?=$task->id?>" title="Kalenderrakendus">Link kalendrisse</a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>