<h1><?=__('Teenuse andmed')?></h1>
<fieldset class="span4">
    <form action="<?=URL::base()?>services/edit/<?=$service->id?>" method="post">
        <table>
            <tr>
                <td><?=__('Nimi')?></td>
                <td><input type="text" size="30" maxlength="100" name="name" value="<?=$service->name?>" autofocus/>
                </td>
            </tr>

            <tr>
                <td><?=__('Kood')?></td>
                <td><input type="text" size="10" maxlength="15" name="code" value="<?=$service->code?>"/></td>
            </tr>

            <tr>
                <td>
                    <input type="hidden" name="id" value="<?=$service->id?>"/>
                    <a href="<?=URL::base()?>services/" class="btn"><?=__('Tagasi')?></a>
                </td>
                <td>
                    <a href="<?=URL::base()?>services/delete/<?=$service->id?>" class="btn danger"><?=__('Kustuta')?></a>
                    <input type="submit" class="right btn btn-primary" value="<?=__('Salvesta')?>"/>
                </td>
            </tr>
        </table>
    </form>
</fieldset>