<h1><?=__('Teenuste nimekiri')?></h1>

<a data-keyboard="true" class="btn" data-controls-modal="modal-new" data-backdrop="true">
    <?=__('Uus teenus')?>
</a>

<div class="clearfix"></div>

<!-- List all services -->
<table class="table margin-center tablesorter span8 table-condensed table-striped">
    <thead>
    <tr>
        <th><?=__('ID')?></th>
        <th><?=__('Nimi')?></th>
        <th><?=__('Kood')?></th>
    </tr>
    </thead>
    <tbody>
    <?if (!empty($services)): ?>
        <? foreach ($services as $service): ?>

        <tr>
            <td><?=$service->id?></td>
            <td>
                <a href="<?=URL::base()?>services/edit/<?=$service->id?>" title="<?=__('Muuda')?>">
                    <?=$service->name?>
                </a>
            </td>
            <td><?=$service->code?></td>
        </tr>
            <? endforeach ?>
        <? endif?>
    </tbody>
</table>

<!-- Dialog for creating a new service -->
<div id="modal-new" class="modal hide fade">
    <form action="<?=URL::base()?>services" class="form-stacked" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Uus teenus')?>
            </h3>
        </div>
        <div class="modal-body">

            <label for="new-name"><?=__('Teenuse nimi')?>:</label>
            <input type="text" name="name" size="20" maxlength="100" id="new-name" placeholder="Nimi"/>

            <div class="clearfix"></div>

            <label for="new-code"><?=__('Kood')?>:</label>
            <input id="new-code" type="text" name="code" size="10" maxlength="10" placeholder="Kood"/>

            <div class="clearfix"></div>

        </div>
        <div class="modal-footer">
            <input type="submit" value="<?=__('Lisa teenus')?>" class="btn btn-large btn-primary"/>
        </div>
    </form>
</div>

<div class="clearfix"></div>