<h1><?=__('Objekti ":name" muutmine', array(':name' => $object->name))?></h1>


<form action="<?=URL::base()?>objects/edit/<?=$object->id?>" class="span8" method="post">
    <fieldset>
        <legend><?=__('Objekti andmed')?></legend>
        <table>
            <tr>
                <td><?=__('Nimi')?></td>
                <td><input type="text" size="15" maxlength="80" name="name" value="<?=$object->name?>"
                           autofocus/></td>
            </tr>
            <tr>
                <td><?=__('Klient, kelle alla objekt kuulub')?></td>
                <td>
                    <select name="client_id">
                        <?=Helper_Clients::clients_select($object->client_id)?>
                    </select>

                    <p class="small">
                        <a href="<?=URL::base()?>clients/edit/<?=$object->client_id?>"
                           title="Kliendi detailvaade"><?=__('Vaata kliendi andmeid')?></a>
                    </p>
                </td>
            </tr>
            <tr>
                <td><?=__('Kood')?></td>
                <td><input type="text" size="10" maxlength="15" name="code" value="<?=$object->code?>"/>
                </td>
            </tr>
            <tr>
                <td><?=__('Piirkond')?></td>
                <td>
                    <?=Helper_Objects::area_select($object->area)?>
                </td>
            </tr>
            <tr>
                <td><?=__('Töödejuhataja')?></td>
                <td><select name="employee_id">
                    <option value=""></option>
                    <?=Helper_Employee::employee_options($object->employee_id, TRUE)?>
                </select></td>
            </tr>
            <tr>
                <td><input type="hidden" name="id" value="<?=$object->id?>"/>
                    <a href="<?=URL::base()?>objects" title="Objektid" class="btn"><?=__('Tagasi')?></a>
                    <a href="<?=URL::base()?>objects/delete/<?=$object->id?>" title="<?=__('Kustuta')?>"
                       class="btn danger"><?=__('Kustuta')?></a>
                </td>
                <td><input type="submit" class="right btn btn-primary" name="edit_object" value="<?=__('Salvesta')?>"/></td>
            </tr>
        </table>

    </fieldset>
</form>

<div class="clearfix"></div>



<form action="<?=URL::base()?>objects/edit/<?=$object->id?>" class="span8" method="post">
    <!-- Defines the contact to remove from the object -->
    <input type="hidden" name="remove_contact_id"/>

    <table class="table">
        <caption><?=__('Kontaktisikud')?></caption>
        <thead>
        <tr>
            <th><?=__('Nimi')?></th>
            <th><?=__('E-mail')?></th>
            <th><?=__('Telefon')?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?=Helper_Contacts::entity_contacts_list('object', $object->id)?>
        </tbody>
        <tfoot>
        <tr>
            <td>
                <select name="contact_id">
                    <?=Helper_Contacts::contact_options()?>
                </select>
            </td>
            <td colspan="3">
                <input type="submit" class="btn" name="add_contact" value="<?=__('Lisa kontaktisik')?>"/>
            </td>
        </tr>
        <tr>
            <td>
                <a class="btn" href="<?=URL::base()?>contacts" title="<?=__('Lisa või eemalda kontakte')?>">
                    <?=__('Aadressiraamat')?>
                </a>
            </td>
            <td colspan="3"></td>
        </tr>
        </tfoot>
    </table>

</form>
<div class="clearfix"></div>

<form action="<?=URL::base()?>objects/upload" class="span8" method="post" enctype="multipart/form-data">
    <table class="table">
        <caption><?=__('Objekti failid')?></caption>
        <thead>
        <tr>
            <th><?=__('Nimi')?></th>
            <th><?=__('Lisatud')?></th>
            <th><?=__('Allalaadimine')?></th>
        </tr>
        </thead>
        <tbody>
        <?=Helper_Objects::files_table_rows($object->id)?>
        </tbody>
        <tfoot>
        <tr>
            <td><?=__('Lisa fail')?>:</td>
            <td colspan="2">
                <input type="hidden" name="object_id" value="<?=$object->id?>"/>
                <input type="file" name="file"/>
                <input type="submit" class="btn btn-primary" name="add_file" value="<?=__('Lisa')?>"/>
            </td>
        </tr>
        </tfoot>
    </table>
</form>
<div class="clearfix"></div>

<h2>Asju, millega arvestada faili lisamisel:</h2>
<ul>
    <li>Lubatud faililaiendid on:
        <strong><?=implode(', ', $upload_settings['allowed_extensions'])?></strong></li>
    <li>Maksimaalne lubatud faili suurus on <?=$upload_settings['max_size']?></li>
    <li>Faile ei saa alla laadida süsteemi sisselogimata
        (seega ei saa saata allalaadimislinke e-kirjaga inimestele, kel süsteemis kontot pole).
    </li>
</ul>

<div class="clearfix"></div>

