<h1><?=__('Objektide nimekiri')?></h1>

<a data-keyboard="true" class="btn" data-controls-modal="modal-new" data-backdrop="true">
    <?=__('Uus objekt')?>
</a>

<table class="table tablesorter table-striped table-condensed">
    <thead>
    <tr>
        <th><?=__('No')?></th>
        <th><?=__('Nimi')?></th>
        <th><?=__('Klient')?></th>
        <th><?=__('Kood')?></th>
        <th><?=__('Piirkond')?></th>
        <th><?=__('Kontaktisikud')?></th>
        <th><?=__('Töödejuhataja')?></th>
        <th><?=__('TJ kood')?></th>
    </tr>
    </thead>
    <tbody>
    <? if (!empty($objects)): ?>
        <? foreach ($objects as $object): ?>
        <tr>
            <td><?=$object->id?></td>
            <td>
                <a href="<?=URL::base()?>objects/edit/<?=$object->id?>" title="<?=__('Muuda')?>">
                    <?=$object->name?>
                </a>
            </td>
            <td><?=$object->client_name?></td>
            <td><?=$object->code?></td>
            <td><?=$object->area_name?></td>
            <td><?=Helper_Contacts::contact_links($o->get_contacts($object->id))?></td>
            <td><?=$object->employee_name ?></td>
            <td><?=$object->employee_code ?></td>
        </tr>
            <? endforeach ?>
        <? endif?>
    </tbody>
</table>

<!-- Dialog for creating a new object -->
<div id="modal-new" class="modal hide fade">
    <form action="<?=URL::base()?>objects" class="form-stacked" method="post">
        <div class="modal-header">
            <a href="#" class="close">&times;</a>

            <h3>
                <?=__('Uus objekt')?>
            </h3>
        </div>
        <div class="modal-body">

            <fieldset>

                <table>
                    <tr>
                        <td><?=__('Nimi')?></td>
                        <td><input type="text" size="15" maxlength="80" required name="name"/></td>
                    </tr>
                    <tr>
                        <td><?=__('Klient')?></td>
                        <td><select name="client_id"><?=Helper_Clients::clients_select()?></select></td>
                    </tr>
                    <tr>
                        <td><?=__('Kood')?></td>
                        <td><input type="text" size="10" maxlength="15" name="code"/></td>
                    </tr>
                    <tr>
                        <td><?=__('Piirkond')?></td>
                        <td>
                            <?=Helper_Objects::area_select()?>
                        </td>
                    </tr>
                </table>

            </fieldset>

        </div>
        <div class="modal-footer">
            <input type="submit" value="<?=__('Lisa objekt')?>" class="btn btn-large btn-primary"/>
        </div>
    </form>
</div>


<div class="clearfix"></div>