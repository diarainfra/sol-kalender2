<h1><?=__('Teemad')?></h1>
<ul>
    <li>
        <a href="<?=URL::base()?>help/changelog">
            <?=__('Muutuste logi')?>
        </a>
        - <?=__('nimekiri muudatustest programmis versioonide kaupa')?>
    </li>
</ul>

<?= __('Lisainfot annab <a href="http://diara.ee">Diara OÜ</a> klienditugi.') ?>