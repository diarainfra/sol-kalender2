<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'employees' => array(
        'empty' => 'Pead lisama vähemalt ühe töötaja.',
        'default' => 'Töötajate nimekiri on vigane',
    ),

    'started' => array(
        'Model_Task::date_smaller_than' => 'Töö algusaeg peab olema varasem töö lõpetamise ajast.',
        'empty' => 'Tööl peab olema algusaeg.',
        'date' => 'Töö algusaeg ei ole õiges vormingus.',
        'default' => __('Töö algusaeg on vigane.'),
    ),
    'finished' => array(
        'empty' => 'Tööl peab olema lõpetamise aeg.',
        'date' => 'Töö lõpetamise aeg ei ole õiges vormingus.',
        'default' => __('Töö lõpetamise aeg on vigane.'),
    ),
    'object' => array(
        'Model_Task::object_exists' => 'Objekti, millega tööd siduda, ei leitud.',
        'default' => __('Objekti väli on vigane.'),
    ),
    'payment' => array(
        'digit' => 'Töö peab olema kas lepingupõhine või -väline.',
        'default' => __('Lepingu väli on vigane.'),
    ),

    'description' => array(
        'empty' => 'Kirjeldus ei tohi olla tühi',
        'default' => 'Kirjeldus on vigane.',

    ),
    'repeat_until' =>array(
        'Model_Task::date_smaller_than' => 'Töö korduse lõpp peab olema suurem töö algusest!',
        'not_empty' => 'Väli "Korda tööd kuni" peab olema täidetud'
    ),
    'repeat_day' => array('not_empty' => 'Korduse päev peab olema täidetud'),
    'repeat_month' => array('not_empty' => 'Korduse kuu peab olema täidetud'),
    'repeat_day_of_week' => array('not_empty' => 'Korduse nädalapäev peab olema täidetud'),

);