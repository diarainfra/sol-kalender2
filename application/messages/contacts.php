<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'name' => array(
        'default' => 'Nimi on vigane',
    ),
    'email' => array(
        'default' => 'E-mail on vigane',
    ),
        'phone' => array(
        'default' => 'Telefon on vigane',
    )
);