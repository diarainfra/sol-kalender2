<?

class Model_EmployeeTest extends KohanaTestCase
{


    public function setUp()
    {
        parent::setUp();
        $this->_count = $this->getConnection()->getRowCount('employees');
    }

    public function test_get_tasks()
    {

        $e = New Model_Employee();

        $tasks = $e->get_tasks(2);
        $this->assertEquals(4, $tasks->count());

        $tasks = $e->get_tasks(4);
        $this->assertEquals(5, $tasks->count());

        $tasks = $e->get_tasks(40);
        $this->assertEquals(0, $tasks->count());
    }

    public function test_get()
    {

        $e = New Model_Employee();

        $data = $e->get();
        $this->assertEquals(7, count($data));

        $data = $e->get(2);
        $this->assertEquals('Malle', $data->name);
        $this->assertEquals('AKFF5', $data->code);
    }

    public function test_add()
    {

        $e = New Model_Employee();

        $id = $e->add(array('name' => 'Hitchhiker', 'code' => '3PO', 'email' => 'test@test.ee', 'pass' => 'testin'));
        $data = $e->get($id);

        $this->assertEquals('Hitchhiker', $data->name);
        $this->assertEquals('3PO', $data->code);
        $this->assertEquals('test@test.ee', $data->email);
    }


}