<?
/**
 * @package Unittests
 */
class Model_SettingsTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->_count = $this->getConnection()->getRowCount('settings');
    }

    public function test_get()
    {

        $s = New Model_Settings();
        $this->assertEquals('0.05', $s->get('extra_price'));
    }


    public function test_set()
    {

        $s = New Model_Settings();

        $this->assertTrue($s->set('extra_price', '0.8'));
        $this->assertEquals('0.8', $s->get('extra_price'));
    }


    public function test_get_accounts()
    {


        $s = New Model_Settings();

        $accounts = $s->get_accounts();

        $this->assertEquals(5421, $accounts[0]);
        $this->assertEquals(5879, $accounts[1]);
        $this->assertEquals(87411, $accounts[2]);

    }

    public function test_delete_account()
    {

        $s = New Model_Settings();

        $this->assertTrue($s->delete_account(87411));

        $accounts = $s->get_accounts();

        $this->assertEquals(5421, $accounts[0]);
        $this->assertEquals(5879, $accounts[1]);
        $this->assertEquals(2, count($accounts));
    }

    public function test_add_account()
    {

        $s = New Model_Settings();

        $s->add_account(8888);
        $accounts = $s->get_accounts();

        $this->assertEquals(8888, $accounts[3]);

        $this->assertFalse($s->add_account('asod'));
    }

}