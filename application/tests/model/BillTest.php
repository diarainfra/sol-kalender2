<?

class Model_BillTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function test_confirm_bill()
    {

        $b = New Model_Bill();

        $this->assertFalse($b->confirm_bill(array()));

        $tasks = array(47, 44, 42);
        $this->assertTrue($b->confirm_bill($tasks));

        $t = New Model_Task();
        $task = $t->get(47);
        $this->assertEquals('1', $task->billed);

        $task = $t->get(42);
        $this->assertEquals('1', $task->billed);

        $task = $t->get(41);
        $this->assertEquals('0', $task->billed, 'Billing changes non-selected task billed value too!');
    }


}