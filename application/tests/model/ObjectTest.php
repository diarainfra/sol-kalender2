<?

class Model_ObjectTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->_count = $this->getConnection()->getRowCount('objects');
    }

    public function test_get()
    {

        $o = New Model_Object();

        $objects = $o->get(5);
        $this->assertEquals('2', $objects->client_id);
        $this->assertEquals('Diara OY', $objects->client_name);
        $this->assertEquals('Gross', $objects->name);
        $this->assertEquals('Kea4', $objects->code);
        $this->assertEquals('Pohi', $objects->area_name);
        $this->assertEquals('Pille', $objects->employee_name);
        $this->assertEquals('AKFA5', $objects->employee_code);
    }

    public function test_area_exists()
    {

        $o = New Model_Object();
        $this->assertFalse($o->area_exists(54));
        $this->assertTrue($o->area_exists(1));
    }

    public function test_get_area()
    {

        $o = New Model_Object();

        $this->assertEquals(1, $o->get_area('Pohi')->id);
        $this->assertEquals('Pohi', $o->get_area(1)->name);
    }

    public function test_get_areas()
    {

        $o = New Model_Object();

        $objects = $o->get_areas();
        $this->assertEquals(4, $objects->count());
    }

    public function test_add()
    {

        $o = New Model_Object();

        $data = array('client_id' => 2, 'name' => 'Tallinna pood', 'code' => 'Ta5', 'area' => 9);
        $tmp = $o->add($data);
        $this->assertFalse($tmp);

        $data = array('client_id' => 2, 'name' => 'Tallinna pood', 'code' => 'Ta5', 'area' => 2);
        $tmp = $o->add($data);
        $this->assertTrue((bool)$tmp);

        $data = $o->get($tmp);
        $this->assertEquals(2, $data->area);
        $this->assertEquals('2', $data->client_id);
        $this->assertEquals('Tallinna pood', $data->name);

        // Contacts
        $contacts = $o->get_contacts($data->id);
        $this->assertEquals(3, count($contacts));
    }


    public function test_update()
    {

        $o = New Model_Object();

        $new_data = array('id' => 5, 'client_id' => 3, 'name' => 'Tallinna pood', 'employee_id' => 2, 'code' => 'Ta5', 'area' => 4);
        $o->update($new_data);
        $data = $o->get('5');

        $this->assertEquals('3', $data->client_id);
        $this->assertEquals('Tallinna pood', $data->name);
        $this->assertEquals('Ta5', $data->code);
        $this->assertEquals('4', $data->area);
        $this->assertEquals('2', $data->employee_id);

        //$new_data['client_id'] = 555;
        // $this->assertFalse($o->update($new_data));
    }


}