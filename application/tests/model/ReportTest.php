<?

class Model_ReportTest extends KohanaTestCase
{
    public function setUp()
    {
        parent::setUp();
    }


    public function test_range()
    {

        $r = New Model_Report();

        $this->assertEquals(2, count($r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => 1, 'payment' => 1))));
        $this->assertEquals(0, count($r->range(array('start' => '01.05.2001', 'end' => '31.05.2001', 'object' => 1))));
        $this->assertEquals(0, count($r->range(array('start' => '01.05.2021', 'end' => '31.05.2200', 'object' => 1))));
        $this->assertEquals(1, count($r->range(array('start' => '15.01.2010', 'end' => '27.01.2010', 'object' => 3, 'payment' => 0))));
        $this->assertEquals(0, count($r->range(array('start' => '15.01.2010', 'end' => '27.01.2010', 'object' => 1, 'payment' => 0))));


        $data = $r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => 1, 'payment' => 1));
        $this->assertEquals('Parandasin arvutit', $data->current()->description);
        $this->assertEquals('Kunda tehas', $data->current()->object_name);
        $this->assertEquals(10, (int)$data->current()->unit_price);


        // Test billed status
        $this->assertEquals(1, count($r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => 1, 'payment' => 1, 'billed' => 1))));
        $this->assertEquals(1, count($r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => 1, 'payment' => 1, 'billed' => 0))));
        $this->assertEquals(2, count($r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'payment' => 1, 'billed' => 0))));
        $this->assertEquals(1, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2020', 'object' => 3, 'payment' => 0, 'billed' => 0))));

        // Payment independant
        $this->assertEquals(5, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'billed' => 0))));
        $this->assertEquals(3, count($r->range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'billed' => 1))));

        // By employee
        $this->assertEquals(3, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'employee' => 1))));
        $this->assertEquals(4, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'employee' => 6))));

        // By client
        $this->assertEquals(5, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'client' => 1))));

        // By area
        $this->assertEquals(5, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'area' => 1))));

        // By internal bill status
        $this->assertEquals(3, count($r->range(array('start' => '01.01.2011', 'end' => '31.05.2011', 'internal_bill' => TRUE))));

    }


    public function test_get_tasks_array()
    {

        $r = New Model_Report();

        // Return valid number of columns?
        $data = $r->get_tasks_array(1, '05.04.2011', '06.06.2011');
        $this->assertEquals(8, count($data[4]));

        // What about payment = 1?
        $data = $r->get_tasks_array(1, '05.04.2011', '06.06.2011', 1);
        $this->assertEquals(10, count($data[4]));
    }


    public function test_get_materials_array()
    {

        $r = New Model_Report();

        $data = $r->get_materials_array(array(0 => 45));
        $this->assertEquals(10, count($data));

        $data = $r->get_materials_array(array(0 => 45, 1 => 46, 2 => 47));
        $this->assertEquals(13, count($data));
    }

}