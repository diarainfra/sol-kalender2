<?

class Model_RecurringTaskTest extends KohanaTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function test_calc_range()
    {

        $rt = New Model_Recurring();

        /**
         * N.B! For some reason the method doesn't work with times other than midnight?
         */

        // Assert that the task occurs 9 times in Aug - every monday including the 1th
        $this->assertEquals(10, count($rt->calc_range('01.08.2011', '15.08.2011', '10 0 * * 1-5')));

        $this->assertEquals(4, count($rt->calc_range('01.08.2011', '31.08.2011', '10 0 * * 1')));

        $this->assertEquals(2, count($rt->calc_range('01.08.2011', '06.08.2011', '10 0 1,3,5 * *')));
        $this->assertEquals(4, count($rt->calc_range('26.06.2011', '25.07.2011', '10 0 * * 0')));
        $this->assertEquals(1, count($rt->calc_range('01.01.2011', '31.08.2011', '30 08 10 06 *')));


    }


    public function test_update()
    {
        $this->markTestSkipped('Recursive tasks are BROKEN!');

        $t = New Model_Task();
        $rt = New Model_Recurring();

        $task_data = $t->get(46);

        $data = array('started' => '2011-08-14 10:17:01',
            'finished' => '2011-08-14 12:17:00',
            'object' => '2',
            'employees' => '1+2+3',
            'description' => 'Uuendatud kirjeldus',
            'payment' => 1,
            'unit_price' => 15,
            'service' => 2,
            'id' => 46,
            'time_estimate' => '00:30',
            'internal_bill' => '0',
            'car_mileage' => '2,7',
            'car_billing' => 1,
            'car_time' => '0:20',
            'time_elapsed' => '00:35');

        // This is where things get tricky. Try to create a recurring task
        $data['repeating'] = 1;
        $data['repeat_day'] = array(1, 2);
        $data['repeat_month'] = '0';
        $data['repeat_day_of_week'] = '0';
        $data['repeat_until'] = '2011-09-05';

        $r = $t->update($data);
        $task_data = $t->get($r);

        $duplicate1 = $rt->get_recurring($task_data->id);
        $this->assertEquals(2, count($duplicate1), 'Auto-creation of duplicate task failed!');


        $this->assertEquals('10 0 1,2 * *', $task_data->repeat_expression);


        $emps = $t->assoc_employees($duplicate1->current()->id);
        $this->assertEquals(3, count($emps));
        $this->assertEquals('2011-09-01 10:17:01', $duplicate1->current()->started);
        $this->assertEquals('2011-09-01 12:17:00', $duplicate1->current()->finished);
        $this->assertEquals('2011-09-02 10:17:01', $duplicate1->next()->current()->started);

        // Change the end date of the repeating schedule
        $data['repeat_until'] = '2011-09-01';
        $data['employees'] = '1';
        $data['service'] = 3;
        $data['description'] = 'See kirjeldus muutub ka duplikaatidel';

        $r = $t->update($data);
        $task_data = $t->get($r);
        $duplicate2 = $rt->get_recurring($task_data->id);

        $emps = $t->assoc_employees($duplicate1->current()->id);
        $this->assertEquals(3, $duplicate2->current()->service);
        $this->assertEquals('See kirjeldus muutub ka duplikaatidel', $duplicate2->current()->description);
        $this->assertEquals(3, count($emps)); // Auto deleting a recurring task doesn't remove employees

        $this->assertEquals(1, count($duplicate2), 'Expected Model_Recurring::handle_repeating_tasks() to automatically remove one duplicate task!');

        // Test repeating with no end date
        $data['finished'] = NULL;
        $r = $t->update($data);
        $task_data = $t->get($r);
        $duplicate2 = $rt->get_recurring($task_data->id);
        $this->assertEmpty($task_data->finished);
        $this->assertEmpty($duplicate2->current()->finished);

        // Set it back to non-repeating
        $data['repeating'] = 0;

        $r = $t->update($data);
        $task_data = $t->get($r);
        $duplicate3 = $rt->get_recurring($task_data->id);

        $this->assertEquals(0, $duplicate3->count());
        $this->assertNull($task_data->repeat_until);
        $this->assertNull($task_data->repeat_expression);

    }


    public function test_update_duplicate()
    {
        $this->markTestSkipped('Recursive tasks are BROKEN!');
        $t = New Model_Task();
        $rt = New Model_Recurring();

        $ORGINAL_ID = 46;
        $data = array('started' => '2011-08-14 10:17:01',
            'finished' => '2011-08-14 12:17:00',
            'object' => '2',
            'employees' => '1+2+3',
            'description' => 'Kirjeldus',
            'payment' => 1,
            'unit_price' => 15,
            'service' => 2,
            'id' => $ORGINAL_ID,
            'time_estimate' => '00:30',
            'internal_bill' => '0',
            'car_mileage' => '2,7',
            'car_billing' => 1,
            'car_time' => '0:20',
            'time_elapsed' => '00:35');


        $data['repeating'] = 1;
        $data['repeat_day'] = array(1, 2);
        $data['repeat_month'] = '0';
        $data['repeat_day_of_week'] = '0';
        $data['repeat_until'] = '2011-09-05';

        $r = $t->update($data);
        $task_data = $t->get($r);
        $this->assertEquals('Kirjeldus', $task_data->description);

        $duplicate1 = $rt->get_recurring($task_data->id)->current();
        $data['description'] = 'Muutus duplikaadis';
        $data['id'] = $duplicate1->id;
        $t->update($data);

        $duplicate_data_data = $t->get($duplicate1->id);
        $task_data = $t->get($ORGINAL_ID);
        $this->assertEquals('Muutus duplikaadis', $duplicate_data_data->description);
        $this->assertEquals('Kirjeldus', $task_data->description);

        // Delete org. task, duplicate should delete too
        $t->delete('tasks', $task_data->id);
        $this->assertEmpty($rt->get_recurring($duplicate1->id)->count());
    }
}