<?

class Model_PdfTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function test_generate_bill_pdf()
    {

        $b = New Model_Pdf();

        $file = $b->generate_bill_pdf('Teststring', 66666, 'test' . time());
        $file = DOCROOT . 'assets/pdf/' . $file;
        $this->assertTrue(file_exists($file), 'Bill PDF creation failed!');
        unlink($file);
    }


}