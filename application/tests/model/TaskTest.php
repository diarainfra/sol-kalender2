<?

class Model_TaskTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->_count = $this->getConnection()->getRowCount('tasks');
    }


    public function test_range()
    {

        $t = New Model_Task();

        $tasks = $t->range(1000, time());
        $this->assertEquals(9, $tasks->count());

        $tasks = $t->range(1000, time(), array('object' => 2));
        $this->assertEquals(2, $tasks->count());

        $tasks = $t->range(1000, time(), array('area' => 2));
        $this->assertEquals(3, $tasks->count());

        $tasks = $t->range(1000, time(), array('area' => 1));
        $this->assertEquals(5, $tasks->count());

        $tasks = $t->range(1000, 2000);
        $this->assertNull($tasks);

        // Test single task
        $tasks = $t->range(1000, time(), array('task_id' => 47));
        $this->assertEquals(1, $tasks->count());
    }

    public function test_get_upcoming()
    {

        $c = New Model_Cron();

        $upcoming = $c->get_upcoming(array('start' => '2011-05-20 23:00:00', 'unfinished' => FALSE, 'offset' => 2));
        $this->assertEquals(1, $upcoming->count());
    }

    public function test_update()
    {

        $t = New Model_Task();

        $task_data = $t->get(46);
        $this->assertEquals('2011-05-21 09:49:00', $task_data->started);
        $this->assertEquals('', (string)$task_data->description);

        $data = array('started' => '2011-08-14 10:17:01',
            'finished' => '2011-08-14 12:17:00',
            'object' => '2',
            'employees' => '1+2',
            'description' => 'Uuendatud kirjeldus',
            'payment' => 1,
            'unit_price' => 15,
            'service' => 2,
            'id' => 46,
            'time_estimate' => '00:30',
            'internal_bill' => '0',
            'car_mileage' => '2,7',
            'car_billing' => 1,
            'car_time' => '0:20',
            'time_elapsed' => '00:35',
            'repeat_day' => '',
            'repeat_month' => '',
            'repeat_day_of_week' => '');
        $r = $t->update($data);
        $this->assertEquals(46, $r);

        $task_data = $t->get($r);

        $this->assertEquals('2011-08-14 10:17:01', $task_data->started);
        $this->assertEquals('Uuendatud kirjeldus', $task_data->description);
        $this->assertEquals('1800', $task_data->time_estimate);
        $this->assertEquals('2100', $task_data->time_elapsed);
        $this->assertEquals(2.7, (float)$task_data->car_mileage);

        $emps = $t->assoc_employees($r);
        $this->assertEquals(2, count($emps));

        $data['finished'] = NULL;
        $r = $t->update($data);
        $task_data = $t->get($r);
        $this->assertEmpty($task_data->finished);

    }

    public function test_tasks_in_range()
    {

        $t = New Model_Task();

        $ids = $t->tasks_in_range(array('start' => '2001-05-10', 'end' => '2001-05-10'));
        $this->assertEquals(0, count($ids));
        $this->assertTrue(empty($ids));

        $ids = $t->tasks_in_range(array('start' => '2001-05-10', 'end' => '2010-05-10'));
        $this->assertEquals(1, count($ids));
        $this->assertEquals(41, $ids[0]);

        $ids = $t->tasks_in_range(array('start' => '2011-01-01', 'end' => '2011-12-30'));
        $this->assertEquals(8, count($ids));

        $ids = $t->tasks_in_range(array('start' => '2011-01-01', 'end' => '2011-12-30', 'billed' => 1));
        $this->assertEquals(3, count($ids));

        // Test billed filter
        $this->assertEquals(1, count($t->tasks_in_range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => 3, 'billed' => 0))));
        $this->assertEquals(0, count($t->tasks_in_range(array('start' => '01.01.2011', 'end' => '31.05.2020', 'object' => 3, 'billed' => 1))));

        // Test bill type
        $this->assertEquals(1, count($t->tasks_in_range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => NULL, 'billed' => 0, 'payment' => 0))));
        $this->assertEquals(2, count($t->tasks_in_range(array('start' => '01.05.2011', 'end' => '31.05.2011', 'object' => NULL, 'billed' => 0, 'payment' => 1))));
    }


    public function test_get_comments()
    {
        $t = New Model_Task();

        $comments = $t->get_comments(41);
        $this->assertEquals(0, $comments->count());

        $comments = $t->get_comments(40);
        $this->assertEquals(2, $comments->count());
        $this->assertEquals('2011-07-20 13:08:16', $comments->current()->timestamp);
        $this->assertEquals('Administraator', $comments->current()->name);
    }


    public function test_used_materials()
    {

        $t = New Model_Task();


        $data = array('name' => 'Kruvid',
            'quantity' => '43',
            'unit' => 'tk',
            'unit_price' => '0.22',
            'task_id' => '47');

        $this->assertTrue((bool)$t->add_materials($data));

        $materials = Model_Task::used_materials(47);

        $this->assertEquals(3, $materials->count());

        $materials = $materials->next()->next()->current();

        $this->assertEquals('Kruvid', $materials->name);
        $this->assertEquals(43, (int)$materials->quantity);
        $this->assertEquals('tk', $materials->unit);
        $this->assertEquals('0.22', (string)$materials->unit_price);
        $this->assertEquals('47', (string)$materials->task_id);
        $this->assertEquals('2011-05-10 09:50:00', (string)$materials->date);

    }

    public function test_used_materials_sum()
    {

        $t = New Model_Task();
        $this->assertEquals(0, $t->used_materials_sum(array()));
        $this->assertEquals(6, $t->used_materials_sum(45));
        $this->assertEquals(8, $t->used_materials_sum(array(45, 46)));
        $this->assertEquals(8.4, $t->used_materials_sum(array(45, 44)));

        $this->assertEquals(6.3, $t->used_materials_sum(45, TRUE));
        $this->assertEquals(8.82, $t->used_materials_sum(array(45, 44), TRUE));

    }

    public function test_add()
    {

        $t = New Model_Task();

        $data = array('started' => '2011-04-14 10:17:00',
            'finished' => '2011-04-16 12:17:00',
            'object' => '2',
            'employees' => '1+2+3',
            'description' => 'Unittesti kirjeldus',
            'payment' => 1,
            'unit_price' => 15,
            'service' => 2,
            'time_estimate' => '01:00',
            'time_elapsed' => '01:10',
            'internal_bill' => 1
        );
        $r = $t->add($data);
        $this->assertTrue(is_numeric($r));

        $task_data = $t->get($r);
        $this->assertEquals('2011-04-14 10:17:00', $task_data->started);
        $this->assertEquals('2011-04-16 12:17:00', $task_data->finished);
        $this->assertEquals('Unittesti kirjeldus', $task_data->description);
        $this->assertEquals('1', $task_data->internal_bill);
        $this->assertEquals(1, (int)$task_data->payment);
        $this->assertEquals(15, (int)$task_data->unit_price);
        $this->assertEquals(0, (int)$task_data->billed);
        $this->assertEquals('2', $task_data->service);
        $this->assertEquals('3600', $task_data->time_estimate);
        $this->assertEquals('4200', $task_data->time_elapsed);

        $c = $t->assoc_employees($task_data->id);
        $this->assertEquals(3, $c->count());

        $data['finished'] = '2011-02-16 12:17:00';
        $this->assertTrue(is_array($t->add($data)));

        $data = array('started' => '2011-04-14 10:17:00',
            'finished' => '2011-04-16 12:17:00',
            'object' => '212',
            'employees' => '1+2+3',
            'description' => 'Unittesti kirjeldus',
            'payment' => 0,
            'service' => 4,
            'unit_price' => '3g#',
            'time_estimate' => '1200',
            'time_elapsed' => '1000',
            'internal_bill' => 0);
        $this->assertTrue(is_array($t->add($data)));


        // Test unit_price formatting
        $data['object'] = 2;
        $data['unit_price'] = 2.5;
        $r = $t->add($data);
        $this->assertTrue(is_numeric($r));
        $task_data = $t->get($r);
        $this->assertEquals('2.5', $task_data->unit_price);

        $data['unit_price'] = '2,8';
        $r = $t->add($data);
        $this->assertTrue(is_numeric($r));
        $task_data = $t->get($r);
        $this->assertEquals('2.8', $task_data->unit_price);

        // Test repeating task adding @todo Not currently implemented
        /*$data['repeating'] = 1;
        $data['repeat_day'] = '1';
        $data['repeat_month'] = '0';
        $data['repeat_day_of_week'] = '0';
        $data['repeat_until'] = '2011-05-05';
        $data['description'] = 'Korduva too lisamine';
        $r = $t->add($data);
        $this->assertTrue(is_numeric($r));
        $rt = New Model_Recurring();
        $duplicates = $rt->get_recurring($r);
        $this->assertEquals(1, $duplicates->count());
        $this->assertEquals('Korduva too lisamine', $duplicates->current()->description);*/

    }


    public function test_add_materials()
    {

        $t = New Model_Task();

        $data = array();
        $this->assertFalse($t->add_materials($data));

        $data = array('name' => 'Kruvid',
            'quantity' => '43',
            'unit' => 'tk',
            'unit_price' => '0.22',
            'task_id' => '454');
        $this->assertFalse($t->add_materials($data)); // No such task_id

        $data['task_id'] = 47;
        $r = $t->add_materials($data);
        $this->assertTrue(is_numeric($r));

        $data['unit_price'] = '0,22';
        $data['quantity'] = '15,8';
        $this->assertTrue((bool)$t->add_materials($data));

        $data = array('name' => '',
            'quantity' => '43',
            'unit' => 'tk',
            'unit_price' => '0.22',
            'task_id' => '47');
        $this->assertFalse($t->add_materials($data));

    }


    public function test_assoc_employees()
    {

        $t = New Model_Task();

        $employees = $t->assoc_employees(46);
        $this->assertEquals(3, $employees->count());

        $names = array();
        foreach ($employees as $employee) {
            $names[] = $employee->name;
        }
        $this->assertContains('Nimetu', $names);
        $this->assertContains('Malle', $names);
        $this->assertContains('kupi', $names);

        $this->assertNull($t->assoc_employees(406));
    }


    public function test_get()
    {

        $t = New Model_Task();

        $task = $t->get(47);
        $this->assertEquals('Parandasin arvutit', $task->description);

        $task = $t->get();
        $this->assertEquals(10, count($task));

        $task = $t->get(array(46, 47));
        $this->assertEquals(2, count($task));
    }

}