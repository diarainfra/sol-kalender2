<?

class Model_ClientTest extends KohanaTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->_count = $this->getConnection()->getRowCount('clients');
    }

    public function test_get() {

        $c = New Model_Client();

        $clients = $c->get(1);
        $this->assertEquals('A4345', $clients->code);
        $this->assertEquals('Kristiine Kodukaubad AS', $clients->name);
        $this->assertEquals('Kristiine pst 3432', $clients->postal_address);

        $clients = $c->get();
        $this->assertEquals('JA44', $clients->current()->code);
        $this->assertEquals('M87AR', $clients->next()->current()->code);
    }

    public function test_add() {

        $c = New Model_Client();

        $data = array('name' => 'Tallinna pood',
                      'code' => 'Ta5',
                      'postal_address' => 'Kolipargi 55a',
                      'phone' => '3221281',
                      'email' => 'ando.roots@diara.ee',
                      'birthday' => '31.12.1991',
                      'contract_start' => '01.01.2001',
                      'contract_end' => '31.12.2002',
                      'extra' => 'This client is a VIP'
        );
        $tmp = $c->add($data);
        $this->assertGreaterThan(0, $tmp);

        $data = $c->get($tmp);
        $this->assertEquals('Ta5', $data->code);
        $this->assertEquals('Kolipargi 55a', $data->postal_address);
        $this->assertEquals('Tallinna pood', $data->name);
        $this->assertEquals('1991-12-31', $data->birthday);
        $this->assertEquals('This client is a VIP', $data->extra);
    }


    public function test_update() {

        $c = New Model_Client();

        $clients = $c->get(1);
        $this->assertEquals('A4345', $clients->code);
        $data = array('id' => '1',
                      'name' => 'testup',
                      'code' => 'testcode',
                      'postal_address' => 'postal',
                      'phone' => '3221281',
                      'email' => 'ando.roots@diara.ee',
                      'birthday' => '31.12.1991',
                      'contract_start' => '01.01.2001',
                      'contract_end' => '31.12.2002',
                      'extra' => 'This client is a VIP');
        $c->update($data);

        $clients = $c->get(1);
        $this->assertEquals('testcode', $clients->code);
        $this->assertEquals('postal', $clients->postal_address);
        $this->assertEquals('testup', $clients->name);
        $this->assertEquals('2002-12-31', $clients->contract_end);

        $data['name'] = '';
        $this->assertFalse($c->update($data));

    }


    public function test_objects() {

        $c = New Model_Client();

        $objects = $c->objects(1);
        $this->assertEquals(2, count($objects));
        $this->assertEquals('A5483', $objects->current()->code);
        $this->assertEquals('4334', $objects->next()->current()->code);
    }


    public function test_add_contact() {

        $c = New Model_Client();
        $this->assertEquals(3, count($c->get_contacts(2)));

        $this->assertFalse($c->add_contact(2, 3));
        $c->add_contact(2, 4);
        $this->assertEquals(4, count($c->get_contacts(2)));

    }

}