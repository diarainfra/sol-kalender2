<?

define('SUPPRESS_REQUEST', TRUE);
$_SERVER['SERVER_NAME'] = 'localhost';
require_once('PHP/Token/Stream/Autoload.php');
require_once 'PHPUnit/Autoload.php';

require('../index.php');

function reset_database()
{
    exec('mysql -u unittest -ptestcase unittest < tests/fixtures/drop.sql');
    exec('mysql -u unittest -ptestcase unittest < ../doc/db/structure.sql');
}

function testuser_login()
{
    if (!defined('TEST_USER_ID')) {
        throw new Exception('Cannot login testuser');
    }
    User::$_current_user = ORM::factory('user', TEST_USER_ID);
    Auth::instance()->force_login(TEST_USER_USERNAME);
}


reset_database();


// Mock the logged in user for testing
define('TEST_USER_ID', 1);
define('TEST_USER_USERNAME', 'admin');
define('TEST_USER_CLIENT', 'klient');
define('TEST_USER_CLIENT_PW', 'diaratest');

require('KohanaTestCase.php');
