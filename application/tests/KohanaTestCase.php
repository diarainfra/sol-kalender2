<?

class KohanaTestCase extends Kohana_Unittest_Database_TestCase
{

    /**
     * @var The count of all rows in the current table
     */
    protected $_count;

    /**
     * @var The count of deleted rows in the current table
     */
    protected $_deleted;

    /**
     * @var The current ORM object
     */
    protected $_orm;


    public function getDataSet()
    {
        return new PHPUnit_Extensions_Database_DataSet_YamlDataSet(
            'tests/fixtures/inserts.yml'
        );
    }

    public function setUp()
    {
        testuser_login();
        parent::setUp();
    }


}