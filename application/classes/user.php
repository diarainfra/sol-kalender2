<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * @package User
 * @since 1.1.2
 */
class User extends Model_Auth_User
{
    /**
     * @var Model_Auth_User
     */
    public static $_current_user;

    /**
     * Get the current user
     *
     * @since 1.1.2
     * @static
     * @return Model_Auth_User
     */
    public static function current()
    {
        if (User::$_current_user === NULL) {
            User::$_current_user = Auth::instance()->get_user();
        }
        return User::$_current_user;
    }

    /**
     * Check the last version the user has used (from DB) and
     * display a notification if it's smaller than the current version
     *
     * @static
     * @since 1.1.2
     */
    public static function new_release_notification() {
        //todo
    }
}