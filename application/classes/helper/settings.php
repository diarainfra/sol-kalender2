<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Settings
{

    /**
     * @static
     * @param $accounts
     * @param null $selected
     * @return null|string
     */
    public static function account_options($accounts, $selected = NULL) {
        $html = NULL;
        if (is_array($accounts)) {
            foreach ($accounts as $account) {
                $sel = ($account == $selected) ? ' selected' : '';
                $html .= "<option value=\"$account\"$sel>$account</option>";
            }
        }
        return $html;

    }


    /**
     * Static accessor method for settings
     * Used in view files to access setting values
     * @static
     * @param $key
     * @return mixed
     */
    public static function get($key) {
        $s = New Model_Settings();
        return $s->get($key);
    }

}