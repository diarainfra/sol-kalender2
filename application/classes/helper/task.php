<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Task
{

    /**
     * Generates tbody and tfoot for used materials table
     * in the bills and reports view
     * @static
     * @param bool $footer Whether to generate the footer HTML
     * @param bool $report TRUE to include additional cells for reports page
     * @param mixed $task_ids An array or a int of task id's whose materials will be listed
     * @return null|string
     */
    public static function material_rows($task_ids, $footer = TRUE, $report = TRUE)
    {

        $t = New Model_Task();

        // The function is meant for multiple tasks
        if (!is_array($task_ids)) {
            $task_ids = array(0 => $task_ids);
        }

        $html = NULL;

        if (empty($task_ids)) {
            return NULL;
        }
        // Loop over tasks
        foreach ($task_ids as $task_id) {
            $used_materials = Model_Task::used_materials($task_id);

            if (empty($used_materials)) {
                return NULL;
            }
            // Loop over materials
            foreach ($used_materials as $row) {
                $total = round($row->quantity * $row->unit_price, 2);

                $html .= "
                <tr>
                    <td>".HTML::anchor('task/view/' . $task_id, '#' . $task_id)."</td>
                    <td>" . date('d.m', strtotime($row->date)) . "</td>
                    <td>$row->name</td>
                    <td>$row->quantity</td>
                    <td>$row->unit</td>
                    <td class=\"nowrap\">$row->unit_price</td>
                    <td class=\"nowrap\">$total €</td>";

                if ($report) {
                    $html .= '<td>' . Text::bit2str($row->internal_bill) . '</td>
                    <td>'.Text::bit2str($row->billed).'</td>';

                }
                $html .= '</tr>';
            }
        }


        if (empty($html)) {
            return '<tr><td colspan="8">'.__("Kasutatud materjale sellel perioodil ei ole.").'</td> </tr>';
        }

        // Footer HTML
        $html = "<tbody>$html</tbody>";
        $sum = $t->used_materials_sum($task_ids);

        // Extra cost (lepinguline juurdehindlus)
        $s = New Model_Settings();
        $extra_cost = $s->get('extra_price');

        $sum_total = $t->used_materials_sum($task_ids, TRUE);

        if ($footer) {
            $html .= '<tfoot><tr>
                <th colspan="3" style="text-align: right;">Lepinguline juurdehindlus</th>
                <th>' . ($extra_cost * 100) . '</th>
                <th>%</th>
                <th>' . $sum . '</th>
                <th colspan="2">' . round(($sum * $extra_cost), 2) . ' €</th>
                </tr>';
            $col = ($report) ? 7 : 6;
            $html .= '<tr><th colspan="' . $col . '" style="text-align: right;">Kokku</th><th colspan="2">' . $sum_total . ' €</th></tr>';

            $html .= '</tfoot>';
        }
        return $html;
    }


    /**
     * Calculate the amount of money to bill the customer
     * @static
     * @param  $time Spent time, in seconds
     * @param  $unit_price The price of an hour of work
     * @return float Sum
     */
    public static function calc_sum($time, $unit_price)
    {
        return round($unit_price * Helper_Reports::pretty_time($time, TRUE) + ($unit_price * gmdate('i', $time) / 60), 2);
    }


    /**
     * Converts seconds to a pretty format of H:ii, hours can exceed 24h
     * @static
     * @param int $seconds Seconds
     * @param bool $only_hours Return hours without minutes?
     * @return string Time in HH:ii
     */
    public static function pretty_time($seconds, $only_hours = FALSE)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor($seconds % 3600 / 60);
        $time = sprintf("%d:%02d", $hours, $minutes);
        if ($only_hours) {
            $time = explode(':', $time);
            return $time[0];
        }

        return sprintf("%d:%02d", $hours, $minutes);
    }


    /**
     * Return the title string for a report
     * @static
     * @param $filters
     * @return string
     */
    public static function report_title($filters)
    {
        $title = '';
        if (!isset($filters['object_name'])) {
            $o = New Model_Object();
            $object_data = $o->get($filters['object']);
            $filters['object_name'] = $object_data->name;
            $title = "Objekt: " . $filters['object_name'] . ";";
        }
        $title .= " Periood: " . $filters['start'] . " - " . $filters['end'];
        return $title;
    }


    public static function task_options($task_ids, $selected = NULL)
    {
        $t = New Model_Task();
        $tasks = $t->get($task_ids);

        $html = NULL;
        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $label = '#' . $task->id . ' ' . $task->description;
                $label = Helper_Template::limit_str($label, 6);

                $sel = ($selected == $task->id) ? ' selected="selected"' : '';

                $html .= "<option value=\"$task->id\"$sel>$label</option>";
            }
        }
        return $html;
    }


    /**
     * Returns tbody of comments table in /task/view
     * @static
     * @param $comments
     * @return null|string
     */
    public static function comments_list($comments)
    {
        $html = NULL;

        if ($comments->count() > 0) {
            foreach ($comments as $comment) {
                $html .= "<tr>
                <td>" . Date::localized_date($comment->timestamp) . "</td>
                <td>$comment->name</td>
                <td>$comment->comment</td>
                <td><a href=\"" . URL::base() . "task/delete_comment/" . $comment->id . "\">Kustuta</a></td>
                </tr>";
            }
        } else {
            $html = '<tr><td colspan="4">Ühtegi kommentaari pole lisatud</td> </tr>';
        }
        return $html;
    }


    /**
     * Test whether the task has a completion date
     *
     * Task field 'finished' is a date with possible values
     * of NULL and Epoch. This method tests whether we have a valid
     * end date.
     * @static
     * @param $date Datetime
     * @return bool
     */
    public static function date_set($date)
    {
        return !(empty($date) || date('Y', strtotime($date)) < 1980);
    }


}