<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Clients
{

    /**
     * @static
     * @return null|string HTML for objects table body
     */
    public static function clients_list() {
        $c = New Model_Client();
        $html = NULL;

        foreach ($c->get() as $client) {
            $html .= "
   <tr id=\"client-$client->id\">
        <td>$client->code</td>
        <td>$client->name</td>
        <td>$client->postal_address</td>
        <td>" . Helper_Contacts::contact_links($c->get_contacts($client->id)) . "</td>
        <td>$client->email</td>
        <td>$client->phone</td>
        <td>" . Date::localized_date($client->contract_start) . "</td>
        <td>" . Date::localized_date($client->contract_end) . "</td>
        <td>" . Date::localized_date($client->birthday) . "</td>
        <td>$client->extra</td>
    </tr>";
        }
        return $html;
    }


    /**
     * Generate selectbox options for clients
     * @static
     * @param null $selected_id
     * @return null|string
     */
    public static function clients_select($selected_id = NULL) {
        $c = New Model_Client();
        $html = NULL;
        $clients = $c->get();

        if (!empty($clients)) {
            foreach ($clients as $client) {

                $sel = ($selected_id == $client->id) ? ' selected' : '';
                $html .= "<option value=\"$client->id\"$sel>$client->name</option>";
            }
        }
        return $html;
    }


}