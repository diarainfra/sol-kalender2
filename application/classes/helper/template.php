<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Template
{


    /**
     * Input is a $_GET string (example: foo=bar&miki=yes&...)
     * @static
     * @deprecated Bad style
     * @param  $filters
     * @return array Exploded array (example: array('foo' => 'bar'))
     */
    public static function explode_query_string($filters) {

        if (strstr($filters, '?')) {
            $filters = explode('?', $filters);
            $filters = $filters[1];
        }

        if (!empty($filters)) {
            $fil = array();
            foreach (explode('&', $filters) as $f) {
                $item = explode('=', $f);
                if (!empty($item[0]) && !empty($item[1])) {
                    $fil[$item[0]] = $item[1];
                }
            }
            return $fil;
        }
    }


    /**
     * Converts input date to MYSQL format
     * @static
     * @param string $date Any valid date string
     * @param bool $include_time If true, return H:i:s also
     * @return null|string
     */
    public static function mysql_date($date, $include_time = FALSE) {
        if (empty($date)) {
            return NULL;
        }
        if (!strtotime($date) && $date > 1) {
            $date = date('Y-m-d H:i:s', $date);
        }

        if ($include_time) {
            return date('Y-m-d H:i:s', strtotime($date));
        }
        return date('Y-m-d', strtotime($date));
    }


    /**
     * Returns the localized version of a date
     * @static
     * @param $date
     * @deprecated Replaced by commoneer
     * @param bool $include_time Whether to return hours and minutes
     * @return null|string
     */
    public static function localized_date($date, $include_time = FALSE) {
        if (!Helper_Task::date_set($date)) {
            return NULL;
        }
        if (strpos($date, '-')) {
            if ($include_time) {
                return date('d.m.Y H:m', strtotime($date));
            }
            return date('d.m.Y', strtotime($date));
        }
        return $date;
    }


    /**
     * Convert a string to the file/URL safe "slug" form
     *
     * @param string $string the string to clean
     * @param bool $is_filename TRUE will allow additional filename characters
     * @return string
     */
    public static function sanitize_file_name($string = '', $is_filename = FALSE) {
        // Replace all weird characters with dashes
        $string = preg_replace('/[^\w\-' . ($is_filename ? '~_\.' : '') . ']+/u', '-', $string);

        // Only allow one dash separator at a time (and make string lowercase)
        return mb_strtolower(preg_replace('/--+/u', '-', $string), 'UTF-8');
    }


    /**
     * Calculate the amount of money to bill the customer
     * @static
     * @param  $time Spent time, in seconds
     * @param  $unit_price The price of an hour of work
     * @return float Sum
     */
    public static function calc_sum($time, $unit_price) {
        return round($unit_price * Helper_Template::pretty_time($time, TRUE) + ($unit_price * gmdate('i', $time) / 60), 2);
    }


    /**
     * Converts seconds to a pretty format of H:ii, hours can exceed 24h
     * @static
     * @param int $seconds Seconds
     * @param bool $only_hours Return only the hour part?
     * @return string Time in HH:ii
     */
    public static function pretty_time($seconds, $only_hours = FALSE) {
        if (!is_numeric($seconds)) {
            if (strpos($seconds, ':') && count(explode(':', $seconds)) == 2) {
                return $seconds;
            } else {
                $seconds = strtotime($seconds);
            }
        }
        $hours = floor($seconds / 3600);
        $minutes = floor($seconds % 3600 / 60);
        $time = sprintf("%d:%02d", $hours, $minutes);
        if ($only_hours) {
            $time = explode(':', $time);
            return $time[0];
        }

        return sprintf("%d:%02d", $hours, $minutes);
    }


    /**
     * Returns the corresponding month name in Estonian
     * or an array of month names if $month is empty
     * @static
     * @param int|null $month From 1 to 12
     * @return bool|string|array
     */
    public static function month_name($month = NULL) {
        $months = array(1 => 'Jaanuar',
                        2 => 'Veebuar',
                        3 => 'Märts',
                        4 => 'Aprill',
                        5 => 'Mai',
                        6 => 'Juuni',
                        7 => 'Juuli',
                        8 => 'August',
                        9 => 'September',
                        10 => 'Oktoober',
                        11 => 'November',
                        12 => 'Detsember');
        if ($month === NULL) {
            return $months;
        }

        if (!array_key_exists($month, $months)) {
            return FALSE;
        } else {
            return $months[$month];
        }
    }


    /**
     * Returns the corresponding day name in Estonian
     * or an array of day names if $day is empty
     * @static
     * @param int|null $day From 1 to 12
     * @return bool|string|array
     */
    public static function day_name($day = NULL) {
        $days = array(
            0 => 'Pühapäev',
            1 => 'Esmaspäev',
            2 => 'Teisipäev',
            3 => 'Kolmapäev',
            4 => 'Neljapäev',
            5 => 'Reede',
            6 => 'Laupäev');
        if ($day === NULL) {
            return $days;
        }

        if (!array_key_exists($day, $days)) {
            return FALSE;
        } else {
            return $days[$day];
        }
    }


    /**
     * Return a shortened version of the string
     * @param $str
     * @param $length
     * @return string
     */
    public static function limit_str($str, $length) {
        $str = strip_tags($str);
        $str = explode(" ", $str);
        return implode(" ", array_slice($str, 0, $length));
    }


    /**
     * Returns a human-readable time diff between $start and $end
     * @static
     * @param $start Any valid time string
     * @param $end Any valid time string
     * @param bool $in_pretty If True, return time in HH:mm, if false, return sec
     * @return string Time diff between $start and $end
     */
    public static function time_diff($start, $end, $in_pretty = TRUE) {
        $diff = strtotime($end) - strtotime($start);
        if (!$in_pretty) {
            return $diff;
        }

        $pretty = Helper_Template::pretty_time(abs($diff));
        if ($diff < 0) {
            $pretty .= '-' . $pretty;
        }

        return $pretty;
    }


    /**
     * Converts pretty time to seconds
     * Example: Input: 01:00 Output: 3600
     * @param string $time Time, in the format of HH:ii
     * @return int Seconds
     */
    public static function pretty_to_sec($time) {
        if (!strstr($time, ':')) {
            return 0;
        }

        $time = explode(':', $time);
        return ((int)$time[0] * 60 * 60) + ((int)$time[1] * 60);
    }


    /**
     * A hack for Kohana's Validation library.
     * Validation unsets all $_POST variable that don't have rules
     * associated with them. This pseudo-rule always
     * returns true and so allows to bypass this check.
     * @static
     * @param $value
     * @return bool
     */
    public static function optional_field($value) {
        return TRUE;
    }


    /**
     * Handles displaying form validation errors to the user.
     * The errors are displayed as Notify::msg messages only,
     * any other aspect (form refilling) must be done separately.
     * @static
     * @param array $e Validation::errors() array
     * @return void
     */
    public static function validation_error($e) {
        if (!empty($e) && is_array($e)) {
            foreach ($e as $error) {
                Notify::msg($error, 'error');
            }
        }
    }


    /**
     * Convert comma to period in the input string
     * @static
     * @param string|int $input An input string
     * @param int|bool $round How many decimal places to return?
     * @return float
     */
    public static function mysql_float($input = 0, $round = FALSE) {
        if (empty($input)) {
            return (float)0;
        }
        $input = (float)rtrim(str_replace(',', '.', $input), ' 0');
        if ($round === FALSE) {
            return $input;
        }
        return round($input, $round);
    }


}