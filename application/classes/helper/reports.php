<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Reports
{

    /**
     * Generates tbody for reports view - all task rows for a period
     * @static
     * @param  $data Multidimensional array of tasks data
     * @param string $type Either 'in' or 'out'
     * @return null|string
     */
    public static function rows($data, $type = 'in')
    {

        if ($data->count() == 0) {

            return '<tr><td colspan="' . ($type == "in" ? 11 : 12) . '">' . __("Sellel perioodil töid ei leitud.") . '</td> </tr>';
        }
        $t = New Model_Task();

        $html = NULL;

        // Loop over tasks
        foreach ($data as $row) {
            $employees_count = count($t->assoc_employees($row->id));
            $employees_count =  $employees_count == 0 ? 1 : $employees_count;
            $date = date('d.m', strtotime($row->started));
            $started = date('H:i', strtotime($row->started));
            $finished = date('H:i', strtotime($row->finished));
            $id = HTML::anchor('task/view/' . $row->id, '#' . $row->id);
            $elapsed = Helper_Template::pretty_time($row->time_elapsed * $employees_count);

            // Row HTML
            $html .= <<< EOP
                <tr>
                    <td>
                    $id
                    </td>
                    <td>$date</td>
                    <td>$started</td>
                    <td>$finished</td>
                    <td>$row->object_name</td>
                    <td style="word-wrap: break-word !important;">$row->description</td>
                    <td class="nowrap">$elapsed</td>
                    <td>h</td>
EOP;

            // Car costs
            if ($row->car_mileage > 0) {

                $html .= "<td>{$row->car_mileage}km,
                " . Helper_Template::pretty_time($row->car_time) . "h
                (+" . $t->car_mileage($row->car_mileage) . "€)</td>";
            } else {
                $html .= '<td></td>';
            }

            // Outside-contract tasks have 2 extra rows
            if ($row->payment == 1) {
                $total = Helper_Template::calc_sum($row->time_elapsed, $row->unit_price);
                $html .= "<td class=\"nowrap\">$row->unit_price €</td>
                          <td class=\"nowrap\">$total €</td>";
            }

            // Internal / billed status
            $html .= '<td>' . Text::bit2str($row->internal_bill) . '</td>
            <td>' . Text::bit2str($row->billed) . '</td>';

            $html .= '</tr>';
        }

        return $html;
    }


    /**
     * Generate report table footer with all the sums
     * For in contract / outside contract tables
     * @static
     * @param $data object Tasks data object
     * @param string $type Return table row HTML or only sum as string
     * @return null|string
     */
    public static function sums($data, $type = 'all')
    {
        if (empty($data)) {
            return NULL;
        }

        $t = Model::factory('task');

        $sums = array('time' => 0, 'sum' => 0);

        // Loop over all the tasks and calc sums
        foreach ($data as $row) {
            $sums['time'] += $row->time_elapsed;

            /* Payment row is considered for internal bills
             * Bills that go for the client should only ever pass outside contract
             * tasks as $data to this function
             * Internal bills however consider the $unit_price too.
             */
            if ($row->payment == 0) {
                $sums['sum'] += $row->unit_price;
            } else {
                $sums['sum'] += Helper_Template::calc_sum($row->time_elapsed, $row->unit_price);
            }

            // Add car cost
            if ($row->car_billing) {
                $sums['sum'] += $t->car_mileage($row->car_mileage);
            }

        }


        if ($type == 'sum') {
            return $sums['sum'];
        } elseif ($type == 'time') {
            return $sums['time'];
        }

        return $sums;
    }


    /**
     * Generates the title of a report based on it's filters.
     * @todo: Make title generation more complex
     * @static
     * @param $filters
     * @return string
     */
    public static function report_title($filters)
    {
        $title = "Aruanne (periood: " . $filters['start'] . " - " . $filters['end'] . ")";
        return $title;
    }

}