<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Contacts
{

    /**
     * @static
     * @return null|string HTML for contacts table body
     */
    public static function contacts_list()
    {
        $e = ORM::factory('contact');
        $return = NULL;

        $contacts = $e->find_all();
        if (empty($contacts)) {
            return NULL;
        }

        foreach ($contacts as $contact) {
            $delete = URL::base() . 'contacts/delete/' . $contact->id;
            $edit = URL::base() . 'contacts/edit/' . $contact->id;

            $return .= <<<EOF
<tr>
        <td>$contact->id</td>
        <td>
            <a href="$edit">
                $contact->name
            </a>
        </td>
        <td>$contact->email</td>
        <td>$contact->phone</td>
        <td><a class="btn danger" href="$delete" title="Kustuta">Kustuta</a>
</tr>
EOF;
        }
        return $return;
    }


    /**
     * @static
     * Generate contacts dropdown options
     * @param null $selected
     * @return null|string
     */
    public static function contact_options($selected = NULL)
    {
        $html = NULL;
        $e = ORM::factory('contact');
        $contacts = $e->find_all();
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                $sel = ($contact->id == $selected) ? ' selected' : '';
                $html .= "<option value=\"$contact->id\"$sel>$contact->name</option>";
            }
        }
        return $html;
    }


    /**
     * Prints table rows for either
     * a) /clients/edit contacts table
     * or
     * b) /objects/edit contacts table
     * @static
     * @param string $entity object|client
     * @param int $entity_id
     * @return null|string
     */
    public static function entity_contacts_list($entity, $entity_id)
    {

        $c = ORM::factory('contact');
        $contacts = $c->entity_contacts($entity, $entity_id);

        $html = NULL;
        if (!empty($contacts)) {
            foreach ($contacts as $contact_id) {
                $contact = ORM::factory('contact', $contact_id);

                $remove = <<<EOF
   <a href="#" title="Eemalda" class="btn danger" onclick="$('input[name=\'remove_contact_id\']').val($contact->id); $(this).closest('form').submit();">Eemalda</a>
EOF;

                $html .= "<tr>
                   <td>$contact->name</td>
                   <td>$contact->email</td>
                   <td>$contact->phone</td>
                   <td>$remove</td>
                   </tr>";
            }
        }
        return $html;
    }


    /**
     * Returns HTML links to contacts details page
     * @static
     * @param $contacts ORM $contacts model ( ORM::factory('contacts')->find_all() );
     * @return null|string
     */
    public static function contact_links($contacts)
    {
        $html = NULL;
        if (!empty($contacts)) {
            foreach ($contacts as $contact_id) {
                $contact = ORM::factory('contact', $contact_id);
                $html .= '<a href="' . URL::base() . 'contacts/edit/' . $contact->id . '" title="Aadressiraamat">' . $contact->name . '</a>, ';
            }
        }
        return $html;
    }


}