<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Employee
{
    /**
     * @static
     * Generate employee dropdown options
     * @param null $selected
     * @param bool $manager
     * @return null|string
     */
    public static function employee_options($selected = NULL, $manager = FALSE) {
        $html = NULL;
        $e = New Model_Employee();
        $employees = $e->get();
        if (!empty($employees)) {
            foreach ($employees as $employee) {
                if (($manager && !empty($employee->code)) || !$manager) {
                    $sel = ($employee->id == $selected) ? ' selected' : '';
                    $html .= "<option value=\"$employee->id\"$sel>$employee->name</option>";
                }
            }
        }
        return $html;
    }


    /**
     * Return JSON data to prepopulate jQuery tokenizer plugin
     * with employee id => name key => value pairs
     * Typically used after $_POST to refill the form for
     * the user's convenience
     * @static
     * @param $employees A string with employee ID-s separated by +
     * @return NULL|JSON
     */
    public static function prepopulate_tokenizer($employees) {
        if (!empty($employees)) {
            $return = array();
            $e = New Model_Employee();

            foreach (explode('+', $employees) as $employee) {
                $data = $e->get($employee);
                $return[] = array('id' => $employee, 'name' => $data->name);
            }
            return json_encode($return);
        }
        return NULL;
    }

}