<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Bills
{

    /**
     * Generate the title for the bill table
     * @static
     * @param $filters
     * @param bool $confirmed Is the bill confirmed?
     * @return string
     */
    public static function bill_title($filters, $confirmed = FALSE) {
        if (!isset($filters['object_name'])) {
            $o = New Model_Object();
            $object_data = $o->get($filters['object']);
            $filters['object_name'] = $object_data->name;
        }

        $title = "Arve objektile " . $filters['object_name'];

        if (empty($filters['task_id'])) {
            $title .= " perioodil " . $filters['start'] . " - " . $filters['end'];
        } else {
            $title .= " töö #" . $filters['task_id'] . " kohta";
        }

        if ($confirmed) {
            $title = '<span class="error-text" style="font-variant: small-caps;"><strong>[ Kinnitatud ] &nbsp;</strong></span>' . $title;
        }

        return $title;
    }


    /**
     * Return table rows for every previously made bill for $object
     * @static
     * @param $object_id
     * @return null|string
     */
    public static function list_prev_bills($object_id) {
        $dir = DOCROOT . 'assets/pdf/' . $object_id;

        if (!is_numeric($object_id) || $object_id < 1 || !file_exists($dir)) {
            return __('Selle objekti kohta pole koostatud ühtegi arvet.');
        }

        $html = NULL;
        $bills = scandir($dir);

        if (!empty($bills)) {
            foreach ($bills as $bill) {
                $info = pathinfo($dir . '/' . $bill);
                if ($info['extension'] == 'pdf' && !stristr($bill, 'report')) {
                    $file = $object_id . '/' . $bill;
                    $html .= "<li><a href=\"" . URL::base() . "api/request_pdf?file=$file\" title=\"\">$bill</a></li>";
                }
            }
        }
        return $html;
    }



    /**
     * Returns data to generate a selectbox of unbilled tasks
     * @static
     * @return array an assoc array with task_id's as keys
     */
    public static function list_unbilled_tasks() {
        $t = New Model_Task();
        $tasks = $t->get_unbilled();
        $options = array('' => 'Kõik perioodi tööd');

        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                $options[$task->id] = "#{$task->id} [$task->object_name] ".Text::limit_chars($task->description, 30);
            }
        }
        return $options;
    }

}