<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Objects
{


    /**
     * @static
     * @param null|int $selected
     * @param bool $allow_empty Add an empty selection value
     * @return null|string
     */
    public static function area_select($selected = NULL, $allow_empty = FALSE) {
        $o = New Model_Object();
        $areas = $o->get_areas()->as_array('id', 'name');
        if ($allow_empty) {
            array_unshift($areas, '');
        }
        return Form::select('area', $areas, $selected, array('id' => 'area-select'));
    }





    /**
     * @static
     * @param null $selected_item
     * @return null|string
     */
    public static function objects_select($selected_item = NULL) {
        $o = New Model_Object();

        $output = NULL;

        $filters = $_GET;
        $objects = $o->get();

        if (!empty($objects)) {

            foreach ($objects as $object) {
                $selected = '';

                if (!empty($filters)) {
                    if ((isset($filters['object']) && $filters['object'] == $object->id)) {
                        $selected = ' selected ';
                    }
                }
                if ($selected_item == $object->id) {
                    $selected = ' selected ';
                }
                $output .= '<option  value="' . $object->id . '"' . $selected . 'cid="' . $object->client_id . '">' . $object->name . '</option>';
            }
        }
        return $output;
    }

    /**
     * @static
     * @param null $selected_item
     * @return null|string
     */
    public static function objects_select_with_code($selected_item = NULL) {

        function ComparisonDelegate($a, $b)
        {
            if ($a->code == $b->code) {
                return 0;
            }
            return ($a->code < $b->code) ? -1 : 1;
        }

        $o = New Model_Object();

        $output = NULL;

        $filters = $_GET;
        $objects = $o->get();

        if (!empty($objects)) {

            $ob = array();
            foreach ($objects as $object) {
                array_push($ob, $object);
            }
            usort($ob, "ComparisonDelegate");
            foreach ($ob as $object) {
                $selected = '';

                if (!empty($filters)) {
                    if ((isset($filters['object']) && $filters['object'] == $object->id)) {
                        $selected = ' selected ';
                    }
                }
                if ($selected_item == $object->id) {
                    $selected = ' selected ';
                }
                $output .= '<option  value="' . $object->id . '"' . $selected . 'cid="' . $object->client_id . '">' .$object->code. " - ". $object->name . '</option>';
            }
        }
        return $output;
    }


    public static function files_table_rows($object_id) {
        $html = NULL;
        $o = New Model_Object();
        $files = $o->get_files($object_id);
        if (empty($files)) {
            return $html;
        }

        foreach ($files as $filename => $file) {
            $html .= '<tr>
            <td>' . $filename . '</td>
            <td>' . $file["uploaded"] . '</td>
            <td><a href="' . $file["url"] . '" title="Laadi alla">Laadi alla</a></td>
            </tr>';
        }
        return $html;
    }
}