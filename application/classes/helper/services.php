<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Services
{

    /**
     * @static
     * Generate service dropdown options
     * @param null $selected
     * @return null|string
     */
    public static function service_options($selected = NULL) {
        $html = NULL;
        $s = New Model_Service();
        $services = $s->get();
        if (!empty($services)) {
            foreach ($services as $service) {

                $sel = ($service->id == $selected) ? ' selected' : '';
                $html .= "<option value=\"$service->id\"$sel>$service->name</option>";

            }
        }
        return $html;
    }

}