<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * @package User
 * @since 1.1.2
 */
class Text extends Kohana_Text
{
    /**
     * Transform boolean input into words
     *
     * @static
     * @param int $bit
     * @return string
     */
    public static function bit2str($bit = 0) {
        return $bit ? __('Jah') : __('Ei');
    }
}