<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Settings extends Controller_Main
{

    public function action_index() {

        $p = $this->request->post();

        // Edit settings form posted
        if (isset($p['save_settings'])) {

            $this->settings->set('extra_price', (string)Helper_Template::mysql_float($p['extra_price'], 3));
            $this->settings->set('km_price', (string)Helper_Template::mysql_float($p['km_price'], 3));
            $this->settings->set('reminder_offset', (int)$p['reminder_offset']);
            $this->request->redirect('settings');

        } elseif (isset($p['add_account'])) {
            $this->settings->add_account($p['account_number']);
        }


        $this->content->settings = $this->settings->get();
        $this->content->accounts = unserialize($this->settings->get('billing_accounts'));
    }

    public function action_delete_account() {
        $account = $this->request->param('id');
        $s = New Model_Settings();
        $s->delete_account($account);
        $this->request->redirect('settings');
    }
}
