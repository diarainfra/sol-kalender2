<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api extends Controller
{

    /**
     * Returns JSON data for employee searching
     * @return void
     */
    public function action_employee_search() {
        $q = $this->request->post('q');

        $e = New Model_Employee();
        $result = $e->search($q);

        if (!empty($result)) {
            die(json_encode($result));
        }
        die();
    }



    /**
     * Return the task for a particular employee
     * @return void
     */
    public function action_employee_calendar() {
        $e = New Model_Employee();
        $t = New Model_Task();

        $eid = $this->request->param('id');
        $data = array();

        if (!is_numeric($eid) || !$e->exists($eid)) {
            die('No such employee');
        }

        $tasks = $e->get_tasks($eid);

        if (empty($tasks)) {
            die();
        }

        // Loop over all tasks
        foreach ($tasks as $task) {
            if ($t->exists($task->task_id)) {
                // Get the involved employees
                $employees = $t->assoc_employees($task->task_id);

                $names = 'Töö';
                $participants = array();

                if (!empty($employees)) {
                    $names = NULL;
                    foreach ($employees as $employee) {
                        $names .= $employee->name . ', ';
                        $participants[] = $employee->id;
                    }
                    $names = trim($names, ', ');
                }

                $data[] = array(
                    'id' => $task->id,
                    'title' => $names,
                    'start' => $task->started,
                    'end' => $task->finished,
                    'url' => URL::base() . 'task/view/' . $task->task_id
                );
            }

        }

        die(json_encode($data));
    }


    /**
     * Returns a field from SQL table for AJAX queries
     * @return void
     */
    public function action_get() {
        $table = @$_POST['table'];
        $field = @$_POST['field'];
        $id = @$_POST['id'];

        $m = New Model_Main();
        die((string)$m->ajax_get_field($table, $field, $id));
    }


    /**
     * Request the download of a .htaccess protected PDF
     * @return void
     */
    public function action_request_pdf() {

        if (!Auth::instance()->logged_in()) {
            die('Not logged in!');
        }

        $file_name = DOCROOT . 'assets/pdf/' . $this->request->query('file');
        if (!file_exists($file_name) || strstr($file_name, '..')) {
            die('No file');
        }

        try {
            $download_name = explode('/', $this->request->query('file'));
            $download_name = $download_name[1];
        } catch (Exception $e) {
            $download_name = 'report_' . time() . '.pdf';
        }

        header('Content-type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $download_name);
        echo file_get_contents($file_name);
    }


    /**
     * Request the download of a protected Object attachment file
     * @return void
     */
    public function action_request_attachment() {
        if (!Auth::instance()->logged_in()) {
            die('Not logged in!');
        }

        $file_name = DOCROOT . 'assets/objects/' . $this->request->query('file');
        if (!file_exists($file_name) || strstr($file_name, '..')) {
            die('No file');
        }

        $info = pathinfo($file_name);
        $download_name = $info['filename'] . '.' . $info['extension'];
        header('Content-type: application/force-download');
        header('Content-Disposition: attachment; filename=' . $download_name);
        echo file_get_contents($file_name);
    }


    /**
     * Output HTML with the client's objects as <select>
     * Used on New Task Page for getting a list of client's objects
     * The client_id is in $_POST['client_id']
     * @return void
     */
    public function action_client_objects() {
        $p = $this->request->post('client_id');

        $c = New Model_Client();
        $objects = $c->objects($p);

        if ($objects->count() == 0) {
            die(Form::select('object', array(0 => 'Objekte pole')));
        }

        die(Form::select('object', $objects->as_array('id', 'name')));
    }


}
