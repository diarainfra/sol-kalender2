<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Bills extends Controller_Main
{


    /**
     * Index page, select bill filters
     *
     * @since 1.0
     */
    public function action_index()
    {
        // Autofill period select
        $this->content->filters = array();
        $this->content->filters['period'] = '01.' . date('m.Y', strtotime('-1 month'));
        Assets::preset('ui')
                ->preset('date');
    }


    /**
     * Filters set, view bill form
     *
     * @since 1.0
     */
    public function action_view()
    {
        $this->content = View::factory('bills/index');

        // Filters for getting tasks come from $_GET
        $filters = $this->request->query();

        // Internal / external bills are slightly different
        if (!empty($filters['internal_bill']) || isset($_POST['confirm-internal-bill'])) {
            define('INTERNAL_BILL', TRUE);
        } else {
            define('INTERNAL_BILL', FALSE);
        }


        if (isset($filters['bill_list'])) {
            Request::current()->redirect('bills/list/' . @$filters['object']);
        }

        // Try getting the billing period from the filters
        try {
            $period = explode('.', $filters['period']);
            if (count($period) !== 3) {
                die('Wrong date format');
            }
        } catch (Exception $e) {
            throw new Kohana_Exception('Wrong date format');
        }

        // Use different view for internal/external bills
        if (INTERNAL_BILL) {
            $view = $this->content->report = View::factory('bills/internal');
        } else {
            $view = $this->content->report = View::factory('bills/view');
        }

        $r = New Model_Report();

        // Get data for the current object
        $object_data = $this->object->get($filters['object']);

        $filters['object_name'] = $object_data->name;
        $filters['start'] = $filters['period'];
        $filters['end'] = cal_days_in_month(CAL_GREGORIAN, $period[1], $period['2']) . '.' . $period[1] . '.' . $period[2];

        $view->filters = $this->content->filters = $filters;

        $view->object = $object_data;


        /* Key lines that fetch data to populate materials/bill rows */

        // Last param (billed) 0 for fetching only unbilled tasks
        // Only outside contract for calc. bill because we have no data about the cost of in-contract tasks


        if (isset($filters['task_id']) && $this->task->exists($filters['task_id'])) {
            // Make the bill contain only 1 task
            $view->tasks_in_range = $view->outside_contract_tasks = array($filters['task_id']);
            $view->in_contract_tasks = NULL;
            $view->outside_contract_data = array($this->task->get($filters['task_id']));


        } else { // The bill contains all suitable tasks (not only 1)

            $filters['billed'] = 0;
            $filters['internal_bill'] = INTERNAL_BILL;

            $view->outside_contract_data = $r->range(array_merge($filters, array('payment' => 1)));

            // For generating used materials
            $view->tasks_in_range = $this->task->tasks_in_range($filters);

            $view->in_contract_tasks = $this->task->tasks_in_range(array_merge($filters, array('payment' => 0)));
            $view->outside_contract_tasks = $this->task->tasks_in_range(array_merge($filters, array('payment' => 1)));
        }


        $view->tasks_data = $r->range($filters); // Only ever needed for internal bills and car costs

        // Data for other fields

        // Used materials sums for in+outside contract tasks
        $view->materials_sum = $this->task->used_materials_sum($view->tasks_in_range, FALSE); // Internal bill materials sum for both in/out tasks
        $view->materials_sum_in = $this->task->used_materials_sum($view->in_contract_tasks, TRUE); // Arvealus table
        $view->materials_sum_out = $this->task->used_materials_sum($view->outside_contract_tasks, TRUE); // Arvealus table

        $view->car_sums = $this->task->car_sums($view->tasks_data, TRUE); // Car/transportation costs for in+out contract tasks
        $view->jobs_sum = Helper_Reports::sums($view->outside_contract_data, 'sum'); // Arvealus table


        $view->client = $this->client->get($object_data->client_id);
        $view->accounts = $this->settings->get_accounts();
        $view->user = Auth::instance()->get_user();


        // Only for internal bill
        if (INTERNAL_BILL) {
            $e = Model::factory('employee');
            $employee_id = Auth::instance()->get_user()->employee_id;
            if (empty($employee_id)) {
                die('Need employee ID!');
            }
            $view->employee = $e->get($employee_id); // Login user
            $view->object_employee = $e->get($object_data->employee_id); // Object manager
        }

        // TOTAL SUM
        $view->total_sum = $view->materials_sum_in + $view->materials_sum_out + $view->jobs_sum + $view->car_sums['price'];

        // Confirm the bill
        if (isset($_POST['confirm-bill']) || isset($_POST['confirm-internal-bill'])) {
            $view->pdf_file = NULL;

            // Save the bill as PDF for archival / e-mailing
            $p = New Model_Pdf();
            $report_content = clone $view; // The view needs cloning because casting it to a string destroys the view object.

            $report_content->pdf_checkpoint = TRUE;
            $file_prefix = (INTERNAL_BILL) ? 'sisearve_' : 'arve_';
            $file_prefix .= $filters['start'];

            $view->pdf_file = $p->generate_bill_pdf((string)$report_content, $object_data->id, $file_prefix);

            $view->email_form = View::factory('reports/email_form', array('attachment' => $view->pdf_file,
                'type' => (INTERNAL_BILL) ? 'internal_bill'
                        : 'bill',
                'filters' => $filters));
            $b = New Model_Bill();
            $b->confirm_bill($view->tasks_in_range);

        }

        Assets::preset('ui')
                ->preset('date')
                ->use_script('modal');
    }


    public function action_email()
    {
        $p = $this->request->post();
        if (!empty($p)) {
            $b = New Model_Bill();

            if ($b->send($p) == FALSE) {
                Notify::msg('Kirja saatmine ei §nnestunud.', 'error');
            } else {
                Notify::msg('Arve on saadetud.');
            }
            $this->request->redirect('bills');
        }
    }


    public function action_list()
    {
        $object_id = $this->request->param('id');

        $this->content->object = $this->object->get($object_id);
    }

}
