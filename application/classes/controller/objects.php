<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Objects extends Controller_Main
{

    protected $_o;

    public function before()
    {
        parent::before();
        $this->_o = Model::factory('object');
    }

    public function action_index()
    {

        $this->content->objects = $this->_o->get();
        $this->content->o = $this->_o;

        $p = $this->request->post();

        // New object form posted
        if (!empty($p)) {
            $status = $this->object->add($p);
            if (!is_integer($status)) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect('objects');
        }
        Assets::use_script(array('modal', 'tablesorter'));
    }


    public function action_edit()
    {
        $oid = $this->request->param('id');
        if (!$this->object->exists($oid)) {
            Notify::msg('Objekti ei leitud!', 'error');
            $this->request->redirect('objects');
        }
        $this->content->object = $this->object->get($oid);

        $p = $this->request->post();

        // Object edit form posted
        if (!empty($p['edit_object'])) {
            $status = $this->object->update($p);
            if ($status !== TRUE) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect('objects');

            // Manage contacts
        } elseif (!empty($p['add_contact'])) {
            $this->object->add_contact($oid, @$p['contact_id']);
        } elseif (!empty($p['remove_contact_id'])) {
            $this->object->remove_contact($oid, @$p['remove_contact_id']);
        }

        $this->content->upload_settings = Kohana::$config->load('upload');
    }


    public function action_upload()
    {

        $save_result = $this->object->add_file($this->request->post('object_id'), @$_FILES['file']);
        if (is_array($save_result) || $save_result == FALSE) {
            Notify::msg('Faili lisamine ebaõnnestus. Palun kontrolli, et fail vastaks allpool olevatele nõudmistele.', 'error');
        }
        $this->request->redirect('objects/edit/' . Request::current()->post('object_id'));
    }


    public function action_delete()
    {
        $id = $this->request->param('id');
        if ($this->object->delete('objects', $id)) {
            Notify::msg('Objekt on märgitud kustutatuks.');
        }
        $this->request->redirect('objects');
    }
}
