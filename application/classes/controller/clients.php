<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Clients extends Controller_Main
{
    protected $_c;

    public function before()
    {
        parent::before();
        $this->_c = Model::factory('client');
    }

    public function action_index()
    {

        $this->content->c = $this->_c;
        $this->content->clients = $this->_c->get();

        $p = $this->request->post();

        // New client form posted
        if (!empty($p)) {
            $status = $this->client->add($p);
            if (!is_integer($status)) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect('clients');
        }

        Assets::use_script(array('modal','tablesorter'));
    }


    public function action_edit()
    {

        $cid = $this->request->param('id');
        $this->content->client = $this->client->get($cid);

        $p = $this->request->post();

        // Edit
        if (!empty($p['edit_client'])) {
            $status = $this->client->update($p);
            if ($status !== TRUE) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect('clients');

            // Manage contacts
        } elseif (!empty($p['add_contact'])) {
            $this->client->add_contact($cid, @$p['contact_id']);
        } elseif (!empty($p['remove_contact_id'])) {
            $this->client->remove_contact($cid, @$p['remove_contact_id']);
        }
    }


    public function action_new()
    {
    }


    public function action_delete()
    {
        $id = $this->request->param('id');
        if ($this->client->delete('clients', $id)) {
            Notify::msg('Klient on märgitud kustutatuks. Temaga seotud objektid vajavad ümberseostamist.');
        }
        $this->request->redirect('clients');
    }
}
