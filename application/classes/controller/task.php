<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Task extends Controller_Main
{

//    public function before() {
//        parent::before();
//
//        // Show error if the task is nonexistent or deleted
//        if (in_array($this->request->action(), array('edit', 'view', 'delete'))) {
//            $tid = (int)$this->request->param('id');
//
//            if (empty($tid) || $this->task->is_deleted('tasks', $tid) || !$this->task->exists($tid)) {
//                Notify::msg('Töö, mida üritate vaadata, on märgitud kustutatuks või seda ei eksisteeri.', 'error');
//                $this->request->redirect('calendar');
//            }
//        }
//    }


    public function action_new()
    {
        $this->content = View::factory('task/form');
        $this->content->new_task = TRUE;
        $p = $this->request->post();

        // New task form posted
        if (!empty($p['add_task'])) {
            $status = $this->task->add($p);

            if ($status === FALSE) {
                Notify::msg('Kõik nõutud väljad ei olnud täidetud!', 'error');
            } elseif (is_array($status)) {
                foreach ($status as $error) {
                    Notify::msg($error, 'error');
                }
            } else {
                $this->request->redirect("task/view/$status");
            }
        }

        // Refill the form
        $this->content->p = $p;
        $this->content->tokenizer = Helper_Employee::prepopulate_tokenizer(@$p['employees']);
        $this->content->p['repeat_expression'] = '';

        Assets::preset(array('ui', 'date'))
                ->use_script('tokeninput')
                ->use_css('tokeninput');
    }


    /**
     * View basic info about a task
     * @return void
     */
    public function action_view()
    {

        $tid = (int)$this->request->param('id');

        if (empty($tid) && isset($_POST['task_id'])) {
            $tid = (int)$this->request->post('task_id');
        }

        $p = $this->request->post();

        // Add a comment
        if (isset($p['add_comment'])) {
            if (!$this->task->add_comment($p)) {
                Notify::msg('Kommentaari lisamine ei õnnestunud!', 'error');
            }
        }

        $task_data = $this->task->get($tid);
	$material_data = $this->task->all_materials($tid);
        $employees = $this->task->assoc_employees($tid);
        $employees_count = 1;
        $names = NULL;
        if (!empty($employees)) {
            $employees_count = count($employees);
            foreach ($employees as $e) {
                $names .= '<a href="' . URL::base() . 'employee/edit/' . $e->id . '" title="Vaata töötaja andmeid">' . $e->name . '</a>, ';
            }
        }


        View::set_global('task', $task_data);
        View::set_global('material', $material_data);

        $this->content->used_materials = Model_Task::used_materials($tid);
        $this->content->employees = trim($names, ' ,');
        $this->content->object = $this->object->get($task_data->object);
        $this->content->service = $this->service->get($task_data->service);
        $this->content->comments = $this->task->get_comments($tid);
        $this->content->employees_count = $employees_count;

        $rt = New Model_Recurring();
        $this->content->recurring_tasks = $rt->get_recurring(($task_data->duplicate_of) ? $task_data->duplicate_of : $tid);

        $this->content->logs = ORM::factory('log')->get(array('task_id' => $tid));

        Assets::use_script(array('tabs', 'modal'));
    }


    public function action_edit()
    {

        $task_id = $this->request->param('id');

        $this->content = View::factory('task/form');
        $this->content->new_task = FALSE;

        // Fill form with existing data from the DB
        $this->content->p = $task_data = (array)$this->task->get($task_id);


        // Don't show repeat options for duplicates
        if ($task_data['duplicate_of']) {
            $this->content->duplicate = $task_data['duplicate_of'];
        }

        if (!Helper_Task::date_set($this->content->p['started'])) {
            $this->content->p['started'] = NULL;
        }
        if (!Helper_Task::date_set($this->content->p['finished'])) {
            $this->content->p['finished'] = NULL;
        }

        // Employees tokenizer according to the DB
        $e = $this->task->assoc_employees($task_id);


        // If form was posted, refill form fields using $_POST data
        $p = $this->request->post();

        // Edit task form posted
        if (!empty($p['save_task'])) {
            $status = $this->task->update($p);

            if ($status === FALSE) {
                Notify::msg('Töö salvestamine ebaõnnestus. Kas kõik nõutud väljad on täidetud?', 'error');
            } elseif ($status === 101) {
                Notify::msg('Ühtegi sobivat kuupäeva järgmiseks töö korduseks ei leitud.
                Palun tühista töö korduvus või muuda kuupäevi.', 'error');
            } elseif ($status === 102) {
                $max = Kohana::$config->load('app.max_duplicate_count');
                Notify::msg("Valitud ajavahemikul kordub töö rohkem kui $max korda (see on ülempiir).", 'error');

            } elseif (is_array($status)) {
                foreach ($status as $error) {
                    Notify::msg($error, 'error');
                }
            } else {
                $this->request->redirect("task/view/$status");
            }

            // Form data is the one posted, not what was fetched from the DB
            $this->content->p = $p;

            $e = $this->task->assoc_employees($p['id']); // Employees could have changed
        }

        // Build array for tokenizer pre-fill
        $emp = array();
        if (!empty($e)) {
            foreach ($e as $employee) {
                $emp[] = $employee->id;
            }
        }
        $emp = implode('+', $emp);
        $this->content->tokenizer = Helper_Employee::prepopulate_tokenizer($emp);
        $this->content->p['repeat_expression'] = $task_data['repeat_expression'];

        Assets::preset(array('ui', 'date'))
                ->use_script('tokeninput')
                ->use_css('tokeninput');
    }


    public function action_add_materials()
    {

        $p = $this->request->post();
        // Add materials posted
        if (!empty($p['add_materials'])) {
            $status = $this->task->add_materials($p);
            if (!is_numeric($status)) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect("task/view/" . @$p['task_id']);
        }
    }


    public function action_delete_comment()
    {
        $comment_id = $this->request->param('id');

        $c = ORM::factory('comment', $comment_id);
        if (!$c->loaded()) {
            Notify::msg('Kommentaari ei leitud!', 'error');
            $this->request->redirect();
        } else {
            $url = 'task/view/' . $c->task_id;
            Syslog::write(__('Kommentaari #:id kustutamine', array(':id' => $c->id)), array('task' => $c->task_id));
            $c->delete();
            $this->request->redirect($url);
        }
    }

    /**
     * Delete a task material
     *
     * @since 1.2.0
     */
    public function action_delete_material()
    {
        // Get material data
        $material = Model::factory('task_material')
                ->get($this->id);

        // 404
        if (!$material) {
            $this->request->redirect('');
        }

        // Try deleting
        if (!Model_Task_Material::delete_material($this->id)) {
            Notify::msg('Kustutamine ebaõnnestus!', 'error');
        }

        // Redirect back to view task
        $this->request->redirect('task/view/' . $material->task_id);
    }
	public function action_edit_materials()
    {
              $p = $this->request->post();
	$task_id = $p['task_id'];
	$name = $p['name'];
	$unit = $p['unit'];
	$unit_price = $p['unit_price'];
	$quantity = $p['quantity'];
	$update_data = array('name' => $name, 'quantity' => $quantity, 'unit' => $unit, 'unit_price' => $unit_price );
        // Add materials posted
        if (!empty($p['edit_materials'])) {
            $status = $this->task->edit_materials($update_data);
            $this->request->redirect("task/view/" . $task_id);
        }
    }



    public function action_delete()
    {
        $id = $this->request->param('id');
        if ($this->task->delete('tasks', $id)) {
            Notify::msg("Töö #$id on märgitud kustutatuks.");
        }
        $this->request->redirect('calendar');
    }

}
