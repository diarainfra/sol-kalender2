<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Services extends Controller_Main
{

    /**
     * @var Model_Service
     */
    protected $_s;

    public function before() {
        parent::before();
        $this->_s = Model::factory('service');
    }

    public function action_index()
    {

        $p = $this->request->post();

        if (!empty($p)) {

            $status = $this->service->add(@$p['name'], @$p['code']);
            if ($status == FALSE) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            } else {
                $this->request->redirect('services');
            }
        }
        $this->content->services = $this->_s->get();

        Assets::use_script(array('tablesorter', 'modal'));
    }


    public function action_edit()
    {
        $eid = $this->request->param('id');
        $this->content->service = $this->service->get($eid);

        $p = $this->request->post();

        // Edit form posted
        if (!empty($p)) {
            $status = $this->service->update($p);
            if ($status !== TRUE) {
                Notify::msg('Andmed ei valideerunud!', 'error');
            }
            $this->request->redirect('services');
        }
    }


    public function action_delete()
    {
        $id = $this->request->param('id');
        if (Model::factory('main')->delete('services', $id)) {
            Notify::msg("Teenus #$id on märgitud kustutatuks.");
        }
        $this->request->redirect('services');
    }

}
