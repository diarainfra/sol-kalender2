<?php defined('SYSPATH') or die('No direct access allowed.');

class Controller_Public_Auth extends Controller_Public_Main
{

    public function action_index()
    {
        if (Auth::instance()->logged_in()) {
            $this->request->redirect('');
        }

        if (Kohana::$environment !== Kohana::PRODUCTION) {
            Assets::use_script('modal');
        }
    }

    /**
     * Shows the login page and handles login
     * @return void
     */
    public function action_login()
    {

        // Logged in users should not see this page
        if (Auth::instance()->logged_in()) {
            $this->request->redirect('');
        }

        // If the login form was posted...
        if ($this->request->post()) {

            // Try to login
            if (Auth::instance()->login($this->request->post('user'), $this->request->post('pass'), (bool)$this->request->post('remember'))) {

                // Notify the user that a new version has been released
                User::new_release_notification();

                $this->request->redirect('calendar');
            } else {
                Notify::msg('Autentimine ebaõnnestus! ', 'error');
            }
        }
        $this->request->redirect('public');
    }

    public function action_logout()
    {
        Auth::instance()->logout();
        $this->request->redirect('public');
    }

}