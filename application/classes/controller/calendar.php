<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Calendar extends Controller_Main
{

    public function before()
    {
        parent::before();

    }

    public function action_index()
    {
        Assets::use_script(array(
            'fullcalendar',
            'tokeninput',
            'modal'
        ))
                ->use_css(array(
            'fullcalendar',
            'tokeninput'
        ))
                ->preset('ui');
    }

    /**
     * Output JSON data for the main calendar
     *
     * The data forms the main dashboard view with all employee's tasks listed as calendar events
     *
     * @ajax
     * @return void
     */
    public function action_data()
    {
        $t = New Model_Task();
        $o = New Model_Object();

        $start = (int)Request::current()->query('start');
        $end = (int)Request::current()->query('end');


        /*
         * For testing the API only
        if (empty($start)) {
            $start = time()-10000000;
            $end = time();
        }*/

        $data = array();

        if ($start <= 0 || $end <= 0) {
            $this->respond(Commoneer_Controller_Ajax::STATUS_BAD_REQUEST, 'No time range');
        }

        // Get filters as an array
        // Could possibly be replaced by $this->request->query();
        $filters = Request::current()->query();

        $tasks = $t->range($start, $end, $filters);


        if (empty($tasks)) {
            $this->respond(Commoneer_Controller_Ajax::STATUS_OK, 'Nothing to return.');
        }

        // Loop over all tasks for the given period
        foreach ($tasks as $task) {

            $show_result = TRUE; // User filter sets it to false

            // Get the involved employees
            $employees = $t->assoc_employees($task->id);

            $names = '*';
            $participants = array();

            if (!empty($employees)) {
                $names = NULL;
                foreach ($employees as $employee) {
                    $names .= $employee->name . ', ';
                    $participants[] = $employee->id;
                }
                $names = trim($names, ', ');
            }


            // Apply people filter

            if (!isset($filters['employee-type'])) {
                $filters['employee-type'] = 0;
            }
            if (!empty($filters['employees'])) {

                // AND filter - only show a task if ALL the persons are participants
                if ((bool)$filters['employee-type']) {

                    foreach (explode('+', $filters['employees']) as $user) {

                        if (!in_array($user, $participants)) {
                            $show_result = FALSE;
                        }
                    }
                } else {
                    // OR filter - match task with any employee
                    $show_result = FALSE;
                    foreach ($participants as $participant) {
                        if (in_array($participant, explode('+', $filters['employees']))) {
                            $show_result = TRUE;
                        }
                    }
                }
            }

            $object = $o->get($task->object);

            if ($show_result) {
                $data[] = array(
                    'id' => $task->id,
                    'title' => $object->name . ' (' . $names . ')',
                    'start' => $task->started,
                    'end' => $task->finished,
                    'url' => URL::base() . 'task/view/' . $task->id
                );
            }
        }

        // Add in client birthdays with a special class color
        $c = New Model_Client();
        $clients = $c->get();
        if (!empty($clients)) {
            foreach ($clients as $client) {
                if (!empty($client->birthday)) {
                    $data[] = array('id' => 875 + rand(1, 555),
                        'title' => $client->name . ' sünnipäev',
                        'start' => $client->birthday,
                        'end' => $client->birthday,
                        'url' => URL::base() . 'clients/edit/' . $client->id,
                        'allDay' => '1'
                    );
                }
            }
        }

        $comms = $t->get_comms();
        if (!empty($comms)) {
            foreach ($comms as $comm) {
                if (!empty($comm->comment)) {
                    $data[] = array(
				'id' => 875 + rand(1, 555),
                        'title' => 'M�rkus',
                        'start' => $comm->date,
                        'end' => $comm->date,
                        'url' => URL::base() . 'clients/edit/' . $client->id,
                        'allDay' => '1'
                    );
                }
            }
        }


        $this->respond(Commoneer_Controller_Ajax::STATUS_OK, $data);
    }


}