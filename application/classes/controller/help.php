<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Help extends Commoneer_Controller_Template
{

    public function action_index()
    {

    }

    public function action_changelog()
    {
        $this->title = __('Muudatused');
        $changelog = file_get_contents(DOCROOT . 'doc/CHANGELOG.md');
        $this->content = Markdown::instance()->transform($changelog);
    }

}