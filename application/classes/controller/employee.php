<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Employee extends Controller_Main
{

    /**
     * @var Model_Employee
     */
    protected $_e;

    public function before()
    {
        parent::before();
        $this->_e = Model::factory('employee');
    }

    public function action_index()
    {

        $this->content->employees = $this->_e->get();
        $p = $this->request->post();

        // New employee form posted
        if (!empty($p)) {

            $status = $this->employee->add($p);
            if (!is_numeric($status) && is_array($status)) {
                foreach ($status as $error) {
                    Notify::msg($error, 'error');
                }

            } else {
                $this->request->redirect('employee/edit/' . $status);
            }
        }
        Assets::use_script(array('tablesorter', 'modal'));
    }


    public function action_edit()
    {
        $eid = $this->request->param('id');

        $p = $this->request->post();
        if (isset($p['edit'])) {
            $status = $this->employee->edit($p);
            if (!is_numeric($status) && is_array($status)) {
                foreach ($status as $error) {
                    Notify::msg($error, 'error');
                }

            }
            $this->request->redirect('employee');
        }

        $this->content->employee = $this->employee->get($eid);
        $this->content->user = $this->employee->get_user($eid);

        Assets::preset('ui')
                ->use_script('fullcalendar')
                ->use_css('fullcalendar');
    }


    public function action_delete()
    {
        $id = $this->request->param('id');
        if ($this->employee->delete('employees', $id)) {
            Notify::msg("Töötaja #$id on märgitud kustutatuks.");
        }
        $this->request->redirect('employee');
    }

}
