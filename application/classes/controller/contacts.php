<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Contacts extends Controller_Main
{

    public function action_index()
    {
        Assets::instance()->use_script('tablesorter');
    }


    public function action_edit()
    {

        $p = $this->request->post();
        if (!empty($p)) {

            $c = ORM::factory('contact', @$p['contact_id']);
            $c->name = $p['name'];
            $c->email = $p['email'];
            $c->phone = $p['phone'];
            try {
                $c->save();
                $this->request->redirect('contacts');
            } catch (ORM_Validation_Exception $e) {
                Helper_Template::validation_error($e->errors('contacts'));
            }

        }

        $cid = $this->request->param('id');
        $this->content->contact = ORM::factory('contact', $cid);
    }


    public function action_delete()
    {
        $id = $this->request->param('id');
        $c = ORM::factory('contact', $id);
        $c->delete();
        $this->request->redirect('contacts');
    }

}
