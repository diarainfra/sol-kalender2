<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @package Logging
 * @since 1.1.4
 */
class Controller_Log extends Controller_Main
{

    public function before()
    {
        parent::before();
        if (!User::current()->has('roles', 2)) {
            $this->request->redirect('');
        }
    }

    public function action_index()
    {

        $this->content->logs = ORM::factory('log')->limit(100)->find_all();

        Assets::use_script('tablesorter');
    }
}
