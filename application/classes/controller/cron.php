<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cron extends Controller
{

    public function action_index() {

               $this->response->body('Use a direct URI to a cron action!');
    }

    /**
     * Send e-mail reminders to employees
     * about upcoming tasks
     * @return void
     */
    public function action_email_reminders() {
        $count = Model::factory('cron')->send_task_reminders();
        Kohana::$log->add(5, 'Cron: Sent e-mail reminders about ' . $count["tasks"] . ' tasks to ' . $count["employees"] . ' (non-unique) employees.');
    }
}