<?php defined('SYSPATH') or die('No direct script access.');

/**
 * SOL Kalendrirakendus
 * @author Ando Roots
 * @date 2011
 * @copyright Diara OÜ
 */
class Controller_Main extends Commoneer_Controller_Ajax
{

    public $employee;
    public $object;
    public $client;
    public $settings;
    public $service;

    public function before() {
        $this->_require_login = TRUE;

        parent::before();


        // Make models globally accessible within controllers
        $this->employee = New Model_Employee();
        $this->object = New Model_Object();
        $this->task = New Model_Task();
        $this->client = New Model_Client();
        $this->settings = New Model_Settings();
        $this->service = New Model_Service();



        // Don't allow CRUD URLs to be accessed if the object is nonexistent or deleted
        // Structure: controller => array('table' => 'MYSQ_TABLE_NAME', 'actions' => array(LIST_OF_PROTECTED_ACTIONS))
        $CRUD_protected = array('task' => array('table' => 'tasks', 'actions' => array('edit', 'view', 'delete')),
                                'employee' => array('table' => 'employees', 'actions' => array('edit', 'delete')),
                                'clients' => array('table' => 'clients', 'actions' => array('edit', 'delete')),
                                'services' => array('table' => 'services', 'actions' => array('edit', 'delete')),
        );

        // Check if the current controller/action is CRUD-protected
        $controller = $this->request->controller();
        if (array_key_exists($controller, $CRUD_protected) &&
            in_array($this->request->action(), $CRUD_protected[$controller]['actions'])
        ) {
            $id = (int)$this->request->param('id');

            // Check for deletion and existence
            if (empty($id)
                || Model_Main::is_deleted($CRUD_protected[$controller]['table'], $id)
                || !Model_Main::row_exists($CRUD_protected[$controller]['table'], $id)
            ) {
                Notify::msg('Objekt, mida üritate vaadata, on märgitud kustutatuks või seda ei eksisteeri.', 'error');
                $this->request->redirect('calendar');
            }
        }

    }


}

?>
