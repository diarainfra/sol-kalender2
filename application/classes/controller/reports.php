<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @package Report
 */
class Controller_Reports extends Controller_Main
{
    /**
     * Redirect to view
     */
    public function action_index()
    {
        $start = '01' . date('.m.Y');
        $end = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y')) . date('.m.Y');
        $this->request->redirect("reports/view?start=$start&end=$end");
    }


    public function action_view()
    {

        $this->content = View::factory('reports/index');


        $view = $this->content->report = View::factory('reports/view');

        $filters = $this->request->query();

        $r = New Model_Report();

        if (!empty($filters['object'])) {
            $object_data = $this->object->get($filters['object']);
            $filters['object_name'] = $object_data->name;
            $view->object = $object_data;
        }

        $view->filters = $this->content->filters = $filters;

        $this->content->email_form = (string)View::factory('reports/email_form', array('type' => 'report', 'filters' => $this->content->filters));


        $view->in_contract_data = $r->range(array_merge($filters, array('payment' => 0)));
        $view->outside_contract_data = $r->range(array_merge($filters, array('payment' => 1)));
        $view->tasks_in_range = $this->task->tasks_in_range($filters); // For generating used materials

        $view->materials_sum = $this->task->used_materials_sum($view->tasks_in_range, TRUE); // Arvealus table
        $view->jobs_sum = Helper_Reports::sums($view->outside_contract_data, 'sum'); // Arvealus table


        // Generate PDF
        if (isset($filters['pdf'])) {

            $p = New Model_Pdf();
            $report_content = clone $view;

            $file = explode('assets/pdf/', $p->generate_report_pdf((string)$report_content, $filters));

            $this->request->redirect('api/request_pdf?file=' . $file[1]);
        }

        Assets::preset(array('ui', 'date'))
                ->use_script(array(
            'tablesorter',
            'modal',
        ));
    }


    /**
     * Accepts POST data and sends the report as a PDF attachment by email
     * @return void
     */
    public function action_email()
    {
        $p = $this->request->post();
        if (!empty($p)) {
            $r = New Model_Report();

            if ($r->send($p) == FALSE) {
                Notify::msg('Kirja saatmine ei õnnestunud.', 'error');
            } else {
                Notify::msg('Aruanne on saadetud.', 'success');
            }
            $this->request->redirect('reports');
        }
    }


    /**
     * Download the report as a Excel file
     * @return void
     */
    public function action_excel()
    {
        $object = $this->request->param('id');
        $start = $this->request->param('param1');
        $end = $this->request->param('param2');

        $r = New Model_Report();
        $file = $r->generate_xls($object, str_replace('-', '.', $start), str_replace('-', '.', $end));
        $this->request->redirect($file);
    }


}
