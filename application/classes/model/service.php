<?php defined('SYSPATH') or die('No direct script access.');

class Model_Service extends Model_Main
{


    /**
     * Returns data about a service
     * @param null $eid If not set, return all
     * @return Database_Result|object
     */
    public function get($eid = NULL) {
        return $this->get_data('services', $eid);
    }


    /**
     * Check if a employee exists
     * @param  $who
     * @return bool
     */
    public function exists($who) {
        if (is_numeric($who)) {
            $data = $this->get($who);
            return !empty($data);
        }
        return FALSE;
    }


    /**
     * @param string $name
     * @param string $code
     * @return Database_Result|object
     */
    public function add($name, $code) {

        $p = array('name' => $name, 'code' => $code);

        $post = Validation::factory($p)
                ->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':value', '100'))
                ->rule('code', 'max_length', array(':value', '10'))
                ->rule('name', 'min_length', array(':value', '2'));

        if ($post->check()) {
            $query = DB::insert('services', array('name', 'code'))
                    ->values(array($post['name'], $post['code']));
            $result = $query->execute();

            if (is_numeric($result[0])) {
                Syslog::write(__('Teenus #:id lisatud', array(':id' => $result[0])));
                return $result[0];
            }
            return $result;
        }
        return FALSE;
    }


    public function update($data) {
        $post = Validation::factory($data)
                ->bind(':name', $data['name'])
                ->bind(':code', $data['code'])

                ->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':name', '100'))
                ->rule('name', 'min_length', array(':name', '2'))

                ->rule('id', 'digit');

        if ($post->check()) {
            $query = DB::update('services')
                    ->set(array('name' => $post['name'],
                               'code' => $post['code']))
                    ->where('id', '=', $post['id']);
            $query->execute();
            Syslog::write(__('Teenuse #:id andmed muudetud', array(':id' => $data['id'])));
            return TRUE;
        }

        return FALSE;
    }

}