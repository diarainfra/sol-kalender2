<?php defined('SYSPATH') or die('No direct script access.');

class Model_Comment extends ORM
{

    protected $_table_name = 'task_comments';

    public function rules() {
        return array(
            'comment' => array(
                array('not_empty')
            ),
            'user_id' => array(
                array('digit')
            ),
            'task_id' => array(
                array('digit')
            )
        );
    }


}