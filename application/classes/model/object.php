<?php defined('SYSPATH') or die('No direct script access.');

class Model_Object extends Model_Main
{


    /**
     * Returns data about a object/objects
     * @param null $oid If not set, return all
     * @return Database_Result|object
     */
    public function get($oid = NULL)
    {

        $q = DB::select('objects.*',
            array('areas.name', 'area_name'),
            array('clients.name', 'client_name'),
            array('employees.name', 'employee_name'),
            array('employees.code', 'employee_code'))
                ->from('objects')
                ->as_object()

                ->join('areas', 'left')
                ->on('objects.area', '=', 'areas.id')

                ->join('clients', 'left')
                ->on('objects.client_id', '=', 'clients.id')

                ->join('employees', 'left')
                ->on('objects.employee_id', '=', 'employees.id');

        if ($oid !== NULL && $oid > 0) {
            return $q->where('objects.id', '=', (int)$oid)
                    ->execute()
                    ->current();
        } else {
            $q->where('objects.deleted', '=', '0');
        }
        return $q->execute();
    }


    /**
     * @param  $data - assoc array with data to be inserted, usually from $_POST
     * @return bool Validation success or failure
     */
    public function add($data)
    {
        $post = Validation::factory($data)
                ->bind(':name', $data['name'])
                ->bind(':code', $data['code'])
                ->bind(':area', $data['area'])

                ->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':name', '80'))
                ->rule('name', 'min_length', array(':name', '2'))

                ->rule('code', 'not_empty')
                ->rule('code', 'max_length', array(':code', '15'))
                ->rule('code', 'min_length', array(':code', '1'))

                ->rule('area', 'not_empty')
                ->rule('area', 'max_length', array(':area', '8'))
                ->rule('area', 'digit')

                ->rule('client_id', 'not_empty')
                ->rule('client_id', 'digit');

        if ($post->check()) {
            if (!$this->area_exists($post['area'])) {
                return FALSE;
            }

            $query = DB::insert('objects', array('client_id', 'name', 'code', 'area'))
                    ->values(array($post['client_id'], $post['name'], $post['code'], $post['area']))
                    ->execute();

            // Object and Client both have contacts, since object belongs
            // To a Client, pre-fill object contacts with the Client's
            $c = ORM::factory('contact');
            $client_contacts = $c->entity_contacts('client', $post['client_id']);
            if (!empty($client_contacts)) {
                foreach ($client_contacts as $contact_id) {
                    $c->bind('object', $query[0], $contact_id);
                }
            }
            Syslog::write(__('Objekt #:id lisatud', array(':id' => $query[0])));
            return $query[0];
        }

        return FALSE;
    }


    /**
     * @description Returns area name or id
     * @param  $area Either area id or area name
     * @return Database_Result|object
     */
    public function get_area($area)
    {
        if (is_numeric($area)) {
            $q = DB::select('name')
                    ->from('areas')
                    ->where('id', '=', $area);
        } else {
            $q = DB::select('id')
                    ->from('areas')
                    ->where('name', '=', $area);
        }
        return $q->as_object()
                ->execute()->current();
    }


    /**
     * Returns a list of all the areas
     * @return Database_Result|object
     */
    public function get_areas()
    {
        return DB::select()
                ->from('areas')
                ->distinct(TRUE)
                ->as_object()
                ->execute();
    }

    /**
     * Add a new area
     * @param  $name
     * @return bool
     */
    private function add_area($name)
    {
        if (!empty($name) && is_string($name) && strlen($name) > 3) {
            $query = DB::insert('areas', array('name'))
                    ->values(array($name));
            list($insert_id, $affected_rows) = $query->execute();
            Syslog::write(__('Piirkond #:id lisatud', array(':id' => $insert_id)));
            return $insert_id;
        }

        return FALSE;
    }


    /**
     * Check if the area exists
     * @param  $name
     * @return bool
     */
    public function area_exists($name)
    {
        if (!empty($name)) {
            $data = $this->get_area($name);
            return (bool)$data;
        }
        return FALSE;
    }


    /**
     * Does the object exist in the database?
     * @param  $id
     * @return bool
     */
    public function exists($id)
    {
        if (!empty($id)) {
            $q = DB::select('id')
                    ->from('objects')
                    ->where('id', '=', $id)
                    ->execute();
            return (bool)$q->count();
        }
        return FALSE;
    }


    public function update($data)
    {
        $post = Validation::factory($data)
                ->bind(':name', $data['name'])
                ->bind(':code', $data['code'])
                ->bind(':area', $data['area'])

                ->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':name', '80'))
                ->rule('name', 'min_length', array(':name', '2'))

                ->rule('id', 'digit')

                ->rule('code', 'not_empty')
                ->rule('code', 'max_length', array(':code', '15'))
                ->rule('code', 'min_length', array(':code', '1'))

                ->rule('client_id', 'digit')

                ->rule('area', 'not_empty')
                ->rule('area', 'max_length', array(':area', '8'))
                ->rule('area', 'digit')
                ->rule('area', 'min_length', array(':area', '1'))
                ->rule('employee_id', 'Helper_Template::optional_field', array(':value'));

        if ($post->check()) {
            $query = DB::update('objects')
                    ->set(array('client_id' => $post['client_id'],
                'name' => $post['name'],
                'code' => $post['code'],
                'area' => $post['area'],
                'employee_id' => $post['employee_id']))
                    ->where('id', '=', $post['id']);
            $query->execute();
            Syslog::write(__('Objekti #:id andmed muudetud', array(':id' => $post['id'])));
            return TRUE;
        }

        return FALSE;
    }


    /**
     * Returns a list of all the attachment files the object has.
     * Object's attachments are held in APPPATH/assets/objects/OBJECT_ID/
     * @param $object_id
     * @return array|null Assoc array with file info or NULL when there are no files
     */
    public function get_files($object_id)
    {
        $dir = "assets/objects/$object_id/";
        if (!file_exists(DOCROOT . $dir)) {
            return NULL;
        }
        $files = scandir(DOCROOT . $dir);
        if (empty($files)) {
            return NULL;
        }

        $output = array();
        foreach ($files as $file) {
            $full_name = DOCROOT . $dir . $file;
            $info = pathinfo($full_name);
            if (!in_array($file, array('.', '..'))) {
                $output[$file] = array('uploaded' => date('d.m.Y', filemtime($full_name)),
                    'url' => URL::base() . 'api/request_attachment/?file=' . $object_id . '/' . $file);
            }
        }
        return $output;
    }


    /**
     * Validate file upload and save the uploaded file
     * @param $object_id The object_id who owns the file
     * @param $file $_FILE from $_POST
     * @return bool|string FALSE if upload failed, full path to the file if successful
     */
    public function add_file($object_id, $file)
    {
        if (empty($file) || !$this->exists($object_id)) {
            return FALSE;
        }
        $dir = DOCROOT . 'assets/objects/' . $object_id;

        $settings = Kohana::$config->load('upload');
        if (!file_exists($dir)) {
            mkdir($dir);
        }

        $file = array('file' => $file);
        $post = Validation::factory($file)
                ->rule('file', 'Upload::not_empty', array(':value'))
                ->rule('file', 'Upload::size', array(':value', $settings['max_size']))
                ->rule('file', 'Upload::type', array(':value', $settings['allowed_extensions']))
                ->rule('file', 'Upload::valid');

        if ($post->check()) {
            Syslog::write(__('Lisatud fail :file objektile #:object', array(':object' => $object_id, ':file' => $file['file']['name'])));
            return Upload::save($file['file'], NULL, $dir);
        } else {
            return FALSE;
        }
    }


    /**
     * One object can have many contact persons.
     * This adds a new row to the object_contacts table
     * @param $object_id
     * @param $contact_id
     * @return int|bool $insert_id The index table's row ID
     */
    public function add_contact($object_id, $contact_id)
    {
        $c = ORM::factory('contact');
        return $c->bind('object', $object_id, $contact_id);
    }

    /**
     * Remove one contact from the object
     * @param $object_id
     * @param $contact_id
     * @return void
     */
    public function remove_contact($object_id, $contact_id)
    {
        $c = ORM::factory('contact');
        return $c->unbind('object', $object_id, $contact_id);
    }


    /**
     * Get a list of contact_id's
     * @param $object_id
     * @return array|bool
     */
    public function get_contacts($object_id)
    {
        $c = ORM::factory('contact');
        return $c->entity_contacts('object', $object_id);
    }
}