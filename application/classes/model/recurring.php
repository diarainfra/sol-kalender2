<?php defined('SYSPATH') or die('No direct script access.');

class Model_Recurring extends Model_Task
{
    public function before()
    {
        parent::before();
    }


    /**
     * Returns an array of dates that match the $expression and fall between $start_date and $end_date
     *
     * N.B! For some reason the method doesn't work with CRON expression times other than midnight?
     *
     * @param string $start_date Minimum date delimiter
     * @param string $end_date Maximum date delimiter
     * @param string $expression A CRON temporal expression specifying the frequency
     * @return array|bool An array of dates the task should repeat. Does not include the original $start_date
     */
    public function calc_range($start_date, $end_date, $expression)
    {


        // Input date might be in a variety of formats
        // Minimum allowed unit is a day so discard everything else
        $start = strtotime(date('Y-m-d', strtotime($start_date))) - 1;
        $end = strtotime(date('Y-m-d', strtotime($end_date)));
        $start_date = date('Y-m-d', strtotime($start_date));

        if (!$start || !$end || ($end - $start) < 1 || empty($expression)) {
            return FALSE;
        }


        // Calculate the number of steps ie days in between start and end dates
        $days = ceil(($end - $start) / 86400);

        $occurrences = array(); // The return array
        $next = 0;


        /**
         * Create a new instance of the Cron module
         * We only use the next() function (the job is never run)
         * callback continue is just a placeholder
         */
        $cron = New Kohana_Cron($expression, 'continue');

        // Loop over each day, checking if the cron falls within that day
        for ($i = 1; $i <= $days; $i++) {

            $next = $cron->next($start); // Get the next scheduled cron run
            $next_date = date('Y-m-d', $next);

            // The tasks run at 00:10, but start/end delimiters don't include time.
            // That's why +601s: include the end date
            if ($next > $end + 601) {
                break;
            }

            $occurrences[] = $next_date; // This date is in the schedule
            $start = $next; // Increment the start timestamp
        }

        if (in_array($start_date, $occurrences)) {
            unset($occurrences[array_search($start_date, $occurrences)]);
        }

        return array_unique($occurrences);
    }


    /**
     * Returns a CRON string
     *
     * @static
     * @param string|array $day
     * @param string|array $month
     * @param string|array $weekday
     * @return string A CRON temporal expression
     */
    public static function cron_encode($day, $month, $weekday)
    {

        if (is_array($day)) {

            $day = implode(',', $day);
            if (empty($day)) {
                $day = '*';
            }
        }

        if (is_array($month)) {

            $month = implode(',', $month);
            if (empty($month)) {
                $month = '*';
            }
        }

        if (is_array($weekday)) {

            $weekday = implode(',', $weekday);

            if (empty($weekday) && $weekday != 0) {
                $weekday = '*';
            }
        }

        // 10 minutes past midnight because there seems to be a bug with the CRON lib if minutes are *
        return "10 0 $day $month $weekday";
    }


    /**
     * Returns an assoc array of time data from a CRON temporal expression
     * Example: Cron: * * 1,2 * 0
     * Result: array('day' => array(1, 2), 'month' => 0, day_of_week => 0)
     * @static
     * @param string $expression A CRON expression
     * @param null|string $return If set, returns only one key instead of an array
     * @return array|null|string
     */
    public static function cron_decode($expression, $return = NULL)
    {
        if (empty($expression)) {
            return $expression;
        }
        $pieces = explode(' ', $expression);

        $output = array();
        $i = 0;
        foreach ($pieces as $piece) {
            if (strpos($piece, ',')) {
                $piece = explode(',', $piece);
            }

            switch ($i) {
                case 2:
                    $key = 'day';
                    break;
                case 3:
                    $key = 'month';
                    break;
                case 4:
                    $key = 'day_of_week';
                    break;
                default:
                    $key = 'unknown';
                    break;
            }
            $output[$key] = str_replace('*', '*', $piece);
            $i++;
        }
        if ($return !== NULL && array_key_exists($return, $output)) {
            return $output[$return];
        }
        return $output;
    }


    /**
     * Returns the data of all the tasks that are duplicates of a parent.
     * Duplicate tasks are created when a task is set as a recurring task
     * The function doesn't return the original task!
     *
     * @param int $duplicate_id Parent task ID
     * @param bool $only_ids When FALSE, returns only task_id's
     * @return array An array of task_id's
     */
    public function get_recurring($duplicate_id, $only_ids = FALSE)
    {

        if ($only_ids) {
            $q = DB::select('id');
        } else {
            $q = DB::select()
                    ->as_object();
        }

        $q = $q->from('tasks')
                ->where('deleted', '=', '0')
                ->where('duplicate_of', '=', $duplicate_id)
                ->where('id', '!=', $duplicate_id)
                ->execute();

        if ($only_ids) {
            return $q->as_array(NULL, 'id');
        }
        return $q;
    }


    /**
     * Handles the creation, deletion and updating of duplicate tasks based on it's repeat schedule
     *
     * @param $post Validated and filtered $_POST data from Model_Task update/add functions
     * @return bool|int True on success, int error code on failure
     */
    public function handle_repeating_tasks($post)
    {


        $old_data = $this->get($post['id']);
        $max = Kohana::$config->load('app.max_duplicate_count');
        // New recurring task added or modified an old task and set it to recurring
        if ((empty($old_data)
                || $post['repeating'] != $old_data->repeating
                || $post['repeat_until'] != $old_data->repeat_until
                || $post['repeat_expression'] != $old_data->repeat_expression)
                && $post['repeating']
        ) {


            // Schedule is an array of dates, each element representing a task
            // that should be present and a duplicate of $post['task_id']
            // All of those must exist at the end of the function
            try {
                $schedule = $this->calc_range($post['started'], $post['repeat_until'], $post['repeat_expression']);
            } catch (Exception $e) {
                return 101; // No next occurrence date found within range (or cron lib fails due to a bug with certain dates)
            }

            if (count($schedule) > $max) {
                return 102; // Exceeded max duplication count
            }

            // These task are already present and duplicates of the modified task
            $duplicates = $this->get_recurring($post['id']);

            // These already exist, possibly need deletion
            $duplicate_ids = $this->extract_ids($duplicates);


            // Loop over all existing duplicates
            if (!empty($duplicates)) {
                foreach ($duplicates as $duplicate) {

                    // Remove each existing duplicate from the schedule and duplicates list
                    // This is done so that when the user changes the schedule (end date or the frequency),
                    // tasks get removed or added as needed
                    $date = Helper_Template::mysql_date($duplicate->started);

                    if (in_array($date, $schedule)) {

                        // Delete from schedule so we'd know which tasks need creation
                        unset($schedule[array_search($date, $schedule)]);

                        // Delete from existing so we'd know which tasks need deletion
                        unset($duplicate_ids[array_search($duplicate->id, $duplicate_ids)]);
                    }
                }
            }


            // Add missing tasks (either the repeat_until widened or the frequency condensed)
            if (!empty($schedule)) {
                foreach ($schedule as $started) {

                    // New values for the new duplicate, adjust start/end times per schedule
                    $started = Helper_Template::mysql_date($started . ' ' . date('H:i:s', strtotime($post['started'])), TRUE);

                    // Finished might be unset
                    $finished = strtotime($post['finished']);
                    if ($finished > 100) {
                        $time_diff = $finished - strtotime($post['started']);
                    } else {
                        $time_diff = 0;
                    }

                    $finished = Helper_Template::mysql_date(strtotime($started) + $time_diff, TRUE);

                    $this->duplicate($post['id'], array('started' => $started,
                        'finished' => $finished,
                    ));
                }
            }

            // Delete tasks that managed to get themselves left outside the schedule
            if (!empty($duplicate_ids)) {
                foreach ($duplicate_ids as $duplicate_id) {
                    $this->delete('tasks', $duplicate_id);
                }
            }


            // Removed repeating feature from the task
        } elseif ($post['repeating'] == 0 && isset($old_data->repeating) && $old_data->repeating == 1) {
            $duplicates = $this->get_recurring($post['id']);
            if (!empty($duplicates)) {
                foreach ($duplicates as $duplicate) {
                    $this->delete('tasks', $duplicate->id);
                }
            }
        }

        // Updade duplicate's data when the original changes
        $this->update_duplicates($post);

        return TRUE;
    }


}