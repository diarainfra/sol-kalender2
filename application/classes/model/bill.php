<?php defined('SYSPATH') or die('No direct script access.');

class Model_Bill extends Model_Main
{

    /**
     * Called when a bill is confirmed.
     * Sets the billed status to 1 for all tasks that belonged to the bill
     * @param array $task_ids
     * @return bool
     */
    public function confirm_bill($task_ids)
    {
        if (empty($task_ids)) {
            return FALSE;
        }

        DB::update('tasks')
                ->set(array('billed' => 1))
                ->where('id', 'IN', $task_ids)
                ->execute();
        Syslog::write(__('Arve kinnitatud. Arves sisalduvad tööd: :ids', array(
            ':ids' => trim(implode(', ', $task_ids), ', ')
        )));
        return TRUE;
    }


    /**
     * Send bill to e-mail as PDF attachment
     * @param $info $_POST array from reports/email_form.php
     * @return bool
     */
    public function send($info)
    {
        $post = Validation::factory($info)
                ->rule('to', 'not_empty')
                ->rule('to', 'min_length', array(':value', '2'))
                ->rule('subject', 'min_length', array(':value', '2'))
                ->rule('body', 'max_length', array(':value', '1000'));

        // Is valid?
        if ($post->check()) {


            // E-mail the PDF as an attachment
            include(APPPATH . 'libraries/EmailAttachment.php');

            if (!empty($post['attachment']) && !strstr($post['attachment'], '..')) {

                $attachment_path = DOCROOT . 'assets/pdf/' . $post['attachment'];
                if (!file_exists($attachment_path)) {
                    return FALSE;
                }
            }
            $mailer = new EmailAttachment($post['to'], $post['subject'], $post['body'], $attachment_path);
            return $mailer->mail();
        }
        return FALSE;
    }


}