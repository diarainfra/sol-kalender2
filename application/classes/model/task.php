<?php defined('SYSPATH') or die('No direct script access.');

class Model_Task extends Model_Main
{


    /**
     * Rules for task fields
     * @param $data
     * @param bool $add Rules for adding / updating are different
     * @return Kohana_Validation
     */
    private function rules($data, $add = TRUE)
    {

        $trimmer = function ($input) {
            if (!is_array($input)) return trim($input); else return $input;
        };
        $data = array_map($trimmer, $data);
        $post = Validation::factory($data);

        if ($add) {
            // $post->rule(TRUE, 'not_empty'); // @todo Figure out how to get around this (the rule doesn't allow finished to be empty)
        } else {
            $post->rule('car_mileage', 'Helper_Template::optional_field', array(':value'))
                ->rule('car_billing', 'digit')
                ->rule('car_time', 'not_empty')
                ->rule('id', 'Model_Task::exists', array(':value'));
        }


        $post->rule('started', 'date', array(':value'))
            ->rule('finished', 'Helper_Template::optional_field', array(':value'))
            ->rule('description', 'min_length', array(':field', '1'))
            ->rules('employees', array(array('min_length', array(':value', '1')),
                array('count', array(':value'))))
            ->rules('object', array(
                array('Model_Task::object_exists', array(':value')),
                array('digit'),
                array('not_empty')
            ))
            ->rule('service', 'digit')
            ->rule('payment', 'digit');

        if (!empty($data['finished'])) {
            $post->rule('finished', 'date', array(':value'))
                ->bind(':finished', $data['finished'])
                ->rule('started', 'Model_Task::date_smaller_than', array(':value', ':finished'));
        }


        if (isset($data['repeating'])) {
            $post->rule('repeat_until', 'not_empty')
                ->bind(':started', $data['started'])
                ->rule('repeat_until', 'date', array(':value'))
                ->rule('repeat_until', 'Model_Task::date_smaller_than', array(':started', ':value'))
                ->rule('repeating', 'digit');
        }

        $post->check();

        return $post;
    }


    /**
     * Checks if the input is an array or a digit, including 0.
     * Used in Validation rules
     * @static
     * @param $input
     * @return bool
     */
    public static function digit_or_array($input)
    {

        if ($input >= 0 || is_array($input)) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     * Applies needed input data transformations
     * to update and add methods
     * @param $post Array of values
     * @return array
     */
    private function process_post($post)
    {
        Arr::check_keys($post, array('repeat_day', 'repeat_month', 'repeat_day_of_week'));

        $post['started'] = Helper_Template::mysql_date($post['started'], TRUE);
        if (!empty($post['finished'])) {
            $post['finished'] = Helper_Template::mysql_date($post['finished'], TRUE);
        } else {
            $post['finished'] = NULL;
        }

        $post['employees'] = array_unique(explode('+', $post['employees']));

        $post['unit_price'] = Helper_Template::mysql_float($post['unit_price']);


        if (!empty($post['car_mileage'])) {
            $post['car_mileage'] = Helper_Template::mysql_float($post['car_mileage']);
        }
        if (empty($post['car_billing'])) {
            $post['car_billing'] = 0;
        }

        if (isset($post['car_time'])) {
            $post['car_time'] = Helper_Template::pretty_to_sec($post['car_time']);
        } else {
            $post['car_time'] = 0;
        }

        $post['payment'] = (isset($post['payment']) && $post['payment'] > 0) ? 1 : 0;
        $post['repeating'] = (isset($post['repeating']) && $post['repeating'] > 0) ? 1 : 0;


        // Repeat expression
        if (($post['repeat_day'] != 0 && empty($post['repeat_day']))
            || (is_array($post['repeat_day']) && in_array('*', $post['repeat_day']))
        ) {
            $post['repeat_day'] = '*';
        }
        if (($post['repeat_month'] != 0 && empty($post['repeat_month'])) || (
                is_array($post['repeat_month']) && in_array('*', $post['repeat_month']))
        ) {
            $post['repeat_month'] = '*';
        }
        if (($post['repeat_day_of_week'] != 0 && empty($post['repeat_day_of_week']))
            || (is_array($post['repeat_day_of_week']) && in_array('*', $post['repeat_day_of_week']))
        ) {
            $post['repeat_day_of_week'] = '*';
        }

        $post['repeat_until'] = (empty($post['repeat_until']) || !$post['repeating']) ? NULL
            : Helper_Template::mysql_date($post['repeat_until']);

        $post['repeat_expression'] = ($post['repeating'])
            ? Model_Recurring::cron_encode($post['repeat_day'], $post['repeat_month'], $post['repeat_day_of_week'])
            : NULL;


        $post['time_estimate'] = Helper_Template::pretty_to_sec($post['time_estimate']);
        $post['time_elapsed'] = Helper_Template::pretty_to_sec($post['time_elapsed']);


        return $post;
    }


    /**
     * Add a new task to the DB
     * @param  $data - assoc array with data to be inserted, usually from $_POST
     * Returns an array of Validation errors on failure
     * set or int insert_id on success.
     * @return array|int Validation success or failure, task_id
     */
    public function add($data)
    {

        // Validate the input ($_POST)
        $post = $this->rules($data);

        if ($post->check()) {

            $emp = explode('+', $post['employees']);

            if (is_array($emp) && count($emp) > 0) {

                $post = $this->process_post($post->data());

                // Auto calc time elapsed
                if (empty($post['time_elapsed']) && !empty($post['finished'])) {
                    $post['time_elapsed'] = Helper_Template::time_diff($post['started'], $post['finished'], FALSE);
                }

                $query = DB::insert('tasks', array('started',
                    'finished',
                    'object',
                    'description',
                    'payment',
                    'unit_price',
                    'service',
                    'time_estimate',
                    'time_elapsed',
                    'internal_bill',
                    'repeating',
                    'repeat_until',
                    'repeat_expression'))
                    ->values(array($post['started'],
                        $post['finished'],
                        $post['object'],
                        $post['description'],
                        $post['payment'],
                        $post['unit_price'],
                        $post['service'],
                        $post['time_estimate'],
                        $post['time_elapsed'],
                        (bool)@$post['internal_bill'],
                        $post['repeating'],
                        $post['repeat_until'],
                        $post['repeat_expression']
                    ));

                $insert_id = $query->execute();
                $insert_id = $insert_id[0];

                Syslog::write(__('Kirje loomine'), array('task' => $insert_id));

                $this->assign_employees($insert_id, $post['employees']);

                // Add or remove new copies of the same task when it's recurring
                //$post['id'] = $insert_id;
                //Model::factory('Recurring')->handle_repeating_tasks($post); // @todo Not implemented yet

                return $insert_id;
            }
        }
        return $post->errors('task');
    }


    /**
     * Update task data
     *
     * Returns an array of Validation errors on failure
     *
     * @param $data $_POST data
     * @return mixed|int Either the task ID or Validation::errors object
     */
    public function update($data)
    {

        $post = $this->rules($data, FALSE);

        if ($post->check()) {
            $old = $this->get($post['id']);

            $post = $this->process_post($post->data());

            // Finish time changed, recalculate time elapsed

            if ((empty($post['time_elapsed']) || $post['time_elapsed'] == '0:00') && strtotime($old->finished) != strtotime($post['finished'])) {
                $post['time_elapsed'] = Helper_Template::time_diff($post['started'], $post['finished']);
            }

            // Assign emp must be above handle_repeating
            $this->assign_employees($post['id'], $post['employees']);

            // Add or remove new copies of the same task when it's recurring
            $result = Model::factory('Recurring')->handle_repeating_tasks($post);
            if ($result !== TRUE) {
                return $result;
            }


            $this->_update($post);
            return $post['id'];

        }
        return $post->errors('task');
    }


    /**
     * Does the actual update to a task.
     * @param array $post Array of data
     * @param bool $duplicates If true, updates duplicate tasks instead
     * @return void
     */
    private function _update($post, $duplicates = FALSE)
    {

        if (empty($post['time_elapsed']) && !empty($post['finished'])) {
            $post['time_elapsed'] = $post['finished'] - $post['started'];
        }

        $data = array(
            'object' => $post['object'],
            'description' => $post['description'],
            'payment' => $post['payment'],
            'unit_price' => $post['unit_price'],
            'service' => $post['service'],
            'time_estimate' => $post['time_estimate'],
            'time_elapsed' => $post['time_elapsed'],
            'internal_bill' => (bool)@$post['internal_bill'],
            'car_mileage' => $post['car_mileage'],
            'car_billing' => (bool)$post['car_billing'],
            'car_time' => $post['car_time'],
            'repeating' => $post['repeating'],
            'repeat_until' => $post['repeat_until'],
            'repeat_expression' => $post['repeat_expression']
        );

        if (!$duplicates) {
            $data['started'] = $post['started'];
            $data['finished'] = $post['finished'];
        }

        if (empty($post['finished'])) {
            $data['finished'] = NULL;
        }

        $q = DB::update('tasks')
            ->set($data);


        if ($duplicates) {
            $q->where('duplicate_of', '=', $post['id']);
        } else {
            $q->where('id', '=', $post['id']);
        }
        Syslog::write(__('Töö #:id andmed muudetud', array(':id' => $post['id'])), array('task' => $post['id']));
        $q->execute();
    }


    /**
     * Updates all task duplicates with $post data.
     * @param array $post Already filtered and validated $_POST data
     * @return bool
     */
    public function update_duplicates($post)
    {
        $task_data = $this->get($post['id']);
        $this->_update($post, TRUE);
        return TRUE;
    }


    /**
     * Assign a set of employees to the task.
     * The set is complete, meaning employees not included, but added before will be deleted
     * @param int $task_id
     * @param array $employees an array of employee id-s
     * @return void
     */
    public function assign_employees($task_id, $employees)
    {

        $e = New Model_Employee();

        // Associate employees with a task
        $e->cleanup_task($task_id);
        foreach ($employees as $employee) {
            if ($e->exists($employee)) {
                $query = DB::insert('employee_tasks', array('employee_id', 'task_id'))
                    ->values(array($employee, $task_id));
                $query->execute();
            }
        }
    }


    /**
     * Add new material entry to a task
     *
     * Ex: Used 4 bolts for task XXX
     *
     * @param  $data array of data
     * @return bool|int Material_entry ID if sucessful
     */
    public function add_materials($data)
    {

        if (!isset($data['name'])
            || !isset($data['quantity'])
            || !isset($data['unit'])
            || !isset($data['unit_price'])
            || !isset($data['task_id'])
            || $data['quantity'] < 0
            || $data['unit_price'] < 0
            || !$this->exists($data['task_id'])
        ) {
            return FALSE;
        }

        $data['unit_price'] = (float)str_replace(',', '.', $data['unit_price']);
        $data['quantity'] = (float)str_replace(',', '.', $data['quantity']);

        $post = Validation::factory($data)
            ->rule(TRUE, 'not_empty')
            ->rule('name', 'max_length', array(':field', '255'));

        if ($post->check()) {

            $query = DB::insert('tasks_materials', array('name',
                'task_id',
                'quantity',
                'unit',
                'unit_price'))
                ->values(array($post['name'],
                    $post['task_id'],
                    $post['quantity'],
                    $post['unit'],
                    $post['unit_price']));

            $result = $query->execute();
            Syslog::write(__('Materjali #:id lisamine', array(':id' => $result[0])), array('task' => $post['task_id']));
            return $result[0];
        }
        return FALSE;
    }

    public function edit_materials($data)
    {

        $id = $_POST['material_id'];
        $name = $data['name'];
        $unit = $data['unit'];
        $unit_price = $data['unit_price'];
        $quantity = $data['quantity'];
        $update_data = array('name' => $name, 'quantity' => $quantity, 'unit' => $unit, 'unit_price' => $unit_price);

        $update_data['unit_price'] = (float)str_replace(',', '.', $data['unit_price']);
        $update_data['quantity'] = (float)str_replace(',', '.', $data['quantity']);

        $post = Validation::factory($update_data)
            ->rule(TRUE, 'not_empty')
            ->rule('name', 'max_length', array(':field', '255'));

        if ($post->check()) {
            $name = $post['name'];
            $qt = $post['quantity'];
            $unit = $post['unit'];
            $u_p = $post['unit_price'];
            $u_data = array('name' => $name, 'quantity' => $qt, 'unit' => $unit, 'unit_price' => $u_p);
            $query = DB::update('tasks_materials')
                ->set($u_data)
                ->where('id', '=', $id);

            $result = $query->execute();
            Syslog::write(__('Materjali #:id muutmine', array(':id' => $result[0])), array('task' => $id));
            return $result[0];
        }
        return FALSE;
    }


    /**
     * Get a list of materials used in a task
     *
     * @static
     * @since 1.0.0
     * @param int $task_id
     * @return object Database result object
     */
    public static function used_materials($task_id)
    {

        if (!Model_Task::exists($task_id)) {
            return FALSE;
        }

        $q = DB::select('tasks_materials.*',
            array('tasks.started', 'date'),
            array('tasks.internal_bill', 'internal_bill'),
            array('tasks.billed', 'billed'))
            ->from('tasks_materials')
            ->where('tasks_materials.task_id', '=', $task_id)
            ->where('tasks_materials.deleted', '=', 0)
            ->join('tasks', 'left')->on('tasks.id', '=', 'tasks_materials.task_id')
            ->as_object()
            ->execute();
        return $q;
    }

    public static function all_materials($task_id)
    {


        $q = $materials = DB::select()
            ->from('tasks_materials')
            ->where('task_id', '=', $task_id)
            ->where('deleted', '=', 0)
            ->execute();
        return $q;
    }


    /**
     * Returns the sum (cost) of all used materials for the given tasks
     * @param mixed $task_ids Either an array or a int of task_id's
     * @param bool $extra Whether to include extra_price (lepinguline juurdehindlus)
     * @return bool|int The sum of used materials
     */
    public function used_materials_sum($task_ids, $extra = FALSE)
    {
        // Require $task_ids to be an array
        if ((!is_array($task_ids) && !is_numeric($task_ids)) || $task_ids < 1 || empty($task_ids)) {
            return 0;
        }

        if (!is_array($task_ids)) {
            $task_ids = array(0 => $task_ids);
        }

        // Select and calculate the sum of specified task_id's materials
        $sum = DB::select(array('ROUND( SUM("quantity"*"unit_price"), 2)', 'sum'))
            ->from('tasks_materials')
            ->where('task_id', 'IN', $task_ids)
            ->where('deleted', '=', 0)
            ->execute()
            ->get('sum');

        if ($extra && (int)$sum > 0) {
            $s = New Model_Settings();
            $extra_cost = $s->get('extra_price');
            return $sum + round($sum * $extra_cost, 2);
        }


        if (empty($sum)) {
            return 0;
        }

        return $sum;
    }


    /**
     * Get data about a task
     * @param null $tid
     * @return Database_Result|object
     */
    public function get($tid = NULL)
    {
        return $this->get_data('tasks', $tid);
    }


    /**
     * Check if date1 is smaller than @param date2
     * @param $date1
     * @param $date2
     * @return bool
     */
    public static function date_smaller_than($date1, $date2)
    {
        return (strtotime($date1) < strtotime($date2));
    }


    /**
     * Stand-in function for ->add validation
     * @static
     * @param  $id
     * @return bool
     */
    public static function object_exists($id)
    {
        $o = New Model_Object();
        return $o->exists($id);
    }


    /**
     * @param $q ORM object to apply the where clauses to.
     * Modifies the original $q object (pass-by-reference)
     * @param array $filters Assoc array of filters
     *
     * $filters keys:
     *
     * string $start date Required
     * string $end date Required
     * int $object Wether to restrict the results to a particular object
     * int $billed If 0 or 1, filter by billed
     * int type If set, limit by contract type (0: in contract, 1: outside contract)
     * int $object Object ID
     * int $employee Employee ID
     * int $manager Employee ID (Managers are employees WITH code)
     * int $client Client ID
     * int $area Area ID
     * @return void
     */
    public function add_task_filters(&$q, $filters)
    {
        $allowed_filters = array('payment', 'billed', 'object', 'client', 'area', 'employee', 'internal_bill', 'start', 'end');
        $not_null = array('area', 'client', 'task_id', 'object');

        if (isset($filters['task_id']) && $filters['task_id'] > 0) {
            $q->where('tasks.id', '=', $filters['task_id']);
        } else {

            // Apply all specified and allowed filters
            foreach ($filters as $key => $value) {

                // Skip invalid filters
                if (!in_array($key, $allowed_filters)
                    || ((string)$value == '0' && in_array($key, $not_null))
                    || (empty($value) && $value != '0')
                ) {

                    unset($filters[$key]);
                    continue;
                }

                // Apply filters
                switch ($key) {
                    case 'start':
                        $q->where('started', '>=', date('Y-m-d H:i:s', strtotime($value)));
                        break;

                    case 'end':
                        // Set the time 23:59:59
                        $date = date('Y-m-d H:i:s', strtotime($value) + 86399);
                        $q->where('finished', '<=', $date);
                        break;

                    case 'employee':
                        $q->join('employee_tasks', 'left')->on('employee_tasks.task_id', '=', 'tasks.id')
                            ->where('employee_tasks.employee_id', '=', (int)$value);
                        break;

                    case 'client':
                        $q->where('objects.client_id', '=', (int)$value);
                        break;

                    case 'area':
                        $q->where('objects.area', '=', (int)$value);

                        break;
                    case 'internal_bill':
                        $q->where('tasks.internal_bill', '=', (bool)$value);
                        break;

                    default:
                        $q->where($key, '=', (int)$value);
                        break;
                }
            }
        }
        $q->where('tasks.deleted', '=', 0);
    }


    /**
     * Returns an array of task id's that fall within a given range
     *
     * @param array $filters An array of filters to limit the search.
     * @see Model_Task::add_task_filters()
     * @return array|null
     */
    public function tasks_in_range($filters)
    {

        $q = DB::select('tasks.*', array('objects.name', 'object_name'))
            ->from('tasks')
            ->join('objects', 'left')->on('tasks.object', '=', 'objects.id'); // @todo Join only needed for filters

        $this->add_task_filters($q, $filters);

        return $q->as_object()
            ->execute()
            ->as_array(NULL, 'id');
    }


    /**
     * Returns tasks between $start and $end
     *
     * @param int $start A start UNIX timestamp
     * @param int $end A end UNIX timestamp
     * @param array $filters Extra filters such as area or object
     * @return ORM $q
     */
    public function range($start, $end, $filters = NULL)
    {

        if (!is_int($start)) {
            $start = strtotime($start);
        }
        if (!is_int($end)) {
            $end = strtotime($end);
        }

        $q = DB::select(array('objects.id', 'object_id'), 'tasks.*')
            ->from('tasks')
            ->join('objects')->on('tasks.object', '=', 'objects.id')
            ->as_object()
            ->where('tasks.started', '>=', date('Y-m-d', $start))
            ->where('tasks.deleted', '=', '0')
            ->and_where_open()
            ->where('tasks.finished', '<=', date('Y-m-d', $end))
            ->or_where('tasks.finished', 'IS', DB::expr('NULL'))
            ->and_where_close();


        // Return a single task
        if (!empty($filters['task_id']) && $this->exists($filters['task_id'])) {
            $q->where('tasks.id', '=', $filters['task_id']);
        }

        // Apply extra filters
        if (empty($filters['area']) && !empty($filters['object'])) {
            $q->where('tasks.object', '=', (int)$filters['object']);

        } elseif (!empty($filters['area'])) {


            $task_ids = array();

            $objects = DB::select()
                ->from('objects')
                ->as_object()
                ->where('area', '=', $filters['area'])
                ->execute();

            if ($objects->count() > 0) {
                foreach ($objects as $object) {
                    $tasks = DB::select('id')
                        ->from('tasks')->
                        as_object()
                        ->where('object', '=', $object->id)
                        ->execute();

                    if ($tasks->count() > 0) {
                        foreach ($tasks as $task) {
                            $task_ids[] = $task->id;
                        }
                    }
                }
            }
            if (count($task_ids) > 0) {
                $q->where('tasks.id', 'IN', $task_ids);
            }
        }


        $q = $q->execute();

        if ($q->count() == 0) {
            return NULL;
        }
        return $q;
    }


    /**
     * Return employees associated with a particular task
     * @param  $task_id
     * @return Database_Result|null|object
     */
    public function assoc_employees($task_id)
    {
        $q = DB::select('employee_id')
            ->from('employee_tasks')
            ->where('task_id', '=', $task_id)
            ->execute();
        if ($q->count() == 0) {
            return NULL;
        }

        $ids = array();
        foreach ($q->as_array() as $id) {
            $ids[] = $id['employee_id'];
        }

        foreach ($q as $employee_id) {
            $q2 = DB::select()
                ->from('employees')
                ->where('id', 'IN', $ids)
                ->as_object()
                ->execute();
        }

        if ($q->count() == 0) {
            return NULL;
        }

        return $q2;
    }


    /**
     * Check for the existence of the task
     * @static
     * @param int $tid Task ID
     * @return bool
     */
    public static function exists($tid)
    {
        if (is_numeric($tid)) {
            return (bool)DB::select('id')
                ->from('tasks')
                ->where('id', '=', $tid)
                ->execute()
                ->get('id');
        }
        return FALSE;
    }


    /**
     * Returns the comments added to a given task
     * @param $task_id
     * @return Database_Result
     */
    public function get_comments($task_id)
    {
        $c = ORM::factory('comment');
        $comments = $c->select('comment.*', array('users.name', 'name'))
            ->where('task_id', '=', $task_id)
            ->order_by('timestamp', 'desc')
            ->join('users', 'left')->on('users.id', '=', 'comment.user_id')
            ->find_all();
        return $comments;
    }

    public function get_comms($cid = NULL)
    {
        return $this->get_data('comms', $cid);

    }

    public function add_comm($post)
    {

        $c = ORM::factory('comm');
        $c->comment = $post['comment'];
        $c->date = $post['kuupaev'];
        $c->save();
        return $c->saved();

    }

    /**
     * Add a new comment
     * @param array $post
     * @return bool
     */
    public function add_comment($post)
    {
        $a = Auth::instance();
        $c = ORM::factory('comment');
        $c->user_id = $a->get_user()->id;
        $c->task_id = $post['task_id'];
        $c->comment = $post['comment'];
        $c->save();
        Syslog::write(__('Kommentaari #:id lisamine', array(':id' => $c->id)), array('task' => $post['task_id']));
        return $c->saved();
    }


    /**
     * Calc the cost of the number of km driven
     * @static
     * @param float $km
     * @return float The price fo $km
     */
    public static function car_mileage($km)
    {
        $s = New Model_Settings();
        $km_price = $s->get('km_price');
        return round((float)$km * (float)$km_price, 2);
    }


    /**
     * Calculates the sums of car expenses for the given tasks
     *
     * @param ORM $tasks Task data from Model_Task::range()
     * @param null|bool $billing NULL: Get total sum of ALL car expenses;
     * TRUE: Only sum those marked for billing
     * FALSE: Only sum those marked to be left out from the bill
     * @return array Assoc array of car sums: distance, count, price, time
     */
    public function car_sums($tasks, $billing = NULL)
    {
        $sums = array('distance' => 0, 'count' => 0, 'price' => 0, 'time' => 0);
        if (!empty($tasks)) {

            // Sum all task car expenses (only if car_billing matches $billing)
            foreach ($tasks as $task) {
                if ($task->car_mileage > 0
                    && ($billing === NULL || $billing == (bool)$task->car_billing)
                ) {
                    $sums['price'] += Model_Task::car_mileage($task->car_mileage);
                    $sums['count']++;
                    $sums['time'] += $task->car_time;
                    $sums['distance'] += $task->car_mileage;
                }
            }
        }
        return $sums;
    }


    /**
     * Get data about tasks that need billing.
     * Used for generating a dropdown menu for selecting tasks.
     * @see Helper_Bills::list_unbilled_tasks()
     * @return Database_Result|object
     */
    public function get_unbilled()
    {
        return DB::select(array('objects.name', 'object_name'), 'tasks.*')
            ->from('tasks')
            ->join('objects', 'left')->on('objects.id', '=', 'tasks.object')
            ->where('billed', '=', '0')
            ->where('tasks.deleted', '=', 0)
            ->where('objects.deleted', '=', 0)
            ->as_object()
            ->execute();
    }


    /**
     * Duplicate a task row
     *
     * @param int $task_id
     * @param array|null $overrides An optional assoc array
     * that overrides some fields of the new row
     * @return bool|int New task ID
     */
    public function duplicate($task_id, $overrides = NULL)
    {

        // Get original's data
        $data = DB::select()
            ->from('tasks')
            ->where('id', '=', $task_id)
            ->execute()
            ->current();

        if (empty($data)) {
            return FALSE;
        }
        unset($data['id']);
        $data['duplicate_of'] = $task_id;

        // Apply overrides
        if (is_array($overrides)) {
            foreach ($overrides as $key => $value) {
                if (array_key_exists($key, $data)) {
                    $data[$key] = $value;
                }
            }
        }

        // Insert a duplicate row
        $insert = DB::insert('tasks', array_keys($data))
            ->values($data)
            ->execute();

        if (!isset($insert[0]) || $insert[0] < 1) {
            return FALSE;
        }
        Syslog::write(__('Duplikaattöö #:id lisamine', array(':id' => $insert[0])), array('task' => $insert[0]));

        $employees = $this->assoc_employees($task_id);
        if (!empty($employees)) {
            $this->assign_employees($insert[0], $employees->as_array(NULL, 'id'));
        }

        return $insert[0];
    }


    public function delete($what, $id)
    {
        $data = $this->get($id);

        // Don't allow the deletion of billed tasks
        // with the exception of duplicates
        if (empty($data) || ($data->billed && empty($data->duplicate_of))) {
            return FALSE;
        }

        Syslog::write(__('Kustutatud'), array('task' => $id));

        return parent::delete($what, $id);
    }


}