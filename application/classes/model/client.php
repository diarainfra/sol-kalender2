<?php defined('SYSPATH') or die('No direct script access.');

class Model_Client extends Model_Main
{

    /**
     * Get client's data
     * @param int $cid When specified, return specific client's data
     * @return Database_Result|object
     */
    public function get($cid = NULL) {
        return $this->get_data('clients', $cid);
    }


    /**
     * Applies validation rules to the add|update function
     * @param object $q Validation object
     * @return object Validation object with the rules
     */
    private function rules($q) {
        return $q->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':value', '80'))
                ->rule('name', 'min_length', array(':value', '2'))

                ->rule('code', 'not_empty')
                ->rule('code', 'max_length', array(':value', '10'))
                ->rule('code', 'min_length', array(':value', '1'))

        // Optional fields, workaround rules so that Validation:: wouldn't erase the $_POST data
                ->rule('postal_address', 'Helper_Template::optional_field', array(':value'))
                ->rule('phone', 'Helper_Template::optional_field', array(':value'))
                ->rule('email', 'Helper_Template::optional_field', array(':value'))
                ->rule('birthday', 'Helper_Template::optional_field', array(':value'))
                ->rule('contract_start', 'Helper_Template::optional_field', array(':value'))
                ->rule('contract_end', 'Helper_Template::optional_field', array(':value'))
                ->rule('extra', 'Helper_Template::optional_field', array(':value'));
    }


    /**
     * Process and reformat the $_POST data of add|update functions
     * @param array $post
     * @return void
     */
    private function process_post($post) {

        // Set the date to NULL if it's not filled (as opposed to EPOCH (1970)
        foreach (array('birthday', 'contract_start', 'contract_end') as $key) {
            $post[$key] = Helper_Task::date_set($post[$key]) ? Helper_Template::mysql_date($post[$key])
                    : NULL;
        }
        return $post;
    }


    /**
     * Add a new client
     * @param array $data $_POST data
     * @return bool|int
     */
    public function add($data) {

        $q = Validation::factory($data);

        $post = $this->rules($q);

        if ($post->check()) {

            $post = $this->process_post($post->data());

            $query = DB::insert('clients', array('name',
                                                'code',
                                                'postal_address',
                                                'phone',
                                                'email',
                                                'birthday',
                                                'contract_start',
                                                'contract_end',
                                                'extra'
                                           ))
                    ->values(array($post['name'],
                                  $post['code'],
                                  $post['postal_address'],
                                  $post['phone'],
                                  $post['email'],
                                  $post['birthday'],
                                  $post['contract_start'],
                                  $post['contract_end'],
                                  $post['extra']
                             ));

            $query = $query->execute();
            Syslog::write(__('Klient #:id lisatud', array(':id' => $query[0])));
            return $query[0];
        }

        return FALSE;
    }


    /**
     * Update client data
     * @param array $data
     * @return bool
     */
    public function update($data) {

        $q = Validation::factory($data);

        $post = $this->rules($q);

        if ($post->check()) {
            $post = $this->process_post($post->data());
            $query = DB::update('clients')
                    ->set(array('name' => $post['name'],
                               'code' => $post['code'],
                               'postal_address' => $post['postal_address'],
                               'phone' => $post['phone'],
                               'email' => $post['email'],
                               'birthday' => $post['birthday'],
                               'contract_start' => $post['contract_start'],
                               'contract_end' => $post['contract_end'],
                               'extra' => $post['extra']
                          ))
                    ->where('id', '=', $post['id']);
            $query->execute();
            Syslog::write(__('Kliendi #:id andmeid muudeti', array(':id' => $post['id'])));
            return TRUE;
        }

        return FALSE;
    }


    /**
     * Get all the objects belonging to a particular client
     * @param int $client_id If empty, return all objects!
     * @return Database_Result|object
     */
    public function objects($client_id) {
        $q = DB::select()
                ->from('objects');

        if ($client_id != 0) {
            $q->where('client_id', '=', $client_id);
        }

        return $q->where('deleted', '=', '0')
                ->as_object()
                ->execute();
        
    }

    /**
     * One client can have many contact persons.
     * This adds a new row to the client_contacts table
     * @param $client_id
     * @param $contact_id
     * @return int|bool $insert_id The index table's row ID
     */
    public function add_contact($client_id, $contact_id) {
        $c = ORM::factory('contact');
        return $c->bind('client', $client_id, $contact_id);
    }

    /**
     * Remove one contact from the client
     * @param $client_id
     * @param $contact_id
     * @return void
     */
    public function remove_contact($client_id, $contact_id) {
        $c = ORM::factory('contact');
        return $c->unbind('client', $client_id, $contact_id);
    }


    /**
     * Get a list of contact_id's
     * @param $client_id
     * @return array|bool
     */
    public function get_contacts($client_id) {
        $c = ORM::factory('contact');
        return $c->entity_contacts('client', $client_id);
    }
}