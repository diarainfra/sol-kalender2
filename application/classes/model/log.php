<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Logging class for ALL system logs
 *
 * Logs for specific entities (ie tasks) are bound via pivot-tables.
 *
 * @since 1.1.4
 * @package Logging
 */
class Model_Log extends Commoneer_ORM
{

    protected $_is_deletable = TRUE;

    protected $_has_deleted = TRUE;

    public function rules()
    {
        return array(
            'text' => array(
                array('not_empty')
            ),
            'user_id' => array(
                array('not_empty'),
                array('digit')
            )
        );
    }

    /**
     * Get logs based on filters
     *
     * @since 1.1.4
     * @param null $filters
     * @param bool $execute
     * @return mixed
     */
    public function get($filters = NULL, $execute = TRUE)
    {
        if (isset($filters['task_id'])) {
            $this->join('tasks_logs')->on('tasks_logs.log_id', '=', 'log.id')
                    ->where('tasks_logs.task_id', '=', $filters['task_id']);
        }
        return $this->find_all();
    }

    /**
     *
     * Find all logs
     *
     * @since 1.1.4
     * @return mixed
     */
    public function find_all()
    {
        $this->select(array('users.name', 'user_name'))
                ->join('users', 'left')->on('users.id', '=', 'log.user_id')
                ->order_by('id', 'DESC');
        return parent::find_all();
    }

    /**
     * Create or change a log
     *
     * @since 1.1.4
     * @param array $data
     * @return int|bool
     */
    public function change(array $data)
    {
        $keys = array('text', 'user_id');
        Arr::check_keys($data, $keys);

        if (empty($data['user_id'])) {
            $data['user_id'] = User::current()->id;
        }

        $this->user_id = $data['user_id'];
        $this->text = $data['text'];

        try {
            $this->save();

            // Bind with another entity, if needed
            $this->_add_binding($data);

            return $this->id;
        } catch (ORM_Validation_Exception $e) {
            Validation::show_errors($e);
        } catch (Database_Exception $e) {
        }
        return FALSE;
    }

    /**
     * Binds this log with another object: task, service, etc
     *
     * We'd normally do this with ORM, but since half of the project needs porting to ORM...
     *
     * @since 1.1.4
     * @param array $data entity_name_singular => entity_id
     */
    private function _add_binding(array $data)
    {
        if (!isset($data['binding']) || !$this->loaded() || count($data['binding']) !== 1) {
            return; // Nothing to do here
        }

        $entity_singular = array_keys($data['binding']);
        $entity_singular = $entity_singular[0];

        // Insert a new row to the pivot table, binding this log with another entity
        DB::insert(Inflector::plural($entity_singular) . "_logs", array($entity_singular . '_id', 'log_id'))
                ->values(array($data['binding'][$entity_singular], $this->id))
                ->execute();
    }
}