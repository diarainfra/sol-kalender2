<?php defined('SYSPATH') or die('No direct script access.');

class Model_Employee extends Model_Main
{


    /**
     * Returns data about a employee/employees
     * @param null $id If not set, return all
     * @return Database_Result|object
     */
    public function get($id = NULL)
    {
        $q = DB::select('employees.*', array('users.id', 'user_id'))
                ->from('employees')
                ->join('users','left')
                ->on('employees.id', '=', 'users.employee_id')
                ->where('deleted', '=', '0')
                ->order_by('name')
                ->as_object();

        if ($id !== NULL && is_numeric($id) && $id > 0) {
            return $q->where('employees.id', '=', (int)$id)->execute()->current();
        } elseif (is_array($id) && count($id) > 0) {
            return $q->where('employees.id', 'IN', $id)->execute();
        }

        return $q->execute();
    }


    /**
     * Returns task info for all employee's tasks
     * @param int $eid
     * @return ORM $q
     */
    public function get_tasks($eid)
    {
        $q = DB::select()
                ->from('tasks')
                ->join('employee_tasks')->on('tasks.id', '=', 'employee_tasks.task_id')
                ->as_object()
                ->where('employee_tasks.employee_id', '=', $eid)
                ->execute();

        return $q;
    }

    /**
     * Check if a employee exists
     * @param  $who
     * @return bool
     */
    public function exists($who)
    {
        if (is_numeric($who)) {
            $data = $this->get($who);
            return !empty($data);
        }
        return FALSE;
    }


    /**
     * Rules for the Employee object
     * @param $p
     * @param bool $edit
     * @return Kohana_Validation
     */
    protected function rules($p, $edit = FALSE)
    {
        $rules = Validation::factory($p)
                ->bind(':name', $p['name'])

                ->rule('name', 'not_empty')
                ->rule('name', 'max_length', array(':name', '50'))
                ->rule('email', 'email')
                ->rule('name', 'min_length', array(':name', '2'));

        if ($edit) {
            $rules->rule('employee_id', 'digit');
        }
        return $rules;
    }

    /**
     * Add a new employee
     *
     * @param array $post POST data
     * @return Database_Result|object|int Returns row ID or Validation::errors()
     */
    public function add($post)
    {

        $post = $this->rules($post);

        if ($post->check()) {

            $query = DB::insert('employees', array('name', 'code', 'email'))
                    ->values(array($post['name'], $post['code'], $post['email']));
            $result = $query->execute();

            if (is_numeric($result[0])) {

                // Create login user
                $pass = Auth::instance()->hash($post['pass']);

                Database::instance()->begin();
                $user = DB::insert('users', array('email', 'username', 'name', 'password', 'employee_id'))
                        ->values(array($post['email'], Inflector::camelize($post['name']), $post['name'], $pass, $result[0]))
                        ->execute();

                // Allow user to login
                DB::insert('roles_users', array('role_id', 'user_id'))
                        ->values(array(1, $user[0]))
                        ->execute();
                Syslog::write(__('Lisatud töötaja #:id', array(':id' => $result[0])));
                Database::instance()->commit();

                return $result[0];
            }

            return $result;
        }

        return $post->errors('validation');
    }


    /**
     * Edit an employee
     * @param $post_data
     * @return array|Database_Result|object
     */
    public function edit($post_data)
    {
        $post = $this->rules($post_data, TRUE);

        if ($post->check()) {
            $result = DB::update('employees')
                    ->set(array('name' => $post['name'],
                'code' => $post['code'],
                'email' => $post['email']
            ))
                    ->where('id', '=', (int)$post_data['employee_id'])
                    ->execute();

            $auth = Auth::instance();
            $user = $auth->get_user($post_data['employee_id']);

            if (
                !empty($post_data['pass'])
                && $user->loaded()
                && $auth->hash($post_data['pass']) != $user->password
            ) {
                DB::update('users')
                        ->set(array('password' => $auth->hash($post_data['pass']),
                    'email' => $post_data['email'],
                    'name' => $post_data['name']
                ))
                        ->where('employee_id', '=', $post_data['employee_id'])
                        ->execute();
                Notify::msg('Parool muudeti.');
            }

            if (is_numeric($result[0])) {
                Syslog::write(__('Töötaja #:id andmeid muudeti', array(':id' => $result[0])));
                return $result[0];
            }
            return $result;
        }
        return $post->errors('validation');
    }


    /**
     * Search for a employee by name
     * @param  $name
     * @return Database_Result|object
     */
    public function search($name)
    {
        $q = DB::select()
                ->from('employees')
                ->where('name', 'LIKE', "%$name%")
                ->where('deleted', '!=', 1)
                ->as_assoc()
                ->execute();

        if ($q->count() == 0) {
            return NULL;
        }
        $result = array();

        foreach ($q as $row) {
            $result[] = array('id' => $row['id'], 'name' => $row['name']);
        }
        return $result;
    }


    /**
     * Remove all employee bindings for the specified tasks
     * @param $task_id
     * @return Database_Result|object
     */
    public function cleanup_task($task_id)
    {
        return DB::delete('employee_tasks')
                ->where('task_id', '=', $task_id)
                ->execute();
    }


    /**
     * Get employee's user data
     * @param int $eid
     * @return Database_Result|object
     */
    public function get_user($eid)
    {
        return DB::select()
                ->from('users')
                ->where('employee_id', '=', (int)$eid)
                ->as_object()
                ->execute()
                ->current();
    }

}