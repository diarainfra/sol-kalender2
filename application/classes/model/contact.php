<?php defined('SYSPATH') or die('No direct script access.');

class Model_Contact extends ORM
{

    public function rules() {
        return array(
            'name' => array(
                array('not_empty'),
                array('min_length', array(':value', '2'))
            )
        );
    }


    /**
     * Make a new binding between entity and a contact
     * This method is accessed by a wrapper method in models Model_Client and Model_Object
     * @param string $entity One of contact|object
     * @param $entity_id
     * @param $contact_id
     * @return bool|int Insert id or FALSE
     */
    public function bind($entity, $entity_id, $contact_id) {
        if (empty($entity_id) || empty($contact_id) || !in_array($entity, array('client', 'object'))) {
            return FALSE;
        }

        // Check for duplicate entries
        $contacts = $this->entity_contacts($entity, $entity_id);
        if (is_array($contacts) && in_array($contact_id, $contacts)) {
            return FALSE;
        }


        $q = DB::insert($entity . '_contacts', array($entity . '_id', 'contact_id'))
                ->values(array((int)$entity_id, (int)$contact_id))
                ->execute();
        return $q[0];
    }


    /**
     * Opposite to Model_Contact::bind
     * Removes one contact from the entity.
     * This method is accessed by a wrapper method in models Model_Client and Model_Object
     * @param string $entity One of client|object
     * @param int $entity_id
     * @param int $contact_id
     * @return bool TRUE on success, FALSE on failure
     */
    public function unbind($entity, $entity_id, $contact_id) {
        if (empty($entity_id) || empty($contact_id) || !in_array($entity, array('client', 'object'))) {
            return FALSE;
        }

        $q = DB::delete($entity . '_contacts')
                ->where($entity . '_id', '=', (int)$entity_id)
                ->where('contact_id', '=', (int)$contact_id)
                ->execute();
        if ($q === 1) {
            return TRUE;
        }
        return FALSE;
    }


    /**
     * Returns an array of contact_id's that
     * are assigned to the specified entity.
     * This method is accessed by a wrapper method in models Model_Client and Model_Object
     * @param string $entity One of client|object
     * @param int $entity_id
     * @return NULL|bool|array An array of entity id-s
     */
    public function entity_contacts($entity, $entity_id) {
        if (!in_array($entity, array('client', 'object')) || empty($entity_id)) {
            return FALSE;
        }

        return DB::select('contact_id')
                ->from($entity . '_contacts')
                ->where($entity . '_id', '=', $entity_id)
                ->execute()
                ->as_array(NULL, 'contact_id');
    }
}