<?php defined('SYSPATH') or die('No direct script access.');

class Model_Report extends Model_Main
{

    private $CACHE_PATH = 'assets/cache/xls/'; // Stores generated .pdf .xlsx files

    public function before() {
        parent::before();
        $this->CACHE_PATH = DOCROOT . $this->CACHE_PATH;
    }


    /**
     * Returns data to generate a report for a given period
     *
     * @param array $filters Assoc array of filters to narrow down the results
     * @see Model_Task::add_task_filters()
     * @return object
     */
    public function range($filters) {
        $q = DB::select('tasks.*', array('objects.name', 'object_name'))
                ->from('tasks')
                ->as_object()
                ->join('objects', 'left')->on('tasks.object', '=', 'objects.id');

        $t = Model::factory('task');
        $t->add_task_filters($q, $filters);

        return $q->order_by('started', 'asc')
                ->execute();
    }


    /**
     * Sends a report as a e-mail.
     *
     * @param  $info array(start,end,client,to,body)
     * @return bool
     */
    public function send($info) {
        $post = Validation::factory($info)
                ->rule('to', 'not_empty')
                ->rule('to', 'min_length', array(':value', '2'))
                ->rule('subject', 'min_length', array(':value', '2'));

        // Is valid?
        if ($post->check()) {

            // Generate report content HTML

            // $view is the actual content
            $view = View::factory('reports/view');

            // Info needed by $view
            $o = New Model_Object();
            if (!empty($filters['object'])) {
                $object_data = $o->get($info['object']);
                $view->object = $object_data;
                $info['object_name'] = $object_data->name;
            }

            $t = New Model_Task();

            $view->filters = $info;
            $view->in_contract_data = $this->range(array_merge($info, array('payment' => 0)));
            $view->outside_contract_data = $this->range(array_merge($info, array('payment' => 1)));
            $view->tasks_in_range = $t->tasks_in_range($info); // For generating used materials

            $p = New Model_Pdf();
            $attachment_name = $p->generate_report_pdf($view, $info);


            // E-mail the PDF as an attachment
            include(APPPATH . 'libraries/EmailAttachment.php');

            $mailer = new EmailAttachment($post['to'], $post['subject'], $post['body'], $attachment_name);
            $mailer->mail();

            // Delete temp. pdf
            unlink($attachment_name);
            return TRUE;
        } else {
            return FALSE;
        }
    }


    /**
     * Generate a report in Excel format.
     * The file contains report data and will be cached and sent to the browser.
     * Each report is on a separate sheet.
     * @param  $object
     * @param  $start
     * @param  $end
     * @return string xls path
     */
    public function generate_xls($object, $start, $end) {

        /** PHPExcel */
        require(VENDORPATH . 'phpexcel/PHPExcel.php');

        /** PHPExcel_Writer_Excel2007 */
        require(VENDORPATH . 'phpexcel/PHPExcel/Writer/Excel2007.php');

        // Create new PHPExcel object
        $sheet = new PHPExcel();

        // Set properties
        $sheet->getProperties()->setCreator("Kalendrirakendus");
        $sheet->getProperties()->setLastModifiedBy("Kalendrirakendus");
        $sheet->getProperties()->setTitle(Helper_Reports::report_title(array('object' => $object, 'start' => $start, 'end' => $end)));
        $sheet->getProperties()->setSubject("Aruanne");
        $sheet->getProperties()->setDescription(Helper_Reports::report_title(array('object' => $object, 'start' => $start, 'end' => $end)));


        // Get tasks data
        $o = New Model_Object();
        $object_data = $o->get($object);


        /************* In-contract tasks *************/
        $sheet->setActiveSheetIndex(0);
        // Rename sheet
        $sheet->getActiveSheet()->setTitle('Lepingulised');

        $data = $this->get_tasks_array($object, $start, $end);

        // Write built array of task info to the worksheet
        foreach ($data as $row => $columns) {
            foreach ($columns as $column => $value) {
                $sheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
            }
        }


        /************* Outside-contract tasks *************/
        $sheet->createSheet();
        $sheet->setActiveSheetIndex(1);
        // Rename sheet
        $sheet->getActiveSheet()->setTitle('Väljaspool lepingut');

        $data = $this->get_tasks_array($object, $start, $end, 1);

        // Write built array of task info to the worksheet
        foreach ($data as $row => $columns) {
            foreach ($columns as $column => $value) {
                $sheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
                $sheet->getActiveSheet()->getColumnDimension('F')->setWidth(50);
                $sheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            }
        }


        /************* Used materials *************/
        $sheet->createSheet();
        $sheet->setActiveSheetIndex(2);
        // Rename sheet
        $sheet->getActiveSheet()->setTitle('Kasutatud materjalid');

        $t = New Model_Task();
        $data = $t->tasks_in_range(array('start' => $start, 'end' => $end, 'object' => $object));
        $data = $this->get_materials_array($data);

        // Write built array of materials info to the worksheet
        foreach ($data as $row => $columns) {
            foreach ($columns as $column => $value) {
                $sheet->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
            }
        }


        // Clear cache dir of old xls files (privacy)
        // Delete older than 5min
        if ($dir = opendir($this->CACHE_PATH)) {
            while (false !== ($file = readdir($dir))) {
                $file = $this->CACHE_PATH . $file;

                if (!is_dir($file) && (time() - filemtime($file)) > 300 && substr($file, -10) != 'index.html') {
                    unlink($file);
                }
            }
        }

        // Save Excel 2007 file
        $file = DOCROOT . 'assets/cache/xls/' . $this->sanitize_file_name($object_data->name) . '_' . time() . '.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($sheet);

        $objWriter->save(DOCROOT . $file);

        return $file;
    }


    /**
     * Generates an array containing the rows for used materials
     * table in XLS export.
     * @param  $task_ids
     * @return array|null
     */
    public function get_materials_array($task_ids) {

        if (empty($task_ids)) {
            return NULL;
        }

        // Sheet H1
        $data = array(0 => array(), 1 => array(), 2 => array('Hooldustöödel kasutatatud materjalid, seadmed ja allhanketööd'), 3 => array());

        // Column headers
        $headers = array('Töö nr.', 'Materjali nimetus', 'Kogus', 'Ühik', 'Ühiku hind', 'Kokku');

        $data[] = $headers;

        $sum = 0;

        foreach ($task_ids as $task_id) {
            $materials = Model_Task::used_materials($task_id);

            foreach ($materials as $row) {
                $total = round($row->quantity * $row->unit_price, 2);
                $data[] = array(
                    $row->task_id,
                    $row->name,
                    $row->quantity,
                    $row->unit,
                    $row->unit_price,
                    $total
                );
                $sum += $total;
            }
        }
        $sum = round($sum, 2);

        // Totals in table footer
        $data[] = array();
        $extra_cost = 0.05; // TEMP!
        $extra = round($sum / 100 * $extra_cost, 2);

        $data[] = array(NULL,
                        NULL,
                        'Lepingupõhine juurdehindlus',
                        $extra_cost,
                        '%',
                        $extra
        );

        $data[] = array(NULL, NULL, NULL, NULL, 'Kokku', ($extra + $sum) . '€');

        return $data;
    }


    /**
     * Assembles the array of data to be written into an Excel worksheet
     * Each Excel row is a value in a two-dimensional array
     * @param  $object Object id
     * @param  $start Date
     * @param  $end Date
     * @param int $type 0 - in contract, 1 - outside contract
     * @return array
     */
    public function get_tasks_array($object, $start, $end, $type = 0) {

        $o = New Model_Object();
        $object_data = $o->get($object);

        $tasks_data = $this->range(array('start' => $start, 'end' => $end, 'object' => $object, 'payment' => $type));

        // Write each task
        $i = 1;
        $data = array();
        $sums = array('time' => 0, 'sum' => 0);

        $data[] = array();
        $data[] = array();
        if ($type == 0) {
            $data[] = array('Lepingulised tööd');
        } else {
            $data[] = array('Lepinguvälised tööd');
        }
        $data[] = array();

        // Column headers
        $headers = array('Töö nr.', 'Kuup', 'Algus', 'Lõpp', 'Objekt', 'Kirjeldus', 'Kogus', 'Ühik');
        if ($type == 1) {
            $headers[] = 'Ühiku hind';
            $headers[] = 'Kokku';
        }
        $data[] = $headers;

        foreach ($tasks_data as $row) {

            $duration = strtotime($row->finished) - strtotime($row->started);
            $cols = array(
                $i,
                date('m', strtotime($row->started)),
                date('H:i', strtotime($row->started)),
                date('H:i', strtotime($row->finished)),
                $object_data->name,
                $row->description,
                Helper_Template::pretty_time($duration),
                'h'
            );
            if ($type == 1) {
                $cols[] = $row->unit_price;
                $cols[] = Helper_Template::calc_sum($duration, $row->unit_price);
                ;
            }

            // Sums
            $sums['time'] += $duration;
            $sums['sum'] += Helper_Template::calc_sum($duration, $row->unit_price);

            $data[] = $cols;
            $i++;
        }

        // Include the sum of hours / cost in table footer
        $data[] = array();
        $footer = array(NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        'Teostatud tööd kokku',
                        Helper_Template::pretty_time($sums['time']),
                        'h'
        );
        if ($type == 1) {
            $footer[] = NULL;
            $footer[] = $sums['sum'];
        }

        $data[] = $footer;

        return $data;
    }


}