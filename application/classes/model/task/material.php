<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Model for Task Material row
 *
 * @since 1.2.0
 * @author Ando Roots ando.roots@diara.ee
 * @package Task
 */
class Model_Task_Material extends Model_Main
{
    /**
     * Get task material row from the DB
     *
     * @since 1.2.0
     * @static
     * @param int $id Task material row ID
     * @return mixed
     */
    public static function get($id)
    {
        return Model::factory('task_material')
                ->get_data('tasks_materials', $id);
    }

    /**
     * Delete task material
     *
     * @since 1.2.0
     * @static
     * @param int $id Material row ID
     * @return bool|Database_Result|object
     */
    public static function delete_material($id)
    {
        // Need ID!
        if (empty($id)) {
            return FALSE;
        }

        $material_model = Model::factory('task_material');

        // Get material data
        $material = $material_model
                ->get($id);

        // Invalid ID
        if (!$material) {
            return FALSE;
        }

        // Get task data to which this material is bound
        $task = Model::factory('task')
                ->get($material->task_id);

        // If no task or the task is already billed...
        if (!$task || $task->billed == 1) {
            return FALSE;
        }

        // Write log
        Syslog::write(
            __('Kasutatud materjal #:id (:name) kustutatud', array(':id' => $id, ':name' => $material->name)),
            array('task' => $material->task_id)
        );

        return $material_model->delete('tasks_materials', $id);
    }

}