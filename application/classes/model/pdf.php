<?php defined('SYSPATH') or die('No direct script access.');

class Model_Pdf extends Model_Main
{


    /**
     * Generates a PDF file from a bill
     * @param string $bill_content The HTML of the bill
     * @param int $object_id Put bills into dir with the name of the object
     * @param string $prefix Prefix the generated PDF name, generally with a period (of a bill)
     * @return string Path to the generated bill
     */
    public function generate_bill_pdf($bill_content, $object_id, $prefix) {
        // Use a skeleton HTML template for encoding/compatibility
        $bill_html = View::factory('templates/pdf');
        $bill_html->content = (string)$bill_content;
        include(VENDORPATH . 'dompdf/dompdf_config.inc.php');

        $dompdf = new DOMPDF();
        $dompdf->load_html((string)$bill_html);
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();
        $pdf = $dompdf->output('report.pdf');

        $attachment_name = DOCROOT . 'assets/pdf/' . $object_id;
        if (!file_exists($attachment_name)) {
            mkdir($attachment_name, 0777);
        }
        $attachment_name .= '/' . $prefix . '.' . substr(time(), -4) . '.pdf';
        $h = fopen($attachment_name, 'w');
        fwrite($h, $pdf);
        fclose($h);

        $attachment_name = explode('pdf/', $attachment_name);
        return $attachment_name[1];
    }


    /**
     * Generates a PDF from Report view for sending it to e-mail
     * @param $report_content Kohana view class with filled-in data.
     * @param $info Array of parameters such as start and end date
     * @return string Full path to the generated PDF
     */
    public function generate_report_pdf($report_content, $info) {
        // Use a skeleton HTML template for encoding/compatibility
        $report_html = View::factory('templates/pdf');
        $report_html->content = (string)$report_content;

        // Generate PDF from report content and save to cache folder
        include(VENDORPATH . 'dompdf/dompdf_config.inc.php');

        $dompdf = new DOMPDF();
        $dompdf->load_html((string)$report_html);
        $dompdf->set_paper('a4', 'landscape');
        $dompdf->render();
        $pdf = $dompdf->output('report.pdf');

        $attachment_name = DOCROOT . 'assets/pdf/' . Helper_Template::sanitize_file_name('report') . '_' . $info['start'] . '-' . $info['end'] . '_'.substr(time(), -1, 5).'.pdf';
        $h = fopen($attachment_name, 'w');
        fwrite($h, $pdf);
        fclose($h);

        return $attachment_name;
    }


}