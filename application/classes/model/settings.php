<?php defined('SYSPATH') or die('No direct script access.');

class Model_Settings extends Model_Main
{
    /**
     * Returns a setting value or all settings if $key is empty
     * @param string|null $key
     * @return mixed
     */
    public function get($key = NULL) {
        $q = DB::select('key', 'value')
                ->from('settings');
        if ($key !== NULL) {
            $q = $q->where('key', '=', $key);
        }

        $q = $q->execute();

        if ($key !== NULL) {
            return $q->get('value');
        }

        return $q->as_array('key', 'value');
    }

    /**
     * Save a setting value
     *
     * @param  $key
     * @param  $value
     * @return bool
     */
    public function set($key, $value) {
        $q = DB::update('settings')
                ->set(array('value' => $value))
                ->where('key', '=', $key)
                ->execute();
        Syslog::write(__('Seade muutmine: :key => :value', array(':key' => $key, ':value' => $value)));
        return (bool)$q;
    }


    /**
     * Return billing accounts
     * @return array
     */
    public function get_accounts() {
        return unserialize($this->get('billing_accounts'));
    }


    /**
     * Add a new billing account
     * @param int $account
     * @return bool
     */
    public function add_account($account) {
        if (!is_numeric($account) || $account < 0) {
            return FALSE;
        }
        $accounts = $this->get_accounts();
        $accounts[] = (int)$account;
        $accounts = array_unique($accounts);
        $this->set('billing_accounts', serialize($accounts));
        return TRUE;
    }


    /**
     * Delete a billing account
     * @param int $account
     * @return bool
     */
    public function delete_account($account) {
        $accounts = $this->get_accounts();

        if (!in_array($account, $accounts)) {
            return FALSE;
        }
        $account_index = array_search($account, $accounts);

        unset($accounts[$account_index]);
        $this->set('billing_accounts', serialize($accounts));
        return TRUE;
    }

}