<?php defined('SYSPATH') or die('No direct script access.');

class Model_Main extends Kohana_Model
{

    // Tables whose rows can be deleted
    protected $_deletable = array('employees', 'tasks_materials', 'tasks', 'clients', 'objects', 'services');


    /**
     * Get $table data, optionally returning only the one with a specific ID
     * @param int|array|null $id When specified, return specific id's data
     * @param string $table The table name where information is selected from
     * @return Database_Result|object
     */
    public function get_data($table, $id = NULL)
    {
        if (!in_array($table, array('clients', 'tasks_materials', 'employees', 'objects', 'settings', 'services', 'tasks'))) {
            return FALSE;
        }

        $q = DB::select()
                ->from($table)->
                as_object();

        if ($id !== NULL && is_numeric($id) && $id > 0) {
            return $q->where('id', '=', (int)$id)->execute()->current();
        } elseif (is_array($id) && count($id) > 0) {
            return $q->where('id', 'IN', $id)->execute();
        }

        // Exclude deleted objects, but only when ID is not specified.
        // This allows the system to display backwards-compatible data, but
        // keeps the lists/tables clear of old objects.
        if (empty($id) && in_array($table, $this->_deletable)) {
            $q->where('deleted', '=', '0');
        }


        if (in_array($table, array('services', 'employees', 'objects'))) {
            $q->order_by('name');
        } elseif (in_array($table, array('clients'))) {
            $q->order_by('name');
        }

        return $q->execute();
    }


    /**
     * Returns a single field value. Used by AJAX calls to get a piece of information.
     * @param $table
     * @param $field
     * @param $id
     * @return mixed
     */
    public function ajax_get_field($table, $field, $id)
    {
        if (!in_array($table, array('employees')) || !in_array($field, array('code')) || !is_numeric($id)) {
            die('0');
        }

        return DB::select($field)
                ->from($table)
                ->where('id', '=', $id)
                ->execute()
                ->get($field);
    }


    /**
     * Extract the id's of an array of objects
     *
     * @param object $objects An array of objects with the property id. Usually DB::select() results.
     * @return array Array of object id's
     */
    public function extract_ids($objects)
    {

        $ids = array();
        if (!empty($objects)) {
            foreach ($objects as $object) {
                if (is_object($object) && isset($object->id)) {
                    $ids[] = $object->id;
                }
            }
        }
        return $ids;
    }


    /**
     * "Deletes" a database row
     * @param string $what Database table name
     * @param int $id Row ID
     * @return bool|Database_Result|object
     */
    public function delete($what, $id)
    {
        if (!in_array($what, $this->_deletable)) {
            return FALSE;
        }

        $q = DB::update($what)
                ->set(array('deleted' => 1))
                ->where('id', '=', $id);

        if ($what == 'tasks') {
            $q->or_where('duplicate_of', '=', $id);
        }
        Syslog::write(__(':what #:id kustutatud', array(':id' => $id, ':what' => __($what))));
        return $q->execute();
    }


    /**
     * Check if a row is deleted
     * @static
     * @param string $what Table name
     * @param int $id
     * @return bool
     */
    public static function is_deleted($what, $id)
    {
        return (bool)DB::select('deleted')
                ->from($what)
                ->where('id', '=', $id)
                ->execute()
                ->get('deleted');
    }

    /**
     * Check for the existence of a row
     * @todo Refactor, move exists method here
     * @static
     * @param string $where Table name
     * @param int $id Row ID
     * @return bool
     */
    public static function row_exists($where, $id)
    {
        if (is_numeric($id)) {
            return (bool)DB::select('id')
                    ->from($where)
                    ->where('id', '=', $id)
                    ->execute()
                    ->get('id');
        }
        return FALSE;
    }
}