<?php defined('SYSPATH') or die('No direct script access.');

class Model_Cron extends Model_Main
{
    public function send_task_reminders() {
        $t = New Model_Task();
        $o = New Model_Object();
        $count = array('tasks' => 0, 'employees' => 0); // Return stats

        // Get all tasks that happen in ... (2 days?)
        $upcoming = $this->get_upcoming(array('offset' => 2));
        $count['tasks'] = $upcoming->count();

        // For each upcoming task...
        if (!empty($upcoming)) {
            foreach ($upcoming as $task) {

                $object = $o->get($task->object);
                $employees = $t->assoc_employees($task->id);

                // For each employee associated with the task...
                if (!empty($employees)) {
                    foreach ($employees as $employee) {

                        // Start sending e-mails
                        if (!empty($employee->email)) {
                            echo 'Send to ' . $employee->email . ' for task #' . $task->id . '<br />';

                            $subject = '[Kalender] ' . $object->name . ' ' . Date::localized_date($task->started);

                            // Prepare data for showing object contacts
                            $contacts = $o->get_contacts($object->id);
                            $object_contacts = array();
                            if (!empty($contacts)) {
                                foreach ($contacts as $contact) {
                                    $object_contacts[] = ORM::factory('contact', $contact);
                                }
                            }
                            $body = (string)View::factory('email/upcoming', array('task' => $task,
                                                                                 'object' => $object,
                                                                                 'contacts' => $object_contacts
                                                                            ));

                            $count['employees'] += 1;

                            //if (Kohana::$environment == Kohana::PRODUCTION) {
                            Email::send($employee->email,
                                        Kohana::$config->load('app.email'),
                                        $subject,
                                        $body,
                                        TRUE);
                            //}

                            Kohana::$log->add(5, 'Send e-mail: To:' . $employee->email . ', Subject: ' . $subject . ', Body: ' . $body);
                        }
                    }
                }
            }

            $this->mark_notified($upcoming->as_array(NULL, 'id'));

        } else {
            Kohana::$log->add(5, 'Processed e-mail reminders, but the task list was empty.');
        }

        echo '<br />Cron done.';
        return $count;
    }


    /**
     * Mark tasks that just received an e-mail notification as notified
     * @static
     * @param $task_ids An array of task_id's
     * @return bool|Database_Result|object Expected to return TRUE|int
     */
    public static function mark_notified($task_ids) {

        if (empty($task_ids)) {
            return FALSE;
        }
        
        if (!is_array($task_ids)) {
            if (is_numeric($task_ids)) {
                $task_ids = array($task_ids);
            } else {
                return FALSE;
            }
        }

        return DB::update('tasks')
                ->set(array('notified' => 1))
                ->where('task_id', 'IN', $task_ids)
                ->execute();
    }



    /**
     * Get data about upcoming tasks
     *
     * @param array $filters An array of filters
     * Accepted keys:
     * int offset - How far into the future to look, in days
     * string start - Starting date, defaults to NOW().
     *          Ex: start = 31.05.2000 and offset 2 would look for tasks in the range of 31.05.2000 - 02.05.2000
     * bool unfinished Search only unfinished tasks (meaning hasn't been started)
     *
     * @return Database_Result|object
     */
    public function get_upcoming($filters = array()) {
        if (!isset($filters['offset']) || $filters['offset'] < 1) {
            $filters['offset'] = 2; // 2 days before the start
        }
        if (!isset($filters['start']) || !strtotime($filters['start'])) {
            $filters['start'] = date('Y-m-d H:i:s');
        } else {
            $filters['start'] = date('Y-m-d H:i:s', strtotime($filters["start"]));
        }

        $filters['offset'] = date('Y-m-d H:i:s', strtotime("+{$filters['offset']} days", strtotime($filters['start'])));


        $q = DB::select()
                ->from('tasks')
                ->where('started', '<=', $filters['offset'])
                ->where('started', '>=', $filters['start'])
                ->where('deleted', '=', 0)
                ->where('billed', '=', 0);

        if (!isset($filters['unfinished']) || (bool)$filters['unfinished']) {
            $q->where('finished', '>=', DB::expr('started'));
        }

        return $q->as_object()
                ->execute();
    }
}