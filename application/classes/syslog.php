<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Class for quick access to the singleton Log object
 *
 * @package Logging
 * @since 1.1.4
 */
class Syslog
{
    public static $_instance;

    /**
     * @return mixed
     */
    public static function instance()
    {
        if (Syslog::$_instance === NULL) {
            Syslog::$_instance = ORM::factory('log');
        }
        return Syslog::$_instance;
    }


    /**
     * Write a new system log
     *
     * System logs can either be general (user logged in, a record was accessed)
     * or specific to an entity (task #33 description changed)
     * Specific logs are bound to their targets via an additional table
     *
     * @static
     * @since 1.1.4
     * @param string $message The log message in the local language
     * @param array $binding array('entity_name' => 'entity_id') When the log is entity specific...
     * @param int|null $user_id The user ID [optional]
     * @return bool Log write result
     */
    public static function write($message, array $binding = array(), $user_id = NULL)
    {
        $log_id = Syslog::instance()->change(array(
            'text' => $message,
            'binding' => $binding,
            'user_id' => $user_id
        ));
        return (bool)$log_id;
    }
}