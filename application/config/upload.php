<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Config options for object attachment uploads
 *
 * @package Object
 */
return array(
    'allowed_extensions' =>
    array('jpg', 'png', 'gif', 'jpeg', 'pdf', 'csv', 'doc', 'docx', 'xlsx',
        'xlsm', 'ppt', 'pptx', 'txt', 'mp3', 'bmp', 'odt', 'odp', 'ods'),
    'max_size' => '10M'
);