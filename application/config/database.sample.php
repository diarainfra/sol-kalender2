<?php defined('SYSPATH') or die('No direct access allowed.');

$config = array
(
	'default' => array
	(
		'type'       => 'MySQLi',
		'connection' => array(
			/**
			 * The following options are available for MySQL:
			 *
			 * string   hostname     server hostname, or socket
			 * string   database     database name
			 * string   username     database username
			 * string   password     database password
			 * boolean  persistent   use persistent connections?
			 * array    variables    system variables as "key => value" pairs
			 *
			 * Ports and sockets may be appended to the hostname.
			 */
			'hostname'   => 'localhost',
			'database'   => 'kalender',
			'username'   => 'root',
			'password'   => 'root',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
		'profiling'    => FALSE,
	),
    'development' => array
	(
		'type'       => 'MySQLi',
		'connection' => array(
			'hostname'   => 'localhost',
			'database'   => 'kalender',
			'username'   => 'root',
			'password'   => 'root',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
		'profiling'    => FALSE,
	),
    'production' => array
	(
		'type'       => 'MySQLi',
		'connection' => array(
			'hostname'   => 'localhost',
			'database'   => 'kalender',
			'username'   => 'kalender',
			'password'   => 'kalender',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => FALSE,
		'profiling'    => FALSE,
	),
     'unittest' => array
    (
        'type' => 'mysqli',
        'connection' => array(
            'hostname' => 'localhost',
            'username' => 'kalender',
            'password' => 'kalender',
            'persistent' => false,
            'database' => 'kalender'
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
        'profiling' => TRUE,
    )
);


// Hack for different env databases
if (Kohana::$environment == Kohana::PRODUCTION) {
    $config['default'] = $config['production'];
}

if (Kohana::$environment == Kohana::STAGING) {
    $config['default'] = $config['development'];
}

if (php_sapi_name() == 'cli') {
    return array('default' => $config['unittest']);
}

return $config;
