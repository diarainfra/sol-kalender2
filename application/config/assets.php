<?php defined('SYSPATH') or die('No direct access allowed.');

/**
 * Assets class config
 *
 * @package Commoneer
 * @subpackage Assets
 * @category Base
 * @author Ando Roots 2011
 * @copyright GPL v2 http://www.gnu.org/licenses/gpl-2.0.html
 */
return array(

    /**
     * In case you have some crazy routing going on.
     * This will be appended between the base url and the asset path
     * Normally it's fine to leave it to NULL
     */
    'assets_url' => NULL,

    /**
     * Array of asset folder locations relative to the DOCROOT
     * array('css' => array('assets/css/'), 'js' => array(), ...
     * These paths will be searched for the assets you include
     * **/
    'assets_paths' => array(
        'css' => array(
            'assets/css/',
            'assets/css/',
        ),
        'js' => array(
            'assets/js/',
            'assets/js/',
        ),
        'less' => array(
            'assets/less/',
        )
    ),

    /**
     * Predefined assets - a match from here will be searched for first.
     * Syntax: alias (what you use to include the asset) => path (relative to the DOCROOT, no file extension
     */
    'known_assets' => array(
        'css' => array(
            'fullcalendar' => 'assets/js/libs/fullcalendar-1.5.2/fullcalendar',
            'tokeninput' => 'assets/css/token-input-facebook',
            'ui' => 'assets/css/sunny/jquery-ui-1.8.13.custom',
            'timepicker' => 'assets/css/jquery-ui-timepicker-addon',
        ),
        'js' => array(
            'tablesorter' => 'assets/js/libs/jquery.tablesorter.min',
            'tokeninput' => 'assets/js/libs/jquery.tokeninput',
            'fullcalendar' => 'assets/js/libs/fullcalendar-1.5.2/fullcalendar.min',
            'modal' => 'assets/js/libs/bootstrap/modal-1.4.0.min',
            'tabs' => 'assets/js/libs/bootstrap/tabs-1.4.0.min',
            'ui' => 'assets/js/libs/jquery-ui-1.8.16.min',
            'datepicker-et' => 'assets/js/libs/jquery.ui.datepicker-et',
            'timepicker' => 'assets/js/libs/jquery-ui-timepicker-addon'
        ),
        'less' => array(

        )
    ),

    'presets' => array(

        'ui' => array(
            'js' => array(
                'ui'
            ),
            'css' => array(
                'ui'
            )
        ),
        'date' => array(
            'js' => array(
                'datepicker-et',
                'timepicker',
                'ui-plugins'
            ),
            'css' => array('timepicker')
        ),

    )
);

