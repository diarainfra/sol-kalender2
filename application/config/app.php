<?php defined('SYSPATH') or die('No direct access allowed.');
return array(

    // Used for the sender field of automatic e-mails such as cron notifications
    'email' => 'noreply@diara.ee',

    /*
     * When creating a repeating task, this is the number of max duplicates to be created.
     * When CRON needs to create more duplicates than this number, an error message is printed
     */
    'max_duplicate_count' => 365,

    /**
     * App title
     */
    'title' => 'Kalenderrakendus' . (Kohana::$environment === Kohana::DEVELOPMENT ? ' - [arendusversioon]' : NULL),

    /**
     * Major.Minor.Patch
     */
    'version' => '1.3.0',

    /**
     * Dev server URL
     */
    'dev_url' => 'http://dev.diara.ee/kalender/',

    /**
     * Live URL
     */
    'live_url' => 'https://sol.ee/kalender',

    /**
     * Project codename
     */
    'codename' => 'Kalenderrakendus'
);
