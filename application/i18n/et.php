<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'Error :code' => 'Viga :code',
    'Error' => 'Viga',
    'Sorry, but there was an error while processing your request.' => 'Päringu töötlemisel ilmnes programmi või ressursi viga, ajutine kättesaamatus või muu tõrge.',
    'Home' => 'Avaleht',

    'public.text.must_login' => 'Süsteemi kasutamiseks pead sisse logima.',
    'public.text.admin_creates_accounts' => 'Kasutajakontosid saab  luua administraator.',
    'Login' => 'Sisene',
    'Username' => 'Kasutajanimi',
    'Password' => 'Parool',
    'Remember me' => 'Jäta meelde',
    'tasks_materials' => 'Kasutatud materjal'
);